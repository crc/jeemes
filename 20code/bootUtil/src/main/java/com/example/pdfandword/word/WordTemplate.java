package com.example.pdfandword.word;

import com.example.pdfandword.controller.TableEntity;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class WordTemplate {

    private XWPFDocument document;

    public WordTemplate(InputStream inputStream) throws IOException {
        document = new XWPFDocument(inputStream);
    }

    public synchronized void replaceTag(Map<String, Object> map) {
        for (Entry<String, Object> item : map.entrySet()) {
            if (item.getKey().equals("String")) {   //替换
                Map<String, String> tempMap = (Map<String, String>) item.getValue();
                replaceParagraphs(tempMap);
                replaceTables(tempMap);
            } else if (item.getKey().equals("Table")) {//表格变量
                replaceKeyToTables((Map<String, Object>) item.getValue());

            } else if (item.getKey().equals("pic")) {//图片
                Map<String, InputStream> tempMap = (Map<String, InputStream>) item.getValue();
                try {
                    refreshBooks(tempMap);
                } catch (Exception e) {

                }
            }
        }

    }

    public void write(OutputStream outputStream) throws IOException {
        document.write(outputStream);
    }

    /**
     * 鏇挎崲娈佃惤閲岀殑淇℃伅顒�
     *
     * @param map
     */
    private void replaceParagraphs(Map<String, String> map) {
        List<XWPFParagraph> allXWPFParagraphs = document.getParagraphs();
        for (XWPFParagraph XwpfParagrapg : allXWPFParagraphs) {
            XWPFParagraphHandler XwpfParagrapgUtils = new XWPFParagraphHandler(XwpfParagrapg);
            XwpfParagrapgUtils.replaceAll(map);
        }
    }
    private synchronized void refreshBooks(Map<String, InputStream> dataMap) throws IOException, InvalidFormatException {
        List<XWPFParagraph> paragraphs = document.getParagraphs();
        for (XWPFParagraph xwpfParagraph : paragraphs) {
            CTP ctp = xwpfParagraph.getCTP();

            for (int dwI = 0; dwI < ctp.sizeOfBookmarkStartArray(); dwI++) {
                CTBookmark bookmark = ctp.getBookmarkStartArray(dwI);

                InputStream picIs = dataMap.get(bookmark.getName());
                if(picIs != null){
                    XWPFRun run = xwpfParagraph.createRun();
                    //bus.png为鼠标在word里选择图片时，图片显示的名字，400，400则为像素单元，根据实际需要的大小进行调整即可。
                    try {
                        run.addPicture(picIs,XWPFDocument.PICTURE_TYPE_PNG,"bus.png,", Units.toEMU(40), Units.toEMU(40));
                    } catch (InvalidFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }
    /**
     * 鏇挎崲琛ㄦ牸閲岀殑淇℃伅顒�
     *
     * @param map
     */
    private void replaceTables(Map<String, String> map) {
        List<XWPFTable> xwpfTables = document.getTables();
        for (XWPFTable xwpfTable : xwpfTables) {
            XWPFTableHandler xwpfTableUtils = new XWPFTableHandler(xwpfTable);
            xwpfTableUtils.replace(map);
        }
    }








    /**
     * 鏇挎崲琛ㄦ牸
     *
     * @author huangdongkui
     * @param
     */
    public void replaceKeyToTables(Map<String, Object> param) {
        List<XWPFParagraph> paragraphList = document.getParagraphs();
        if (paragraphList != null && paragraphList.size() > 0) {
            for (XWPFParagraph paragraph : paragraphList) {
                String text = paragraph.getParagraphText();
                for (Entry<String, Object> entry : param.entrySet()) {
                    String key = entry.getKey();
                    if (text.indexOf(key) != -1) {

                        Object value = entry.getValue();
                        if (value instanceof List) {// 琛ㄦ牸
                            text = text.replace(key, "");

                            XWPFTable tableOne = document.insertNewTbl(paragraph.getCTP().newCursor());

                            //娣诲姞鍒�
                            for (int col = 1; col < ((Map<String, String>) ((List) value).get(0)).entrySet().size(); col++) {
                                tableOne.addNewCol();
                            }

                            // 濉厖鍒楀ご
                            XWPFTableRow crow = tableOne.createRow();
                            int k = 0;
                            for (Entry<String, String> col : ((Map<String, String>) ((List) value).get(0)).entrySet()) {
                                String skey = col.getKey();
                                if (skey!=null&&!skey.equals("")) {
                                    crow.getCell(k).setText(skey);

                                    k++;
                                }
                            }

                            for (int i = 1; i < ((List) value).size(); i++) {
                                // 娣诲姞琛�
                                XWPFTableRow rrow = tableOne.createRow();
                                int j = 0;
                                for (Entry<String, String> col : ((Map<String, String>) ((List) value).get(i)).entrySet()) {
                                    String sValue = col.getValue();
                                    if (sValue!=null&&!sValue.equals(""))  {
                                        rrow.getCell(j).setText(sValue);

                                        j++;
                                    }
                                }

                            }

                            tableOne.removeRow(0);
                            setTableWidthAndHAlign(tableOne, "8000", STJc.CENTER);

                        }
                        List<XWPFRun> runs = paragraph.getRuns();

                        for (XWPFRun xwpfrun : runs) {
                            xwpfrun.setText("", 0);
                        }

                    }
                }

            }
        }
    }

    public void setTableWidthAndHAlign(XWPFTable table, String width, STJc.Enum enumValue) {
        CTTblPr tblPr = getTableCTTblPr(table);
        CTTblWidth tblWidth = tblPr.isSetTblW() ? tblPr.getTblW() : tblPr.addNewTblW();
        if (enumValue != null) {
            CTJc cTJc = tblPr.addNewJc();
            cTJc.setVal(enumValue);
        }

        tblWidth.setW(new BigInteger(width));
        tblWidth.setType(STTblWidth.DXA);

    }

    /**
     * @Description: 寰楀埌Table鐨凜TTblPr,涓嶅瓨鍦ㄥ垯鏂板缓
     */
    public CTTblPr getTableCTTblPr(XWPFTable table) {
        CTTbl ttbl = table.getCTTbl();
        CTTblPr tblPr = ttbl.getTblPr() == null ? ttbl.addNewTblPr() : ttbl.getTblPr();
        return tblPr;
    }

}
