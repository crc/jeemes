package org.jeecg.modules.mes.inspect.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mes.inspect.entity.MesChooseCheckLog;
import org.jeecg.modules.mes.inspect.service.IMesChooseCheckLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

 /**
 * @Description: 抽检抽测表
 * @Author: jeecg-boot
 * @Date:   2021-03-31
 * @Version: V1.0
 */
@Api(tags="抽检抽测表")
@RestController
@RequestMapping("/inspect/mesChooseCheckLog")
@Slf4j
public class MesChooseCheckLogController extends JeecgController<MesChooseCheckLog, IMesChooseCheckLogService> {
	@Autowired
	private IMesChooseCheckLogService mesChooseCheckLogService;

	/**
	 * 分页列表查询
	 *
	 * @param mesChooseCheckLog
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "抽检抽测表-分页列表查询")
	@ApiOperation(value="抽检抽测表-分页列表查询", notes="抽检抽测表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChooseCheckLog mesChooseCheckLog,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChooseCheckLog> queryWrapper = QueryGenerator.initQueryWrapper(mesChooseCheckLog, req.getParameterMap());
		Page<MesChooseCheckLog> page = new Page<MesChooseCheckLog>(pageNo, pageSize);
		IPage<MesChooseCheckLog> pageList = mesChooseCheckLogService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param mesChooseCheckLog
	 * @return
	 */
	@AutoLog(value = "抽检抽测表-添加")
	@ApiOperation(value="抽检抽测表-添加", notes="抽检抽测表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChooseCheckLog mesChooseCheckLog) {
		LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		mesChooseCheckLog.setUserId(loginUser.getId());
		mesChooseCheckLog.setUserName(loginUser.getRealname());
		mesChooseCheckLog.setInDate(new Date());
		mesChooseCheckLogService.save(mesChooseCheckLog);
		return Result.ok("添加成功！");
	}

	 @AutoLog(value = "抽检抽测表-批量添加")
	 @ApiOperation(value="抽检抽测表-批量添加", notes="抽检抽测表-批量添加")
	 @PostMapping(value = "/batchadd")
	 public Result<?> batchadd(@RequestBody List<MesChooseCheckLog> mesChooseCheckLog) {
		 LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 for (int i = 0; i < mesChooseCheckLog.size(); i++) {
			 mesChooseCheckLog.get(i).setUserId(loginUser.getId());
			 mesChooseCheckLog.get(i).setUserName(loginUser.getRealname());
			 mesChooseCheckLog.get(i).setInDate(new Date());
		 }
		 mesChooseCheckLogService.saveBatch(mesChooseCheckLog);
		 return Result.ok("添加成功！");
	 }

	/**
	 *  编辑
	 *
	 * @param mesChooseCheckLog
	 * @return
	 */
	@AutoLog(value = "抽检抽测表-编辑")
	@ApiOperation(value="抽检抽测表-编辑", notes="抽检抽测表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChooseCheckLog mesChooseCheckLog) {
		mesChooseCheckLogService.updateById(mesChooseCheckLog);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "抽检抽测表-通过id删除")
	@ApiOperation(value="抽检抽测表-通过id删除", notes="抽检抽测表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChooseCheckLogService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "抽检抽测表-批量删除")
	@ApiOperation(value="抽检抽测表-批量删除", notes="抽检抽测表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChooseCheckLogService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "抽检抽测表-通过id查询")
	@ApiOperation(value="抽检抽测表-通过id查询", notes="抽检抽测表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesChooseCheckLog mesChooseCheckLog = mesChooseCheckLogService.getById(id);
		if(mesChooseCheckLog==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChooseCheckLog);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesChooseCheckLog
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChooseCheckLog mesChooseCheckLog) {
        return super.exportXls(request, mesChooseCheckLog, MesChooseCheckLog.class, "抽检抽测表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesChooseCheckLog.class);
    }

}
