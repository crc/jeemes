package org.jeecg.modules.mes.inspect.controller;

import org.jeecg.common.system.query.QueryGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.inspect.vo.MesIpqcDocketPage;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import java.util.Arrays;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.inspect.entity.MesIpqcdocketItem;
import org.jeecg.modules.mes.inspect.entity.MesIpqcDocket;
import org.jeecg.modules.mes.inspect.service.IMesIpqcDocketService;
import org.jeecg.modules.mes.inspect.service.IMesIpqcdocketItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 质检中心-IPQC单据
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Api(tags="质检中心-IPQC单据")
@RestController
@RequestMapping("/inspect/mesIpqcDocket")
@Slf4j
public class MesIpqcDocketController extends JeecgController<MesIpqcDocket, IMesIpqcDocketService> {

	@Autowired
	private IMesIpqcDocketService mesIpqcDocketService;

	@Autowired
	private IMesIpqcdocketItemService mesIpqcdocketItemService;


	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * @param mesIpqcDocket
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "质检中心-IPQC单据-分页列表查询")
	@ApiOperation(value="质检中心-IPQC单据-分页列表查询", notes="质检中心-IPQC单据-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesIpqcDocket mesIpqcDocket,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesIpqcDocket> queryWrapper = QueryGenerator.initQueryWrapper(mesIpqcDocket, req.getParameterMap());
		Page<MesIpqcDocket> page = new Page<MesIpqcDocket>(pageNo, pageSize);
		IPage<MesIpqcDocket> pageList = mesIpqcDocketService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	 /**
	  *   添加
	  *
	  * @param mesIpqcDocketPage
	  * @return
	  */
	 @AutoLog(value = "质检中心-IPQC单据-添加")
	 @ApiOperation(value="质检中心-IPQC单据-添加", notes="质检中心-IPQC单据-添加")
	 @PostMapping(value = "/addpage")
	 public Result<?> addpage(@RequestBody MesIpqcDocketPage mesIpqcDocketPage) {
		 MesIpqcDocket mesIpqcDocket = new MesIpqcDocket();
		 BeanUtils.copyProperties(mesIpqcDocketPage, mesIpqcDocket);
		 mesIpqcDocketService.saveMain(mesIpqcDocket, mesIpqcDocketPage.getMesIpqcdocketItemList());
		 return Result.ok("添加成功！");
	 }
	/**
     *   添加
     * @param mesIpqcDocket
     * @return
     */
    @AutoLog(value = "质检中心-IPQC单据-添加")
    @ApiOperation(value="质检中心-IPQC单据-添加", notes="质检中心-IPQC单据-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesIpqcDocket mesIpqcDocket) {
        mesIpqcDocketService.save(mesIpqcDocket);
        return Result.ok("添加成功！");
    }

    /**
     *  编辑
     * @param mesIpqcDocket
     * @return
     */
    @AutoLog(value = "质检中心-IPQC单据-编辑")
    @ApiOperation(value="质检中心-IPQC单据-编辑", notes="质检中心-IPQC单据-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesIpqcDocket mesIpqcDocket) {
        mesIpqcDocketService.updateById(mesIpqcDocket);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "质检中心-IPQC单据-通过id删除")
    @ApiOperation(value="质检中心-IPQC单据-通过id删除", notes="质检中心-IPQC单据-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name="id",required=true) String id) {
        mesIpqcDocketService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "质检中心-IPQC单据-批量删除")
    @ApiOperation(value="质检中心-IPQC单据-批量删除", notes="质检中心-IPQC单据-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.mesIpqcDocketService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesIpqcDocket mesIpqcDocket) {
        return super.exportXls(request, mesIpqcDocket, MesIpqcDocket.class, "质检中心-IPQC单据");
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesIpqcDocket.class);
    }
	/*---------------------------------主表处理-end-------------------------------------*/


    /*--------------------------------子表处理-单据明细-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "单据明细-通过主表ID查询")
	@ApiOperation(value="单据明细-通过主表ID查询", notes="单据明细-通过主表ID查询")
	@GetMapping(value = "/listMesIpqcdocketItemByMainId")
    public Result<?> listMesIpqcdocketItemByMainId(MesIpqcdocketItem mesIpqcdocketItem,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<MesIpqcdocketItem> queryWrapper = QueryGenerator.initQueryWrapper(mesIpqcdocketItem, req.getParameterMap());
        Page<MesIpqcdocketItem> page = new Page<MesIpqcdocketItem>(pageNo, pageSize);
        IPage<MesIpqcdocketItem> pageList = mesIpqcdocketItemService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

	/**
	 * 添加
	 * @param mesIpqcdocketItem
	 * @return
	 */
	@AutoLog(value = "单据明细-添加")
	@ApiOperation(value="单据明细-添加", notes="单据明细-添加")
	@PostMapping(value = "/addMesIpqcdocketItem")
	public Result<?> addMesIpqcdocketItem(@RequestBody MesIpqcdocketItem mesIpqcdocketItem) {
		mesIpqcdocketItemService.save(mesIpqcdocketItem);
		return Result.ok("添加成功！");
	}

    /**
	 * 编辑
	 * @param mesIpqcdocketItem
	 * @return
	 */
	@AutoLog(value = "单据明细-编辑")
	@ApiOperation(value="单据明细-编辑", notes="单据明细-编辑")
	@PutMapping(value = "/editMesIpqcdocketItem")
	public Result<?> editMesIpqcdocketItem(@RequestBody MesIpqcdocketItem mesIpqcdocketItem) {
		mesIpqcdocketItemService.updateById(mesIpqcdocketItem);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "单据明细-通过id删除")
	@ApiOperation(value="单据明细-通过id删除", notes="单据明细-通过id删除")
	@DeleteMapping(value = "/deleteMesIpqcdocketItem")
	public Result<?> deleteMesIpqcdocketItem(@RequestParam(name="id",required=true) String id) {
		mesIpqcdocketItemService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "单据明细-批量删除")
	@ApiOperation(value="单据明细-批量删除", notes="单据明细-批量删除")
	@DeleteMapping(value = "/deleteBatchMesIpqcdocketItem")
	public Result<?> deleteBatchMesIpqcdocketItem(@RequestParam(name="ids",required=true) String ids) {
	    this.mesIpqcdocketItemService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportMesIpqcdocketItem")
    public ModelAndView exportMesIpqcdocketItem(HttpServletRequest request, MesIpqcdocketItem mesIpqcdocketItem) {
		 // Step.1 组装查询条件
		 QueryWrapper<MesIpqcdocketItem> queryWrapper = QueryGenerator.initQueryWrapper(mesIpqcdocketItem, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<MesIpqcdocketItem> pageList = mesIpqcdocketItemService.list(queryWrapper);
		 List<MesIpqcdocketItem> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "单据明细"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, MesIpqcdocketItem.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("单据明细报表", "导出人:" + sysUser.getRealname(), "单据明细"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importMesIpqcdocketItem/{mainId}")
    public Result<?> importMesIpqcdocketItem(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<MesIpqcdocketItem> list = ExcelImportUtil.importExcel(file.getInputStream(), MesIpqcdocketItem.class, params);
				 for (MesIpqcdocketItem temp : list) {
                    temp.setIpqcId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 mesIpqcdocketItemService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.ok("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-单据明细-end----------------------------------------------*/




}
