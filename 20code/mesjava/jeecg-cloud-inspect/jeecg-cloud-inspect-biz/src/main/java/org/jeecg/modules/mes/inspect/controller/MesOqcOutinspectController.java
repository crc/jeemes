package org.jeecg.modules.mes.inspect.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.inspect.entity.MesOqcOutinspect;
import org.jeecg.modules.mes.inspect.service.IMesOqcOutinspectService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 品质报表—OQC出货检验
 * @Author: jeecg-boot
 * @Date:   2021-01-20
 * @Version: V1.0
 */
@Api(tags="品质报表—OQC出货检验")
@RestController
@RequestMapping("/inspect/mesOqcOutinspect")
@Slf4j
public class MesOqcOutinspectController extends JeecgController<MesOqcOutinspect, IMesOqcOutinspectService> {
	@Autowired
	private IMesOqcOutinspectService mesOqcOutinspectService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesOqcOutinspect
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "品质报表—OQC出货检验-分页列表查询")
	@ApiOperation(value="品质报表—OQC出货检验-分页列表查询", notes="品质报表—OQC出货检验-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesOqcOutinspect mesOqcOutinspect,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesOqcOutinspect> queryWrapper = QueryGenerator.initQueryWrapper(mesOqcOutinspect, req.getParameterMap());
		Page<MesOqcOutinspect> page = new Page<MesOqcOutinspect>(pageNo, pageSize);
		IPage<MesOqcOutinspect> pageList = mesOqcOutinspectService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesOqcOutinspect
	 * @return
	 */
	@AutoLog(value = "品质报表—OQC出货检验-添加")
	@ApiOperation(value="品质报表—OQC出货检验-添加", notes="品质报表—OQC出货检验-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesOqcOutinspect mesOqcOutinspect) {
		mesOqcOutinspectService.save(mesOqcOutinspect);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesOqcOutinspect
	 * @return
	 */
	@AutoLog(value = "品质报表—OQC出货检验-编辑")
	@ApiOperation(value="品质报表—OQC出货检验-编辑", notes="品质报表—OQC出货检验-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesOqcOutinspect mesOqcOutinspect) {
		mesOqcOutinspectService.updateById(mesOqcOutinspect);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "品质报表—OQC出货检验-通过id删除")
	@ApiOperation(value="品质报表—OQC出货检验-通过id删除", notes="品质报表—OQC出货检验-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesOqcOutinspectService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "品质报表—OQC出货检验-批量删除")
	@ApiOperation(value="品质报表—OQC出货检验-批量删除", notes="品质报表—OQC出货检验-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesOqcOutinspectService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "品质报表—OQC出货检验-通过id查询")
	@ApiOperation(value="品质报表—OQC出货检验-通过id查询", notes="品质报表—OQC出货检验-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesOqcOutinspect mesOqcOutinspect = mesOqcOutinspectService.getById(id);
		if(mesOqcOutinspect==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesOqcOutinspect);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesOqcOutinspect
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesOqcOutinspect mesOqcOutinspect) {
        return super.exportXls(request, mesOqcOutinspect, MesOqcOutinspect.class, "品质报表—OQC出货检验");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesOqcOutinspect.class);
    }

}
