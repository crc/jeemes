package org.jeecg.modules.mes.inspect.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.inspect.entity.PatrolInspectLog;
import org.jeecg.modules.mes.inspect.entity.PatrolInspectMain;
import org.jeecg.modules.mes.inspect.service.IPatrolInspectLogService;
import org.jeecg.modules.mes.inspect.service.IPatrolInspectMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

 /**
 * @Description: 巡检模块主表
 * @Author: jeecg-boot
 * @Date:   2021-03-21
 * @Version: V1.0
 */
@Api(tags="巡检模块主表")
@RestController
@RequestMapping("/inspect/patrolInspectMain")
@Slf4j
public class PatrolInspectMainController extends JeecgController<PatrolInspectMain, IPatrolInspectMainService> {

	@Autowired
	private IPatrolInspectMainService patrolInspectMainService;
	@Autowired
	private IPatrolInspectLogService patrolInspectLogService;


	/**
	 * 分页列表查询
	 *
	 * @param patrolInspectMain
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "巡检模块主表-分页列表查询")
	@ApiOperation(value="巡检模块主表-分页列表查询", notes="巡检模块主表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PatrolInspectMain patrolInspectMain,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<PatrolInspectMain> queryWrapper = QueryGenerator.initQueryWrapper(patrolInspectMain, req.getParameterMap());
		queryWrapper.orderByDesc("create_time");
		Page<PatrolInspectMain> page = new Page<PatrolInspectMain>(pageNo, pageSize);
		IPage<PatrolInspectMain> pageList = patrolInspectMainService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @return
	 */
	@AutoLog(value = "巡检模块主表-添加")
	@ApiOperation(value="巡检模块主表-添加", notes="巡检模块主表-添加 添加记录的时候同时会判断有没有这个主表信息，没有就会新增")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody List<PatrolInspectLog> patrolInspectLogs) {
		patrolInspectMainService.saveMain(patrolInspectLogs);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param patrolInspectMain
	 * @return
	 */
	@AutoLog(value = "巡检模块主表-编辑")
	@ApiOperation(value="巡检模块主表-编辑", notes="巡检模块主表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PatrolInspectMain patrolInspectMain) {
		patrolInspectMainService.updateById(patrolInspectMain);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "巡检模块主表-通过id删除")
	@ApiOperation(value="巡检模块主表-通过id删除", notes="巡检模块主表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		patrolInspectMainService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "巡检模块主表-批量删除")
	@ApiOperation(value="巡检模块主表-批量删除", notes="巡检模块主表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.patrolInspectMainService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "巡检模块主表-通过id查询")
	@ApiOperation(value="巡检模块主表-通过id查询", notes="巡检模块主表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PatrolInspectMain patrolInspectMain = patrolInspectMainService.getById(id);
		if(patrolInspectMain==null) {
			return Result.error("未找到对应数据");
		}else {
			QueryWrapper<PatrolInspectLog> QueryWrapper = new QueryWrapper<>();
			QueryWrapper.eq("main_id",patrolInspectMain.getId());
			List<PatrolInspectLog> patrolInspectLogs = patrolInspectLogService.list(QueryWrapper);
			patrolInspectMain.setPatrolInspectLogs(patrolInspectLogs);
		}
		return Result.ok(patrolInspectMain);
	}

	 /**
	  * 通过id查询
	  *
	  * @param inspectDate
	  * @param lineType
	  * @param inspectFrequen
	  * @return
	  */
	 @AutoLog(value = "巡检模块主表-通过巡检时间、班别、线别查询")
	 @ApiOperation(value="巡检模块主表-通过巡检时间、班别、线别查询", notes="巡检模块主表-通过巡检时间、班别、线别查询")
	 @GetMapping(value = "/queryByIds")
	 public Result<?> queryByIds(@RequestParam(name="inspectDate",required=true) String inspectDate,
								 @RequestParam(name="lineType",required=true) String lineType,
								 @RequestParam(name="inspectFrequen",required=true) String inspectFrequen) {
		 QueryWrapper<PatrolInspectMain> mainQueryWrapper = new QueryWrapper<>();
		 mainQueryWrapper.eq("inspect_date",inspectDate);
		 mainQueryWrapper.eq("line_type",lineType);
		 mainQueryWrapper.eq("inspect_frequen",inspectFrequen);
		 List<PatrolInspectMain> list = patrolInspectMainService.list(mainQueryWrapper);
		 if(list.size()==0){
			 return Result.error("未找到该班别、线别、巡检日期的数据！");
		 }
		 PatrolInspectMain inspectMain = list.get(0);
		 QueryWrapper<PatrolInspectLog> QueryWrapper = new QueryWrapper<>();
		 QueryWrapper.eq("main_id",list.get(0).getId());
		 List<PatrolInspectLog> patrolInspectLogs = patrolInspectLogService.list(QueryWrapper);
		 for(int i = 0; i < patrolInspectLogs.size(); i++){
			 System.out.println(patrolInspectLogs.get(i));
			 if(patrolInspectLogs.get(i).getPatrolUps()==null){
				 patrolInspectLogs.get(i).setPatrolUps(new ArrayList<>());
			 }
			 patrolInspectLogs.get(i).getPatrolUps().add(patrolInspectLogs.get(i).getPatrolUp());
		 }
		 inspectMain.setPatrolInspectLogs(patrolInspectLogs);
		 for (int i = 1; i < list.size(); i++) {
			 QueryWrapper<PatrolInspectLog> QueryWrapperd = new QueryWrapper<>();
			 QueryWrapperd.eq("main_id",list.get(i).getId());
			 List<PatrolInspectLog> patrolInspectLogsd = patrolInspectLogService.list(QueryWrapperd);
			 for (int j = 0; j < patrolInspectLogsd.size(); j++) {
				 inspectMain.getPatrolInspectLogs().get(j).getPatrolUps().add(patrolInspectLogsd.get(j).getPatrolUp());
				 String patrolstate=inspectMain.getPatrolInspectLogs().get(j).getPatrolState()+patrolInspectLogsd.get(j).getPatrolState();
				 inspectMain.getPatrolInspectLogs().get(j).setPatrolState(patrolstate);
			 }
		 }
		 return Result.ok(inspectMain);
	 }

    /**
    * 导出excel
    *
    * @param request
    * @param patrolInspectMain
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PatrolInspectMain patrolInspectMain) {
        return super.exportXls(request, patrolInspectMain, PatrolInspectMain.class, "巡检模块主表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PatrolInspectMain.class);
    }

}
