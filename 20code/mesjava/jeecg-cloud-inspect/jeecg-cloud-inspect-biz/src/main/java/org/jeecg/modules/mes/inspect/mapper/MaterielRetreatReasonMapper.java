package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;
import org.jeecg.modules.mes.inspect.entity.MaterielRetreatReason;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 退料原因表
 * @Author: jeecg-boot
 * @Date:   2021-04-25
 * @Version: V1.0
 */
public interface MaterielRetreatReasonMapper extends BaseMapper<MaterielRetreatReason> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MaterielRetreatReason> selectByMainId(@Param("mainId") String mainId);

}
