package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;
import org.jeecg.modules.mes.inspect.entity.MesCheckprojectType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 检测类型信息
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface MesCheckprojectTypeMapper extends BaseMapper<MesCheckprojectType> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesCheckprojectType> selectByMainId(@Param("mainId") String mainId);

}
