package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MaterielRetreatReason;
import org.jeecg.modules.mes.inspect.entity.MaterielRetreatRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 物料退料记录表
 * @Author: jeecg-boot
 * @Date:   2021-04-25
 * @Version: V1.0
 */
public interface IMaterielRetreatRecordService extends IService<MaterielRetreatRecord> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);


	public boolean saveAll(MaterielRetreatRecord retreatRecord);


	public boolean updateByedit(MaterielRetreatRecord retreatRecord);

}
