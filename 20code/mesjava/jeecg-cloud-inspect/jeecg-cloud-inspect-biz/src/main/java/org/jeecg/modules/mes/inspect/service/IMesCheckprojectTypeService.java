package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesCheckprojectType;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 检测类型信息
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesCheckprojectTypeService extends IService<MesCheckprojectType> {

	public List<MesCheckprojectType> selectByMainId(String mainId);
}
