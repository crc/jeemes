package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesBodLog;
import org.jeecg.modules.mes.inspect.mapper.MesBodLogMapper;
import org.jeecg.modules.mes.inspect.service.IMesBodLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 出货不良记录表
 * @Author: jeecg-boot
 * @Date:   2021-03-21
 * @Version: V1.0
 */
@Service
public class MesBodLogServiceImpl extends ServiceImpl<MesBodLogMapper, MesBodLog> implements IMesBodLogService {

}
