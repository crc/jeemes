package org.jeecg.modules.mes.inspect.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.mes.inspect.entity.MesBodLog;
import org.jeecg.modules.mes.inspect.entity.MesInspectionOut;
import org.jeecg.modules.mes.inspect.mapper.MesBodLogMapper;
import org.jeecg.modules.mes.inspect.mapper.MesInspectionOutMapper;
import org.jeecg.modules.mes.inspect.service.IMesInspectionOutService;
import org.jeecg.modules.mes.inspect.vo.ReportVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.jeecg.modules.mes.inspect.vo.InspectVo;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: 出货检验报告
 * @Author: jeecg-boot
 * @Date:   2021-03-20
 * @Version: V1.0
 */
@Service
public class MesInspectionOutServiceImpl extends ServiceImpl<MesInspectionOutMapper, MesInspectionOut> implements IMesInspectionOutService {
    /**
     * 出货检验报告
     */
    @Autowired
    private MesInspectionOutMapper mesInspectionOutMapper;

    /**
     * 不良现象记录表
     */
    @Autowired
    private MesBodLogMapper mesBodLogMapper;

    @Override
    public List<InspectVo> selectLplList (String level, String goal,String begindate,String enddate,String linecode,String procode){
        return mesInspectionOutMapper.selectLplList(level,goal,begindate,enddate,linecode,procode);
    }

    @Override
    public List<InspectVo> selectLineLplList (String level,String begindate,String enddate,String linecode,String procode){
        return mesInspectionOutMapper.selectLineLplList(level,begindate,enddate,linecode,procode);
    }

    @Override
    public List<InspectVo> selectLplListByDay (String level, String goal,String begindate,String enddate,String linecode,String procode){
        return mesInspectionOutMapper.selectLplListByDay(level,goal,begindate,enddate,linecode,procode);
    }

    @Override
    public List<InspectVo> selectLplListByWeek(String level, String goal,String begindate,String enddate,String linecode,String procode){
        return mesInspectionOutMapper.selectLplListByWeek(level,goal,begindate,enddate,linecode,procode);
    }

    @Override
    public List<InspectVo> selectLplListByYear(String level, String goal,String begindate,String enddate,String linecode,String procode){
        return mesInspectionOutMapper.selectLplListByYear(level,goal,begindate,enddate,linecode,procode);
    }

    @Override
    public List<InspectVo> selectTopIssue (String level,String begindate, String enddate,String linecode,String procode){
        return mesInspectionOutMapper.selectTopIssue(level,begindate,enddate,linecode,procode);
    }


    @Override
    public List<InspectVo> selectPicLoss (String level,String begindate, String enddate,String linecode,String procode){
        return mesInspectionOutMapper.selectPicLoss(level,begindate,enddate,linecode,procode);
    }

    @Override
    public List<InspectVo> selectDeptPicLoss (String level, String dept,String begindate, String enddate,String linecode,String procode){
        return mesInspectionOutMapper.selectDeptPicLoss(level,dept,begindate,enddate,linecode,procode);
    }

    @Override
    public Page<ReportVo> selectTld(Page<ReportVo> page, String begindate){
        return page.setRecords(mesInspectionOutMapper.selectTld(page,begindate));
    }



    /**
     * 新增出货检验报告
     */
    @Override
    @Transactional
    public void saveMain(MesInspectionOut inspectionOut, List<MesBodLog> bodLogs){
        mesInspectionOutMapper.insert(inspectionOut);
        for (int i = 0; i < bodLogs.size(); i++) {
            MesBodLog bodLog = bodLogs.get(i);
            bodLog.setOutId(inspectionOut.getId());
            mesBodLogMapper.insert(bodLog);
        }

    }

}
