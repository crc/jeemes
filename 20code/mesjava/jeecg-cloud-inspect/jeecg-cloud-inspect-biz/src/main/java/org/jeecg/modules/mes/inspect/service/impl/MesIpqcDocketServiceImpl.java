package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesIpqcDocket;
import org.jeecg.modules.mes.inspect.entity.MesIpqcdocketItem;
import org.jeecg.modules.mes.inspect.mapper.MesIpqcdocketItemMapper;
import org.jeecg.modules.mes.inspect.mapper.MesIpqcDocketMapper;
import org.jeecg.modules.mes.inspect.service.IMesIpqcDocketService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 质检中心-IPQC单据
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesIpqcDocketServiceImpl extends ServiceImpl<MesIpqcDocketMapper, MesIpqcDocket> implements IMesIpqcDocketService {

	@Autowired
	private MesIpqcDocketMapper mesIpqcDocketMapper;
	@Autowired
	private MesIpqcdocketItemMapper mesIpqcdocketItemMapper;

	@Override
	@Transactional
	public void delMain(String id) {
		mesIpqcdocketItemMapper.deleteByMainId(id);
		mesIpqcDocketMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesIpqcdocketItemMapper.deleteByMainId(id.toString());
			mesIpqcDocketMapper.deleteById(id);
		}
	}
	@Override
	@Transactional
	public void saveMain(MesIpqcDocket mesIpqcDocket, List<MesIpqcdocketItem> mesIpqcdocketItemList) {
		mesIpqcDocketMapper.insert(mesIpqcDocket);
		if(mesIpqcdocketItemList!=null && mesIpqcdocketItemList.size()>0) {
			for(MesIpqcdocketItem entity:mesIpqcdocketItemList) {
				//外键设置
				entity.setIpqcId(mesIpqcDocket.getId());
				mesIpqcdocketItemMapper.insert(entity);
			}
		}
	}
}
