package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesMaterielCheckprojrct;
import org.jeecg.modules.mes.inspect.entity.MesCheckprojectInfo;
import org.jeecg.modules.mes.inspect.mapper.MesCheckprojectInfoMapper;
import org.jeecg.modules.mes.inspect.mapper.MesMaterielCheckprojrctMapper;
import org.jeecg.modules.mes.inspect.service.IMesMaterielCheckprojrctService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 质检中心-物料检测项目
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesMaterielCheckprojrctServiceImpl extends ServiceImpl<MesMaterielCheckprojrctMapper, MesMaterielCheckprojrct> implements IMesMaterielCheckprojrctService {

	@Autowired
	private MesMaterielCheckprojrctMapper mesMaterielCheckprojrctMapper;
	@Autowired
	private MesCheckprojectInfoMapper mesCheckprojectInfoMapper;
	
	@Override
	@Transactional
	public void delMain(String id) {
		mesCheckprojectInfoMapper.deleteByMainId(id);
		mesMaterielCheckprojrctMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesCheckprojectInfoMapper.deleteByMainId(id.toString());
			mesMaterielCheckprojrctMapper.deleteById(id);
		}
	}
	
}
