package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.PatrolInspectLog;
import org.jeecg.modules.mes.inspect.mapper.PatrolInspectLogMapper;
import org.jeecg.modules.mes.inspect.service.IPatrolInspectLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 巡检模块记录
 * @Author: jeecg-boot
 * @Date:   2021-03-21
 * @Version: V1.0
 */
@Service
public class PatrolInspectLogServiceImpl extends ServiceImpl<PatrolInspectLogMapper, PatrolInspectLog> implements IPatrolInspectLogService {

}
