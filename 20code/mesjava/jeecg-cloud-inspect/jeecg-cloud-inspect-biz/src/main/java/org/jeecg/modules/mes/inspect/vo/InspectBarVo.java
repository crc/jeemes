package org.jeecg.modules.mes.inspect.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class InspectBarVo implements Serializable {

    public static final String LPL_HZ = "_FPYR";
    public static final String TOP_HZ = " TOP issue";
    public static final String BLL_HZ = "_Fail rate";
    public static final String CHART_HZ = " trend chart";
    public static final String SMT_GOAL = "0.99";
    public static final String DIP_GOAL = "0.99";
    public static final String ASS_GOAL = "0.99";
    public static final String SMT_GOAL_PER = "99%";
    public static final String DIP_GOAL_PER = "98%";
    public static final String ASS_GOAL_PER = "97%";

    //x轴数组
    private List<String> xname;
    //柱状图数据
    private List<String> bar;
    //折线1数据
    private List<String> line1;
    //折线2数据
    private List<String> line2;
    //柱状图名称
    private String barname;
    //折线1名称
    private String line1name;
    //折线2名称
    private String line2name;
    //总体名称
    private String name;

}
