package org.jeecg.modules.mes.inspect.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ReportVo implements Serializable {
    private String attr1;
    private String attr2;
    private String attr3;
    private String attr4;
    private String attr5;
    private String attr6;
    private String attr7;
    private String attr8;
    private String attr9;
    private String attr10;
    private String attr11;
    private String attr12;
    private String attr13;
    private String attr14;
    private String attr15;
}
