package org.jeecg.modules.mes.iqc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.mes.iqc.entity.MesIqcDefect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 来料检验缺陷描述
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
public interface MesIqcDefectMapper extends BaseMapper<MesIqcDefect> {

    @Select("select * from mes_iqc_defect where iqc_material_id=#{mainId}")
    public List<MesIqcDefect> selectDefectByMainId(@Param("mainId") String mainId);

}
