package org.jeecg.modules.mes.iqc.service.impl;

import org.jeecg.modules.mes.iqc.entity.MesIqcDefect;
import org.jeecg.modules.mes.iqc.mapper.MesIqcDefectMapper;
import org.jeecg.modules.mes.iqc.service.IMesIqcDefectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 来料检验缺陷描述
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
@Service
public class MesIqcDefectServiceImpl extends ServiceImpl<MesIqcDefectMapper, MesIqcDefect> implements IMesIqcDefectService {

}
