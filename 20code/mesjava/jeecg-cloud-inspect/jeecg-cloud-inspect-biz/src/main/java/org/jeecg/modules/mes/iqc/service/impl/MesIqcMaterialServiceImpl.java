package org.jeecg.modules.mes.iqc.service.impl;

import org.jeecg.modules.mes.iqc.entity.MesIqcMaterial;
import org.jeecg.modules.mes.iqc.mapper.MesIqcMaterialMapper;
import org.jeecg.modules.mes.iqc.service.IMesIqcMaterialService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 来料检验物料详情
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
@Service
public class MesIqcMaterialServiceImpl extends ServiceImpl<MesIqcMaterialMapper, MesIqcMaterial> implements IMesIqcMaterialService {

}
