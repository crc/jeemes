package org.jeecg.modules.mes.machineFile.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionSpi;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: mes_file_collection_spi
 * @Author: jeecg-boot
 * @Date:   2021-04-16
 * @Version: V1.0
 */
public interface MesFileCollectionSpiMapper extends BaseMapper<MesFileCollectionSpi> {

}
