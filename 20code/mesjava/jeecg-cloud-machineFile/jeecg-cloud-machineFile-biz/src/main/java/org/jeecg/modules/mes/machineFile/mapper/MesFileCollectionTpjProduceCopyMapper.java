package org.jeecg.modules.mes.machineFile.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjProduceCopy;

/**
 * @Description: mes_file_collection_tpj_produce_copy
 * @Author: jeecg-boot
 * @Date:   2021-05-31
 * @Version: V1.0
 */
public interface MesFileCollectionTpjProduceCopyMapper extends BaseMapper<MesFileCollectionTpjProduceCopy> {

}
