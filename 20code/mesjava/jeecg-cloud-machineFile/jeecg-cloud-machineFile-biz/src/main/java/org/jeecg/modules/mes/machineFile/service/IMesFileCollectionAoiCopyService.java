package org.jeecg.modules.mes.machineFile.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionAoiCopy;

/**
 * @Description: mes_file_collection_aoi_copy
 * @Author: jeecg-boot
 * @Date:   2021-06-01
 * @Version: V1.0
 */
public interface IMesFileCollectionAoiCopyService extends IService<MesFileCollectionAoiCopy> {

}
