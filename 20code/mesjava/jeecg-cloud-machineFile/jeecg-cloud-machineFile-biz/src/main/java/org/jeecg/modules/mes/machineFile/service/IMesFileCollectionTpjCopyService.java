package org.jeecg.modules.mes.machineFile.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjCopy;

/**
 * @Description: mes_file_collection_tpj_copy
 * @Author: jeecg-boot
 * @Date:   2021-05-31
 * @Version: V1.0
 */
public interface IMesFileCollectionTpjCopyService extends IService<MesFileCollectionTpjCopy> {

}
