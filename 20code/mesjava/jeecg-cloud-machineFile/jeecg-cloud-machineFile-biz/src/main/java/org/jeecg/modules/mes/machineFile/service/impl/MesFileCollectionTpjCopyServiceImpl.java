package org.jeecg.modules.mes.machineFile.service.impl;


import org.jeecg.modules.mes.machineFile.mapper.MesFileCollectionTpjCopyMapper;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionTpjCopyService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjCopy;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: mes_file_collection_tpj_copy
 * @Author: jeecg-boot
 * @Date:   2021-05-31
 * @Version: V1.0
 */
@Service
public class MesFileCollectionTpjCopyServiceImpl extends ServiceImpl<MesFileCollectionTpjCopyMapper, MesFileCollectionTpjCopy> implements IMesFileCollectionTpjCopyService {

}
