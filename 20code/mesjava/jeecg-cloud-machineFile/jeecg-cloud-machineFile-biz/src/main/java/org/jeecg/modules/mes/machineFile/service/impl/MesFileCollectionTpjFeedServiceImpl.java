package org.jeecg.modules.mes.machineFile.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mes.machineFile.mapper.MesFileCollectionTpjFeedMapper;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionTpjFeedService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjFeed;
import org.springframework.stereotype.Service;

/**
 * @Description: mes_file_collection_tpj_feed
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Service
public class MesFileCollectionTpjFeedServiceImpl extends ServiceImpl<MesFileCollectionTpjFeedMapper, MesFileCollectionTpjFeed> implements IMesFileCollectionTpjFeedService {

}
