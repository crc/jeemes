package org.jeecg.modules.mes.machineFile.service.impl;

import cn.hutool.core.util.XmlUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.XML;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.mes.machineFile.mapper.MesFileCollectionTpjMapper;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionTpjCopyService;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionTpjFeedService;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionTpjHeadService;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionTpjProduceCopyService;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionTpjProduceService;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionTpjService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpj;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjCopy;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjFeed;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjHead;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjProduce;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjProduceCopy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: mes_file_collection_tpj
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Service
public class MesFileCollectionTpjServiceImpl extends ServiceImpl<MesFileCollectionTpjMapper, MesFileCollectionTpj> implements IMesFileCollectionTpjService {



    @Autowired
    private IMesFileCollectionTpjProduceService mesFileCollectionTpjProduceService;
    @Autowired
    private IMesFileCollectionTpjFeedService mesFileCollectionTpjFeedService;
    @Autowired
    private IMesFileCollectionTpjHeadService mesFileCollectionTpjHeadService;
    @Autowired
    private IMesFileCollectionTpjCopyService mesFileCollectionTpjCopyService;
    @Autowired
    private IMesFileCollectionTpjProduceCopyService mesFileCollectionTpjProduceCopyService;

    /**
     * 接收贴片机文件
     * @param request request
     * @param line 产线
     */
    @Override
    public void receiveTpjFile(HttpServletRequest request, String line) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
//        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
//            MultipartFile file = entity.getValue();
//            try {
//                readTpjFile(file,line);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }


        String fileTime = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String inspectFrequen="夜班";
        if(DateUtils.JudgeTime("08:00:00","20:00:00")) {//是不是白班
            inspectFrequen="白班";
        }
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            MultipartFile file = entity.getValue();
            try {
                readTpjFileCopy(file,line,inspectFrequen,fileTime);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Transactional
    public void readTpjFile(MultipartFile file,String line) throws IOException {

        JSONObject json = XML.toJSONObject(XmlUtil.toStr(XmlUtil.readXML(file.getInputStream())));
        JSONObject totalObject = json.getJSONObject("TotalProductionInfo");
        MesFileCollectionTpj fileCollectionTpj = new MesFileCollectionTpj();
        fileCollectionTpj.setLine(line);
        fileCollectionTpj.setMachineName(totalObject.getStr("MachineName"));
        fileCollectionTpj.setMachineFunction(totalObject.getStr("MachineFunction"));
        fileCollectionTpj.setProductionProgramName(totalObject.getStr("ProductionProgramName"));
        fileCollectionTpj.setProductionNum(totalObject.getStr("ProductionNum"));
        fileCollectionTpj.setTotalPlacementPoint(totalObject.getStr("TotalPlacementPoint"));
        this.save(fileCollectionTpj);
        Object produce = totalObject.get("ProductionManageTotalInfo");
        if (produce instanceof JSONObject) {
            JSONObject produceObject = totalObject.getJSONObject("ProductionManageTotalInfo");
            MesFileCollectionTpjProduce fileCollectionTpjProduce = new MesFileCollectionTpjProduce();
            fileCollectionTpjProduce.setMainId(fileCollectionTpj.getId());
            fileCollectionTpjProduce.setStationId(produceObject.getStr("StationId"));
            fileCollectionTpjProduce.setCnvrLaneId(produceObject.getStr("CnvrLaneId"));
            fileCollectionTpjProduce.setProductNum(produceObject.getStr("ProductNum"));
            fileCollectionTpjProduce.setProductCir(produceObject.getStr("ProductCir"));
            fileCollectionTpjProduce.setBadMarkNum(produceObject.getStr("BadMarkNum"));
            fileCollectionTpjProduce.setBocMarkNum(produceObject.getStr("BocMarkNum"));
            fileCollectionTpjProduce.setAreaMarkNum(produceObject.getStr("AreaMarkNum"));
            fileCollectionTpjProduce.setTimeRun(produceObject.getStr("TimeRun"));
            fileCollectionTpjProduce.setTimePwb(produceObject.getStr("TimePwb"));
            fileCollectionTpjProduce.setTimePwbIn(produceObject.getStr("TimePwbIn"));
            fileCollectionTpjProduce.setTimePwbOut(produceObject.getStr("TimePwbOut"));
            fileCollectionTpjProduce.setTimeRepair(produceObject.getStr("TimeRepair"));
            fileCollectionTpjProduce.setTimeTrouble(produceObject.getStr("TimeTrouble"));
            fileCollectionTpjProduce.setTimeNoCompo(produceObject.getStr("TimeNoCompo"));
            fileCollectionTpjProduce.setTimeDown(produceObject.getStr("TimeDown"));
            mesFileCollectionTpjProduceService.save(fileCollectionTpjProduce);
        }else if (produce instanceof JSONArray) {
            JSONArray produceArray = totalObject.getJSONArray("ProductionManageTotalInfo");
            List<MesFileCollectionTpjProduce> mesFileCollectionTpjProduces = new ArrayList<>();
            for (int i = 0; i < produceArray.size(); i++) {
                JSONObject produceObject = produceArray.getJSONObject(i);
                MesFileCollectionTpjProduce fileCollectionTpjProduce = new MesFileCollectionTpjProduce();
                fileCollectionTpjProduce.setMainId(fileCollectionTpj.getId());
                fileCollectionTpjProduce.setStationId(produceObject.getStr("StationId"));
                fileCollectionTpjProduce.setCnvrLaneId(produceObject.getStr("CnvrLaneId"));
                fileCollectionTpjProduce.setProductNum(produceObject.getStr("ProductNum"));
                fileCollectionTpjProduce.setProductCir(produceObject.getStr("ProductCir"));
                fileCollectionTpjProduce.setBadMarkNum(produceObject.getStr("BadMarkNum"));
                fileCollectionTpjProduce.setBocMarkNum(produceObject.getStr("BocMarkNum"));
                fileCollectionTpjProduce.setAreaMarkNum(produceObject.getStr("AreaMarkNum"));
                fileCollectionTpjProduce.setTimeRun(produceObject.getStr("TimeRun"));
                fileCollectionTpjProduce.setTimePwb(produceObject.getStr("TimePwb"));
                fileCollectionTpjProduce.setTimePwbIn(produceObject.getStr("TimePwbIn"));
                fileCollectionTpjProduce.setTimePwbOut(produceObject.getStr("TimePwbOut"));
                fileCollectionTpjProduce.setTimeRepair(produceObject.getStr("TimeRepair"));
                fileCollectionTpjProduce.setTimeTrouble(produceObject.getStr("TimeTrouble"));
                fileCollectionTpjProduce.setTimeNoCompo(produceObject.getStr("TimeNoCompo"));
                fileCollectionTpjProduce.setTimeDown(produceObject.getStr("TimeDown"));
                mesFileCollectionTpjProduces.add(fileCollectionTpjProduce);
            }
            if (mesFileCollectionTpjProduces.size() > 0) {
                mesFileCollectionTpjProduceService.saveBatch(mesFileCollectionTpjProduces);
            }
        }



        Object feed = totalObject.get("FeederProductionManageInfo");
        if (feed instanceof JSONArray) {
            JSONArray feedArray = totalObject.getJSONArray("FeederProductionManageInfo");
            List<MesFileCollectionTpjFeed> feedList = new ArrayList<>();
            if (feedArray.size() > 0) {
                for (int i = 0; i < feedArray.size(); i++) {
                    JSONObject feedObject = feedArray.getJSONObject(i);
                    MesFileCollectionTpjFeed mesFileCollectionTpjFeed = new MesFileCollectionTpjFeed();
                    mesFileCollectionTpjFeed.setMainId(fileCollectionTpj.getId());
                    mesFileCollectionTpjFeed.setStationId(feedObject.getStr("StationId"));
                    mesFileCollectionTpjFeed.setCnvrLaneId(feedObject.getStr("CnvrLaneId"));
                    mesFileCollectionTpjFeed.setFeederBankPosition(feedObject.getStr("FeederBankPosition"));
                    mesFileCollectionTpjFeed.setFeederType(feedObject.getStr("FeederType"));
                    mesFileCollectionTpjFeed.setHoleNo(feedObject.getStr("HoleNo"));
                    mesFileCollectionTpjFeed.setLaneNo(feedObject.getStr("LaneNo"));
                    mesFileCollectionTpjFeed.setComponentName(feedObject.getStr("ComponentName"));
                    mesFileCollectionTpjFeed.setInitialNum(feedObject.getStr("InitialNum"));
                    mesFileCollectionTpjFeed.setRemainingQuantity(feedObject.getStr("RemainingQuantity"));
                    mesFileCollectionTpjFeed.setWarningLevel(feedObject.getStr("WarningLevel"));
                    mesFileCollectionTpjFeed.setPickedNum(feedObject.getStr("PickedNum"));
                    mesFileCollectionTpjFeed.setPlacedNum(feedObject.getStr("PlacedNum"));
                    mesFileCollectionTpjFeed.setPickErrorNum(feedObject.getStr("PickErrorNum"));
                    mesFileCollectionTpjFeed.setNoCompoNum(feedObject.getStr("NoCompoNum"));
                    mesFileCollectionTpjFeed.setDiffCompoErrorNum(feedObject.getStr("DiffCompoErrorNum"));
                    mesFileCollectionTpjFeed.setPickRetryOverNum(feedObject.getStr("PickRetryOverNum"));
                    mesFileCollectionTpjFeed.setLaserErrorNum(feedObject.getStr("LaserErrorNum"));
                    mesFileCollectionTpjFeed.setVisionErrorNum(feedObject.getStr("VisionErrorNum"));
                    mesFileCollectionTpjFeed.setCoplaErrorNum(feedObject.getStr("CoplaErrorNum"));
                    mesFileCollectionTpjFeed.setVerifyErrorNum(feedObject.getStr("VerifyErrorNum"));
                    mesFileCollectionTpjFeed.setTombErrorNum(feedObject.getStr("TombErrorNum"));
                    mesFileCollectionTpjFeed.setLeadBendErrorNum(feedObject.getStr("LeadBendErrorNum"));
                    mesFileCollectionTpjFeed.setOtherErrorNum(feedObject.getStr("OtherErrorNum"));
                    mesFileCollectionTpjFeed.setScrapNum(feedObject.getStr("ScrapNum"));
                    mesFileCollectionTpjFeed.setPickPosErrorNum(feedObject.getStr("PickPosErrorNum"));
                    mesFileCollectionTpjFeed.setPosCompoNum(feedObject.getStr("PosCompoNum"));
                    mesFileCollectionTpjFeed.setAngleCompoNum(feedObject.getStr("AngleCompoNum"));
                    mesFileCollectionTpjFeed.setTakeoutCompoNum(feedObject.getStr("TakeoutCompoNum"));
                    mesFileCollectionTpjFeed.setFallCompoNum(feedObject.getStr("FallCompoNum"));
                    feedList.add(mesFileCollectionTpjFeed);
                }
                mesFileCollectionTpjFeedService.saveBatch(feedList);
            }
        }else if (feed instanceof JSONObject) {
            JSONObject feedObject = totalObject.getJSONObject("FeederProductionManageInfo");
            MesFileCollectionTpjFeed mesFileCollectionTpjFeed = new MesFileCollectionTpjFeed();
            mesFileCollectionTpjFeed.setMainId(fileCollectionTpj.getId());
            mesFileCollectionTpjFeed.setStationId(feedObject.getStr("StationId"));
            mesFileCollectionTpjFeed.setCnvrLaneId(feedObject.getStr("CnvrLaneId"));
            mesFileCollectionTpjFeed.setFeederBankPosition(feedObject.getStr("FeederBankPosition"));
            mesFileCollectionTpjFeed.setFeederType(feedObject.getStr("FeederType"));
            mesFileCollectionTpjFeed.setHoleNo(feedObject.getStr("HoleNo"));
            mesFileCollectionTpjFeed.setLaneNo(feedObject.getStr("LaneNo"));
            mesFileCollectionTpjFeed.setComponentName(feedObject.getStr("ComponentName"));
            mesFileCollectionTpjFeed.setInitialNum(feedObject.getStr("InitialNum"));
            mesFileCollectionTpjFeed.setRemainingQuantity(feedObject.getStr("RemainingQuantity"));
            mesFileCollectionTpjFeed.setWarningLevel(feedObject.getStr("WarningLevel"));
            mesFileCollectionTpjFeed.setPickedNum(feedObject.getStr("PickedNum"));
            mesFileCollectionTpjFeed.setPlacedNum(feedObject.getStr("PlacedNum"));
            mesFileCollectionTpjFeed.setPickErrorNum(feedObject.getStr("PickErrorNum"));
            mesFileCollectionTpjFeed.setNoCompoNum(feedObject.getStr("NoCompoNum"));
            mesFileCollectionTpjFeed.setDiffCompoErrorNum(feedObject.getStr("DiffCompoErrorNum"));
            mesFileCollectionTpjFeed.setPickRetryOverNum(feedObject.getStr("PickRetryOverNum"));
            mesFileCollectionTpjFeed.setLaserErrorNum(feedObject.getStr("LaserErrorNum"));
            mesFileCollectionTpjFeed.setVisionErrorNum(feedObject.getStr("VisionErrorNum"));
            mesFileCollectionTpjFeed.setCoplaErrorNum(feedObject.getStr("CoplaErrorNum"));
            mesFileCollectionTpjFeed.setVerifyErrorNum(feedObject.getStr("VerifyErrorNum"));
            mesFileCollectionTpjFeed.setTombErrorNum(feedObject.getStr("TombErrorNum"));
            mesFileCollectionTpjFeed.setLeadBendErrorNum(feedObject.getStr("LeadBendErrorNum"));
            mesFileCollectionTpjFeed.setOtherErrorNum(feedObject.getStr("OtherErrorNum"));
            mesFileCollectionTpjFeed.setScrapNum(feedObject.getStr("ScrapNum"));
            mesFileCollectionTpjFeed.setPickPosErrorNum(feedObject.getStr("PickPosErrorNum"));
            mesFileCollectionTpjFeed.setPosCompoNum(feedObject.getStr("PosCompoNum"));
            mesFileCollectionTpjFeed.setAngleCompoNum(feedObject.getStr("AngleCompoNum"));
            mesFileCollectionTpjFeed.setTakeoutCompoNum(feedObject.getStr("TakeoutCompoNum"));
            mesFileCollectionTpjFeed.setFallCompoNum(feedObject.getStr("FallCompoNum"));
            mesFileCollectionTpjFeedService.save(mesFileCollectionTpjFeed);
        }


        JSONArray headArray = totalObject.getJSONArray("HeadManageInfo");
        List<MesFileCollectionTpjHead> headList = new ArrayList<>();
        int pickTotalNum = 0;
        int placeTotalNum = 0;
        if (headArray.size() > 0) {
            for (int i = 0; i < headArray.size(); i++) {
                JSONObject headObject = headArray.getJSONObject(i);
                MesFileCollectionTpjHead mesFileCollectionTpjHead = new MesFileCollectionTpjHead();
                mesFileCollectionTpjHead.setLine(line);
                mesFileCollectionTpjHead.setMainId(fileCollectionTpj.getId());
                mesFileCollectionTpjHead.setStationId(headObject.getStr("StationId"));
                mesFileCollectionTpjHead.setCnvrLaneId(headObject.getStr("CnvrLaneId"));
                mesFileCollectionTpjHead.setHeadNo(headObject.getStr("HeadNo"));
                mesFileCollectionTpjHead.setHeadId(headObject.getStr("HeadId"));
                mesFileCollectionTpjHead.setPickedNum(headObject.getStr("PickedNum"));
                mesFileCollectionTpjHead.setPlacedNum(headObject.getStr("PlacedNum"));
                mesFileCollectionTpjHead.setPickErrorNum(headObject.getStr("PickErrorNum"));
                mesFileCollectionTpjHead.setFallCompoNum(headObject.getStr("FallCompoNum"));
                mesFileCollectionTpjHead.setTakeoutCompoNum(headObject.getStr("TakeoutCompoNum"));
                mesFileCollectionTpjHead.setLaserErrorNum(headObject.getStr("LaserErrorNum"));
                mesFileCollectionTpjHead.setDiffCompoErrorNum(headObject.getStr("DiffCompoErrorNum"));
                mesFileCollectionTpjHead.setTombErrorNum(headObject.getStr("TombErrorNum"));
                mesFileCollectionTpjHead.setPosCompoNum(headObject.getStr("PosCompoNum"));
                mesFileCollectionTpjHead.setAngleCompoNum(headObject.getStr("AngleCompoNum"));
                mesFileCollectionTpjHead.setNoCompoNum(headObject.getStr("NoCompoNum"));
                mesFileCollectionTpjHead.setPickRetryOverNum(headObject.getStr("PickRetryOverNum"));
                mesFileCollectionTpjHead.setVisionErrorNum(headObject.getStr("VisionErrorNum"));
                mesFileCollectionTpjHead.setCoplaErrorNum(headObject.getStr("CoplaErrorNum"));
                mesFileCollectionTpjHead.setVerifyErrorNum(headObject.getStr("VerifyErrorNum"));
                mesFileCollectionTpjHead.setLeadBendErrorNum(headObject.getStr("LeadBendErrorNum"));
                mesFileCollectionTpjHead.setPickPosErrorNum(headObject.getStr("PickPosErrorNum"));
                mesFileCollectionTpjHead.setScrapNum(headObject.getStr("ScrapNum"));
                mesFileCollectionTpjHead.setOtherErrorNum(headObject.getStr("OtherErrorNum"));
                mesFileCollectionTpjHead.setHeadDeviceType(headObject.getStr("HeadDeviceType"));
                headList.add(mesFileCollectionTpjHead);
                pickTotalNum += Integer.parseInt(mesFileCollectionTpjHead.getPickedNum());
                placeTotalNum += Integer.parseInt(mesFileCollectionTpjHead.getPlacedNum());
            }
            mesFileCollectionTpjHeadService.saveBatch(headList);

        }
        //更新合计数量
        MesFileCollectionTpj mesFileCollectionTpj = new MesFileCollectionTpj();
        mesFileCollectionTpj.setId(fileCollectionTpj.getId());
        mesFileCollectionTpj.setQuery1(pickTotalNum+"");
        mesFileCollectionTpj.setQuery2(placeTotalNum+"");
        this.updateById(mesFileCollectionTpj);

    }

    @Transactional
    public void readTpjFileCopy(MultipartFile file,String line,String groupClass,String fileTime) throws IOException {
        //查询今天,当前线，当前班是否有数据
        MesFileCollectionTpjCopy mesFileCollectionTpjCopy = null;
        MesFileCollectionTpjProduceCopy mesFileCollectionTpjProduceCopy = null;
        boolean insert = true;
        MesFileCollectionTpjCopy mesFileCollectionTpjCopySelect = mesFileCollectionTpjCopyService.lambdaQuery()
                .eq(MesFileCollectionTpjCopy::getGroupClass,groupClass)
                .eq(MesFileCollectionTpjCopy::getFileTime,fileTime)
                .eq(MesFileCollectionTpjCopy::getLine,line)
                .one();
        if (mesFileCollectionTpjCopySelect == null) {
            mesFileCollectionTpjCopy = new MesFileCollectionTpjCopy();
            mesFileCollectionTpjCopy.setLine(line);
            mesFileCollectionTpjCopy.setGroupClass(groupClass);
            mesFileCollectionTpjCopy.setFileTime(fileTime);
            mesFileCollectionTpjCopy.setHeadPickNum("0");
            mesFileCollectionTpjCopy.setHeadPlaceNum("0");

            mesFileCollectionTpjProduceCopy = new MesFileCollectionTpjProduceCopy();
            mesFileCollectionTpjProduceCopy.setTimeRun("0");
            mesFileCollectionTpjProduceCopy.setTimePwb("0");
            mesFileCollectionTpjProduceCopy.setTimePwbIn("0");
            mesFileCollectionTpjProduceCopy.setTimePwbOut("0");
            mesFileCollectionTpjProduceCopy.setTimeRepair("0");
            mesFileCollectionTpjProduceCopy.setTimeTrouble("0");
            mesFileCollectionTpjProduceCopy.setTimeNoCompo("0");
            mesFileCollectionTpjProduceCopy.setTimeDown("0");
        }else {
            mesFileCollectionTpjCopy = mesFileCollectionTpjCopySelect;
            mesFileCollectionTpjProduceCopy = mesFileCollectionTpjProduceCopyService.lambdaQuery().
                    eq(MesFileCollectionTpjProduceCopy::getMainId,mesFileCollectionTpjCopy.getId())
                    .one();
            insert = false;
        }


        JSONObject json = XML.toJSONObject(XmlUtil.toStr(XmlUtil.readXML(file.getInputStream())));
        JSONObject totalObject = json.getJSONObject("TotalProductionInfo");
        //计算吸取数和贴片数
        JSONArray headArray = totalObject.getJSONArray("HeadManageInfo");
        Integer pickTotalNum = Integer.parseInt(mesFileCollectionTpjCopy.getHeadPickNum());
        Integer placeTotalNum = Integer.parseInt(mesFileCollectionTpjCopy.getHeadPlaceNum());
        if (headArray.size() > 0) {
            for (int i = 0; i < headArray.size(); i++) {
                JSONObject headObject = headArray.getJSONObject(i);
                pickTotalNum += Integer.parseInt(headObject.getStr("PickedNum"));
                placeTotalNum += Integer.parseInt(headObject.getStr("PlacedNum"));
            }
        }
        //新增 或者更新
        mesFileCollectionTpjCopy.setHeadPickNum(pickTotalNum+"");
        mesFileCollectionTpjCopy.setHeadPlaceNum(placeTotalNum+"");
        if (insert) {
            mesFileCollectionTpjCopyService.save(mesFileCollectionTpjCopy);
            mesFileCollectionTpjProduceCopy.setMainId(mesFileCollectionTpjCopy.getId());
        }else {
            mesFileCollectionTpjCopyService.updateById(mesFileCollectionTpjCopy);
        }

        //解析生产数据
        Object produce = totalObject.get("ProductionManageTotalInfo");
        if (produce instanceof JSONObject) {
            JSONObject produceObject = totalObject.getJSONObject("ProductionManageTotalInfo");

            String[] timeRunArrays = produceObject.getStr("TimeRun").split(":");
            int timeRunCount = Integer.parseInt(timeRunArrays[0]) * 60 * 60
                    + Integer.parseInt(timeRunArrays[1]) * 60
                    + Integer.parseInt(timeRunArrays[2]);
            mesFileCollectionTpjProduceCopy
                    .setTimeRun(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimeRun()) +timeRunCount +"");

            String[] timePwbRunArrays = produceObject.getStr("TimePwb").split(":");
            int timePwbCount = Integer.parseInt(timePwbRunArrays[0]) * 60 * 60
                    + Integer.parseInt(timePwbRunArrays[1]) * 60
                    + Integer.parseInt(timePwbRunArrays[2]);
            mesFileCollectionTpjProduceCopy
                    .setTimePwb(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimePwb())+timePwbCount + "");

            String[] timePwbInArrays = produceObject.getStr("TimePwbIn").split(":");
            int timePwbInCount = Integer.parseInt(timePwbInArrays[0]) * 60 * 60
                    + Integer.parseInt(timePwbInArrays[1]) * 60
                    + Integer.parseInt(timePwbInArrays[2]);
            mesFileCollectionTpjProduceCopy
                    .setTimePwbIn(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimePwbIn())+timePwbInCount + "");

            String[] timePwbOutArrays = produceObject.getStr("TimePwbOut").split(":");
            int timePwbOutCount = Integer.parseInt(timePwbOutArrays[0]) * 60 * 60
                    + Integer.parseInt(timePwbOutArrays[1]) * 60
                    + Integer.parseInt(timePwbOutArrays[2]);
            mesFileCollectionTpjProduceCopy
                    .setTimePwbOut(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimePwbOut())+timePwbOutCount + "");

            String[] timeRepairArrays = produceObject.getStr("TimeRepair").split(":");
            int timeRepairCount = Integer.parseInt(timeRepairArrays[0]) * 60 * 60
                    + Integer.parseInt(timeRepairArrays[1]) * 60
                    + Integer.parseInt(timeRepairArrays[2]);
            mesFileCollectionTpjProduceCopy
                    .setTimeRepair(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimeRepair())+timeRepairCount +"");

            String[] timeTroubleArrays = produceObject.getStr("TimeTrouble").split(":");
            int timeTroubleCount = Integer.parseInt(timeTroubleArrays[0]) * 60 * 60
                    + Integer.parseInt(timeTroubleArrays[1]) * 60
                    + Integer.parseInt(timeTroubleArrays[2]);
            mesFileCollectionTpjProduceCopy
                    .setTimeTrouble(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimeTrouble())+timeTroubleCount + "");

            String[] timeNoCompoArrays = produceObject.getStr("TimeNoCompo").split(":");
            int timeNoCompoCount = Integer.parseInt(timeNoCompoArrays[0]) * 60 * 60
                    + Integer.parseInt(timeNoCompoArrays[1]) * 60
                    + Integer.parseInt(timeNoCompoArrays[2]);
            mesFileCollectionTpjProduceCopy
                    .setTimeNoCompo(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimeNoCompo())+timeNoCompoCount + "");

            String[] timeDownArrays = produceObject.getStr("TimeDown").split(":");
            int timeDownCount = Integer.parseInt(timeDownArrays[0]) * 60 * 60
                    + Integer.parseInt(timeDownArrays[1]) * 60
                    + Integer.parseInt(timeDownArrays[2]);
            mesFileCollectionTpjProduceCopy
                    .setTimeDown(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimeDown())+timeDownCount + "");

        }else if (produce instanceof JSONArray) {
            JSONArray produceArray = totalObject.getJSONArray("ProductionManageTotalInfo");
            for (int i = 0; i < produceArray.size(); i++) {
                JSONObject produceObject = produceArray.getJSONObject(i);

                String[] timeRunArrays = produceObject.getStr("TimeRun").split(":");
                int timeRunCount = Integer.parseInt(timeRunArrays[0]) * 60 * 60
                        + Integer.parseInt(timeRunArrays[1]) * 60
                        + Integer.parseInt(timeRunArrays[2]);
                mesFileCollectionTpjProduceCopy
                        .setTimeRun(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimeRun()) +timeRunCount +"");

                String[] timePwbRunArrays = produceObject.getStr("TimePwb").split(":");
                int timePwbCount = Integer.parseInt(timePwbRunArrays[0]) * 60 * 60
                        + Integer.parseInt(timePwbRunArrays[1]) * 60
                        + Integer.parseInt(timePwbRunArrays[2]);
                mesFileCollectionTpjProduceCopy
                        .setTimePwb(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimePwb())+timePwbCount + "");

                String[] timePwbInArrays = produceObject.getStr("TimePwbIn").split(":");
                int timePwbInCount = Integer.parseInt(timePwbInArrays[0]) * 60 * 60
                        + Integer.parseInt(timePwbInArrays[1]) * 60
                        + Integer.parseInt(timePwbInArrays[2]);
                mesFileCollectionTpjProduceCopy
                        .setTimePwbIn(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimePwbIn())+timePwbInCount + "");

                String[] timePwbOutArrays = produceObject.getStr("TimePwbOut").split(":");
                int timePwbOutCount = Integer.parseInt(timePwbOutArrays[0]) * 60 * 60
                        + Integer.parseInt(timePwbOutArrays[1]) * 60
                        + Integer.parseInt(timePwbOutArrays[2]);
                mesFileCollectionTpjProduceCopy
                        .setTimePwbOut(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimePwbOut())+timePwbOutCount + "");

                String[] timeRepairArrays = produceObject.getStr("TimeRepair").split(":");
                int timeRepairCount = Integer.parseInt(timeRepairArrays[0]) * 60 * 60
                        + Integer.parseInt(timeRepairArrays[1]) * 60
                        + Integer.parseInt(timeRepairArrays[2]);
                mesFileCollectionTpjProduceCopy
                        .setTimeRepair(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimeRepair())+timeRepairCount +"");

                String[] timeTroubleArrays = produceObject.getStr("TimeTrouble").split(":");
                int timeTroubleCount = Integer.parseInt(timeTroubleArrays[0]) * 60 * 60
                        + Integer.parseInt(timeTroubleArrays[1]) * 60
                        + Integer.parseInt(timeTroubleArrays[2]);
                mesFileCollectionTpjProduceCopy
                        .setTimeTrouble(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimeTrouble())+timeTroubleCount + "");

                String[] timeNoCompoArrays = produceObject.getStr("TimeNoCompo").split(":");
                int timeNoCompoCount = Integer.parseInt(timeNoCompoArrays[0]) * 60 * 60
                        + Integer.parseInt(timeNoCompoArrays[1]) * 60
                        + Integer.parseInt(timeNoCompoArrays[2]);
                mesFileCollectionTpjProduceCopy
                        .setTimeNoCompo(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimeNoCompo())+timeNoCompoCount + "");

                String[] timeDownArrays = produceObject.getStr("TimeDown").split(":");
                int timeDownCount = Integer.parseInt(timeDownArrays[0]) * 60 * 60
                        + Integer.parseInt(timeDownArrays[1]) * 60
                        + Integer.parseInt(timeDownArrays[2]);
                mesFileCollectionTpjProduceCopy
                        .setTimeDown(Integer.parseInt(mesFileCollectionTpjProduceCopy.getTimeDown())+timeDownCount + "");
            }

        }
        if (insert) {
            mesFileCollectionTpjProduceCopyService.save(mesFileCollectionTpjProduceCopy);
        }else {
            mesFileCollectionTpjProduceCopyService.updateById(mesFileCollectionTpjProduceCopy);
        }



    }


}
