package org.jeecg.modules.mes.client;

import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.order.entity.MesOrderProduce;
import org.jeecg.modules.mes.order.entity.MesProduceItem;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
@FeignClient(contextId = "TransactionServiceClient", value = ServiceNameConstants.TRANSACTION_SERVICE)
public interface TransactionClient {


    @GetMapping("order/mesOrderProduce/queryMesProduceById")
    public MesOrderProduce queryMesProduceById(@RequestParam(name="id",required=true) String id);

    @GetMapping("order/mesOrderProduce/listMesProduceItemByOrderId")
    public List<MesProduceItem> listMesProduceItemByOrderId(@RequestParam(name="orderId",required=true) String orderId);

    @GetMapping("order/mesOrderProduce/listMesProduceItemBom")
    public List<MesProduceItem> listMesProduceItemBom(@RequestParam(name = "commandid", required = false) String commandid);

    @GetMapping("order/mesOrderProduce/listProduceOrderItem")
    public List<MesProduceItem> listProduceOrderItem(@RequestParam(name = "produceId", required = false) String produceId,
                                                     @RequestParam(name = "produceGrade",required = false) String produceGrade,
                                                     @RequestParam(name = "ifDelivery",required = false) String ifDelivery,
                                                     @RequestParam(name = "ifGlaze",required = false) String ifGlaze,
                                                     @RequestParam(name = "planNum",required = false) String planNum);

    /**
     * 通过成品料号和线别获取生产订单信息
     * @param materielCode  成品料号
     * @param lineSort  线别
     * @return
     */
    @GetMapping("order/mesOrderProduce/getOrderProduceList")
    public List<MesOrderProduce> getOrderProduceList(@RequestParam(name = "materielCode", required = true)String materielCode,
                                                     @RequestParam(name = "lineSort", required = true)String lineSort);

    @PostMapping("order/mesOrderProduce/updatemesOrderProduce")
    public boolean updatemesOrderProduce(MesOrderProduce mesOrderProduce);
}
