package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.FirstPieceSmtLog;
import org.jeecg.modules.mes.produce.service.IFirstPieceSmtLogService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: SMT首件确认表-检查内容
 * @Author: jeecg-boot
 * @Date:   2021-03-22
 * @Version: V1.0
 */
@Api(tags="SMT首件确认表-检查内容")
@RestController
@RequestMapping("/produce/firstPieceSmtLog")
@Slf4j
public class FirstPieceSmtLogController extends JeecgController<FirstPieceSmtLog, IFirstPieceSmtLogService> {
	@Autowired
	private IFirstPieceSmtLogService firstPieceSmtLogService;
	
	/**
	 * 分页列表查询
	 *
	 * @param firstPieceSmtLog
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "SMT首件确认表-检查内容-分页列表查询")
	@ApiOperation(value="SMT首件确认表-检查内容-分页列表查询", notes="SMT首件确认表-检查内容-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(FirstPieceSmtLog firstPieceSmtLog,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<FirstPieceSmtLog> queryWrapper = QueryGenerator.initQueryWrapper(firstPieceSmtLog, req.getParameterMap());
		Page<FirstPieceSmtLog> page = new Page<FirstPieceSmtLog>(pageNo, pageSize);
		IPage<FirstPieceSmtLog> pageList = firstPieceSmtLogService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param firstPieceSmtLog
	 * @return
	 */
	@AutoLog(value = "SMT首件确认表-检查内容-添加")
	@ApiOperation(value="SMT首件确认表-检查内容-添加", notes="SMT首件确认表-检查内容-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody FirstPieceSmtLog firstPieceSmtLog) {
		firstPieceSmtLogService.save(firstPieceSmtLog);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param firstPieceSmtLog
	 * @return
	 */
	@AutoLog(value = "SMT首件确认表-检查内容-编辑")
	@ApiOperation(value="SMT首件确认表-检查内容-编辑", notes="SMT首件确认表-检查内容-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody FirstPieceSmtLog firstPieceSmtLog) {
		firstPieceSmtLogService.updateById(firstPieceSmtLog);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "SMT首件确认表-检查内容-通过id删除")
	@ApiOperation(value="SMT首件确认表-检查内容-通过id删除", notes="SMT首件确认表-检查内容-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		firstPieceSmtLogService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "SMT首件确认表-检查内容-批量删除")
	@ApiOperation(value="SMT首件确认表-检查内容-批量删除", notes="SMT首件确认表-检查内容-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.firstPieceSmtLogService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "SMT首件确认表-检查内容-通过id查询")
	@ApiOperation(value="SMT首件确认表-检查内容-通过id查询", notes="SMT首件确认表-检查内容-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		FirstPieceSmtLog firstPieceSmtLog = firstPieceSmtLogService.getById(id);
		if(firstPieceSmtLog==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(firstPieceSmtLog);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param firstPieceSmtLog
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FirstPieceSmtLog firstPieceSmtLog) {
        return super.exportXls(request, firstPieceSmtLog, FirstPieceSmtLog.class, "SMT首件确认表-检查内容");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, FirstPieceSmtLog.class);
    }

}
