package org.jeecg.modules.mes.produce.controller;

import javax.servlet.http.HttpServletRequest;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesCommandbillDlog;
import org.jeecg.modules.mes.produce.service.IMesCommandbillDlogService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 制令单发料记录
 * @Author: jeecg-boot
 * @Date: 2021-03-04
 * @Version: V1.0
 */
@Api(tags = "制令单发料记录")
@RestController
@RequestMapping("/produce/mesCommandbillDlog")
@Slf4j
public class MesCommandbillDlogController extends JeecgController<MesCommandbillDlog, IMesCommandbillDlogService> {
	@Autowired
	private IMesCommandbillDlogService mesCommandbillDlogService;

    /**
     * 分页列表查询
     *
     * @param mesCommandbillDlog
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "制令单发料记录-分页列表查询")
    @ApiOperation(value = "制令单发料记录-分页列表查询", notes = "制令单发料记录-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(MesCommandbillDlog mesCommandbillDlog,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<MesCommandbillDlog> queryWrapper = QueryGenerator.initQueryWrapper(mesCommandbillDlog, req.getParameterMap());
        Page<MesCommandbillDlog> page = new Page<MesCommandbillDlog>(pageNo, pageSize);
        IPage<MesCommandbillDlog> pageList = mesCommandbillDlogService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    /**
     * 远程调用 发料记录日志添加
     *
     * @param mesCommandbillDlog
     * @return
     */
    @AutoLog(value = "制令单发料记录-添加")
    @ApiOperation(value = "制令单发料记录-添加", notes = "制令单发料记录-添加")
    @PostMapping(value = "/addLog")
    public Boolean add(@RequestBody MesCommandbillDlog mesCommandbillDlog) {
        return mesCommandbillDlogService.save(mesCommandbillDlog);
	}

	/**
	 * 导出excel
	 *
	 * @param request
	 * @param mesCommandbillDlog
	 */
	@RequestMapping(value = "/exportXls")
	public ModelAndView exportXls(HttpServletRequest request, MesCommandbillDlog mesCommandbillDlog) {
		return super.exportXls(request, mesCommandbillDlog, MesCommandbillDlog.class, "制令单上料记录");
	}


}
