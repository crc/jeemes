package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesConditionPlan;
import org.jeecg.modules.mes.produce.service.IMesConditionPlanService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 制造中心-保养计划
 * @Author: jeecg-boot
 * @Date:   2020-10-18
 * @Version: V1.0
 */
@Api(tags="制造中心-保养计划")
@RestController
@RequestMapping("/produce/mesConditionPlan")
@Slf4j
public class MesConditionPlanController extends JeecgController<MesConditionPlan, IMesConditionPlanService> {
	@Autowired
	private IMesConditionPlanService mesConditionPlanService;

	/**
	 * 分页列表查询
	 *
	 * @param mesConditionPlan
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "制造中心-保养计划-分页列表查询")
	@ApiOperation(value="制造中心-保养计划-分页列表查询", notes="制造中心-保养计划-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesConditionPlan mesConditionPlan,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesConditionPlan> queryWrapper = QueryGenerator.initQueryWrapper(mesConditionPlan, req.getParameterMap());
		Page<MesConditionPlan> page = new Page<MesConditionPlan>(pageNo, pageSize);
		IPage<MesConditionPlan> pageList = mesConditionPlanService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param mesConditionPlan
	 * @return
	 */
	@AutoLog(value = "制造中心-保养计划-添加")
	@ApiOperation(value="制造中心-保养计划-添加", notes="制造中心-保养计划-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesConditionPlan mesConditionPlan) {
		mesConditionPlanService.save(mesConditionPlan);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param mesConditionPlan
	 * @return
	 */
	@AutoLog(value = "制造中心-保养计划-编辑")
	@ApiOperation(value="制造中心-保养计划-编辑", notes="制造中心-保养计划-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesConditionPlan mesConditionPlan) {
		if(mesConditionPlan.getStartTime() !=null){
			mesConditionPlanService.updateById(mesConditionPlan);
			return Result.ok("开始保养!");
		}else if (mesConditionPlan.getEndTime() != null){
			mesConditionPlanService.updateById(mesConditionPlan);
			return Result.ok("结束保养!");
		}
		mesConditionPlanService.updateById(mesConditionPlan);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-保养计划-通过id删除")
	@ApiOperation(value="制造中心-保养计划-通过id删除", notes="制造中心-保养计划-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesConditionPlanService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "制造中心-保养计划-批量删除")
	@ApiOperation(value="制造中心-保养计划-批量删除", notes="制造中心-保养计划-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesConditionPlanService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-保养计划-通过id查询")
	@ApiOperation(value="制造中心-保养计划-通过id查询", notes="制造中心-保养计划-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesConditionPlan mesConditionPlan = mesConditionPlanService.getById(id);
		if(mesConditionPlan==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesConditionPlan);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesConditionPlan
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesConditionPlan mesConditionPlan) {
        return super.exportXls(request, mesConditionPlan, MesConditionPlan.class, "制造中心-保养计划");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesConditionPlan.class);
    }

}
