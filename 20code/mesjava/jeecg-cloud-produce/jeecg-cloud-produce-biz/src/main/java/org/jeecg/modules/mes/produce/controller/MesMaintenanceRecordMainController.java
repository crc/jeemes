package org.jeecg.modules.mes.produce.controller;

import java.text.ParseException;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesMaintenanceRecordMain;
import org.jeecg.modules.mes.produce.service.IMesMaintenanceRecordMainService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.mes.produce.vo.OperationRecordVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 维护保养点检记录主表
 * @Author: jeecg-boot
 * @Date: 2021-06-10
 * @Version: V1.0
 */
@Api(tags = "维护保养点检记录主表")
@RestController
@RequestMapping("/produce/mesMaintenanceRecordMain")
@Slf4j
public class MesMaintenanceRecordMainController extends JeecgController<MesMaintenanceRecordMain, IMesMaintenanceRecordMainService> {
    @Autowired
    private IMesMaintenanceRecordMainService mesMaintenanceRecordMainService;

    /**
     * 分页列表查询
     *
     * @param mesMaintenanceRecordMain
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "维护保养点检记录主表-分页列表查询")
    @ApiOperation(value = "维护保养点检记录主表-分页列表查询", notes = "维护保养点检记录主表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(MesMaintenanceRecordMain mesMaintenanceRecordMain,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<MesMaintenanceRecordMain> queryWrapper = QueryGenerator.initQueryWrapper(mesMaintenanceRecordMain, req.getParameterMap());
        Page<MesMaintenanceRecordMain> page = new Page<MesMaintenanceRecordMain>(pageNo, pageSize);
        IPage<MesMaintenanceRecordMain> pageList = mesMaintenanceRecordMainService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    /**
     * 添加
     *
     * @param mesMaintenanceRecordMain
     * @return
     */
    @AutoLog(value = "维护保养点检记录主表-添加")
    @ApiOperation(value = "维护保养点检记录主表-添加", notes = "维护保养点检记录主表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesMaintenanceRecordMain mesMaintenanceRecordMain) {
        return mesMaintenanceRecordMainService.addMesMaintenanceRecordMain(mesMaintenanceRecordMain);
    }

    /**
     * 编辑
     *
     * @param mesMaintenanceRecordMain
     * @return
     */
    @AutoLog(value = "维护保养点检记录主表-编辑")
    @ApiOperation(value = "维护保养点检记录主表-编辑", notes = "维护保养点检记录主表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesMaintenanceRecordMain mesMaintenanceRecordMain) {
        mesMaintenanceRecordMainService.updateById(mesMaintenanceRecordMain);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "维护保养点检记录主表-通过id删除")
    @ApiOperation(value = "维护保养点检记录主表-通过id删除", notes = "维护保养点检记录主表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        mesMaintenanceRecordMainService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "维护保养点检记录主表-批量删除")
    @ApiOperation(value = "维护保养点检记录主表-批量删除", notes = "维护保养点检记录主表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.mesMaintenanceRecordMainService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "维护保养点检记录主表-通过id查询")
    @ApiOperation(value = "维护保养点检记录主表-通过id查询", notes = "维护保养点检记录主表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        MesMaintenanceRecordMain mesMaintenanceRecordMain = mesMaintenanceRecordMainService.getById(id);
        if (mesMaintenanceRecordMain == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(mesMaintenanceRecordMain);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param mesMaintenanceRecordMain
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesMaintenanceRecordMain mesMaintenanceRecordMain) {
        return super.exportXls(request, mesMaintenanceRecordMain, MesMaintenanceRecordMain.class, "维护保养点检记录主表");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesMaintenanceRecordMain.class);
    }

    /**
     * 通过tableNameCode查询作业项目模板
     *
     * @param tableNameCode
     * @return
     */
    @AutoLog(value = "维护保养点检记录主表-通过tableNameCode查询作业项目模板")
    @ApiOperation(value = "维护保养点检记录主表-通过tableNameCode查询作业项目模板", notes = "维护保养点检记录主表-通过tableNameCode查询作业项目模板")
    @GetMapping(value = "/queryProjectModeByCode")
    public Result<?> queryProjectModeByCode(@RequestParam(name = "tableNameCode") String tableNameCode) {
        return mesMaintenanceRecordMainService.queryProjectModeByCode(tableNameCode);
    }

    /**
     * 通过tableNameCode和项目模板查询作业项目
     *
     * @param tableNameCode
     * @return
     */
    @AutoLog(value = "维护保养点检记录主表-通过tableNameCode和项目模板查询作业项目")
    @ApiOperation(value = "维护保养点检记录主表-通过tableNameCode和项目模板查询作业项目", notes = "维护保养点检记录主表-通过tableNameCode和项目模板查询作业项目")
    @GetMapping(value = "/queryProjectModeByCodeAndProject")
    public Result<?> queryProjectModeByCodeAndProject(@RequestParam(name = "tableNameCode") String tableNameCode,
                                                      @RequestParam(name = "project") String project) {
        return mesMaintenanceRecordMainService.queryProjectModeByCodeAndProject(tableNameCode, project);
    }

    /**
     * APP添加
     *
     * @param operationRecordVo
     * @return
     */
    @AutoLog(value = "维护保养点检记录主表-APP添加")
    @ApiOperation(value = "维护保养点检记录主表-APP添加", notes = "维护保养点检记录主表-APP添加")
    @PostMapping(value = "/addMesMaintenanceRecordItemForApp")
    public Result<?> addMesMaintenanceRecordItemForApp(@RequestBody OperationRecordVo operationRecordVo) {
        try{
            return mesMaintenanceRecordMainService.addMesMaintenanceRecordItemForApp(operationRecordVo);
        }catch (ParseException e){
            e.printStackTrace();
            return Result.error("出现异常，请稍后再试！");
        }
    }

}
