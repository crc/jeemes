package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesMaketoolResume;
import org.jeecg.modules.mes.produce.service.IMesMaketoolResumeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 制造中心-制具履历
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Api(tags="制造中心-制具履历")
@RestController
@RequestMapping("/produce/mesMaketoolResume")
@Slf4j
public class MesMaketoolResumeController extends JeecgController<MesMaketoolResume, IMesMaketoolResumeService> {
	@Autowired
	private IMesMaketoolResumeService mesMaketoolResumeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesMaketoolResume
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "制造中心-制具履历-分页列表查询")
	@ApiOperation(value="制造中心-制具履历-分页列表查询", notes="制造中心-制具履历-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesMaketoolResume mesMaketoolResume,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesMaketoolResume> queryWrapper = QueryGenerator.initQueryWrapper(mesMaketoolResume, req.getParameterMap());
		Page<MesMaketoolResume> page = new Page<MesMaketoolResume>(pageNo, pageSize);
		IPage<MesMaketoolResume> pageList = mesMaketoolResumeService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesMaketoolResume
	 * @return
	 */
	@AutoLog(value = "制造中心-制具履历-添加")
	@ApiOperation(value="制造中心-制具履历-添加", notes="制造中心-制具履历-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesMaketoolResume mesMaketoolResume) {
		mesMaketoolResumeService.save(mesMaketoolResume);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesMaketoolResume
	 * @return
	 */
	@AutoLog(value = "制造中心-制具履历-编辑")
	@ApiOperation(value="制造中心-制具履历-编辑", notes="制造中心-制具履历-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesMaketoolResume mesMaketoolResume) {
		mesMaketoolResumeService.updateById(mesMaketoolResume);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-制具履历-通过id删除")
	@ApiOperation(value="制造中心-制具履历-通过id删除", notes="制造中心-制具履历-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesMaketoolResumeService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "制造中心-制具履历-批量删除")
	@ApiOperation(value="制造中心-制具履历-批量删除", notes="制造中心-制具履历-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesMaketoolResumeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-制具履历-通过id查询")
	@ApiOperation(value="制造中心-制具履历-通过id查询", notes="制造中心-制具履历-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesMaketoolResume mesMaketoolResume = mesMaketoolResumeService.getById(id);
		if(mesMaketoolResume==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesMaketoolResume);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesMaketoolResume
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesMaketoolResume mesMaketoolResume) {
        return super.exportXls(request, mesMaketoolResume, MesMaketoolResume.class, "制造中心-制具履历");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesMaketoolResume.class);
    }

}
