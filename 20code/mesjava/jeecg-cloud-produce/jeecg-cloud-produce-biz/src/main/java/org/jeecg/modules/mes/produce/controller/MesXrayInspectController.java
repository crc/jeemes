package org.jeecg.modules.mes.produce.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.produce.entity.MesXrayBadinfo;
import org.jeecg.modules.mes.produce.entity.MesXrayInspect;
import org.jeecg.modules.mes.produce.vo.MesXrayInspectPage;
import org.jeecg.modules.mes.produce.service.IMesXrayInspectService;
import org.jeecg.modules.mes.produce.service.IMesXrayBadinfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 制造中心-X-Ray检测
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Api(tags="制造中心-X-Ray检测")
@RestController
@RequestMapping("/produce/mesXrayInspect")
@Slf4j
public class MesXrayInspectController {
	@Autowired
	private IMesXrayInspectService mesXrayInspectService;
	@Autowired
	private IMesXrayBadinfoService mesXrayBadinfoService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesXrayInspect
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "制造中心-X-Ray检测-分页列表查询")
	@ApiOperation(value="制造中心-X-Ray检测-分页列表查询", notes="制造中心-X-Ray检测-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesXrayInspect mesXrayInspect,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesXrayInspect> queryWrapper = QueryGenerator.initQueryWrapper(mesXrayInspect, req.getParameterMap());
		Page<MesXrayInspect> page = new Page<MesXrayInspect>(pageNo, pageSize);
		IPage<MesXrayInspect> pageList = mesXrayInspectService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesXrayInspectPage
	 * @return
	 */
	@AutoLog(value = "制造中心-X-Ray检测-添加")
	@ApiOperation(value="制造中心-X-Ray检测-添加", notes="制造中心-X-Ray检测-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesXrayInspectPage mesXrayInspectPage) {
		MesXrayInspect mesXrayInspect = new MesXrayInspect();
		BeanUtils.copyProperties(mesXrayInspectPage, mesXrayInspect);
		mesXrayInspectService.saveMain(mesXrayInspect, mesXrayInspectPage.getMesXrayBadinfoList());
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesXrayInspectPage
	 * @return
	 */
	@AutoLog(value = "制造中心-X-Ray检测-编辑")
	@ApiOperation(value="制造中心-X-Ray检测-编辑", notes="制造中心-X-Ray检测-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesXrayInspectPage mesXrayInspectPage) {
		MesXrayInspect mesXrayInspect = new MesXrayInspect();
		BeanUtils.copyProperties(mesXrayInspectPage, mesXrayInspect);
		MesXrayInspect mesXrayInspectEntity = mesXrayInspectService.getById(mesXrayInspect.getId());
		if(mesXrayInspectEntity==null) {
			return Result.error("未找到对应数据");
		}
		mesXrayInspectService.updateMain(mesXrayInspect, mesXrayInspectPage.getMesXrayBadinfoList());
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-X-Ray检测-通过id删除")
	@ApiOperation(value="制造中心-X-Ray检测-通过id删除", notes="制造中心-X-Ray检测-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesXrayInspectService.delMain(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "制造中心-X-Ray检测-批量删除")
	@ApiOperation(value="制造中心-X-Ray检测-批量删除", notes="制造中心-X-Ray检测-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesXrayInspectService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-X-Ray检测-通过id查询")
	@ApiOperation(value="制造中心-X-Ray检测-通过id查询", notes="制造中心-X-Ray检测-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesXrayInspect mesXrayInspect = mesXrayInspectService.getById(id);
		if(mesXrayInspect==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesXrayInspect);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "X-Ray检测-不良信息通过主表ID查询")
	@ApiOperation(value="X-Ray检测-不良信息主表ID查询", notes="X-Ray检测-不良信息-通主表ID查询")
	@GetMapping(value = "/queryMesXrayBadinfoByMainId")
	public Result<?> queryMesXrayBadinfoListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesXrayBadinfo> mesXrayBadinfoList = mesXrayBadinfoService.selectByMainId(id);
		return Result.ok(mesXrayBadinfoList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesXrayInspect
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesXrayInspect mesXrayInspect) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<MesXrayInspect> queryWrapper = QueryGenerator.initQueryWrapper(mesXrayInspect, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<MesXrayInspect> queryList = mesXrayInspectService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<MesXrayInspect> mesXrayInspectList = new ArrayList<MesXrayInspect>();
      if(oConvertUtils.isEmpty(selections)) {
          mesXrayInspectList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          mesXrayInspectList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<MesXrayInspectPage> pageList = new ArrayList<MesXrayInspectPage>();
      for (MesXrayInspect main : mesXrayInspectList) {
          MesXrayInspectPage vo = new MesXrayInspectPage();
          BeanUtils.copyProperties(main, vo);
          List<MesXrayBadinfo> mesXrayBadinfoList = mesXrayBadinfoService.selectByMainId(main.getId());
          vo.setMesXrayBadinfoList(mesXrayBadinfoList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "制造中心-X-Ray检测列表");
      mv.addObject(NormalExcelConstants.CLASS, MesXrayInspectPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("制造中心-X-Ray检测数据", "导出人:"+sysUser.getRealname(), "制造中心-X-Ray检测"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<MesXrayInspectPage> list = ExcelImportUtil.importExcel(file.getInputStream(), MesXrayInspectPage.class, params);
              for (MesXrayInspectPage page : list) {
                  MesXrayInspect po = new MesXrayInspect();
                  BeanUtils.copyProperties(page, po);
                  mesXrayInspectService.saveMain(po, page.getMesXrayBadinfoList());
              }
              return Result.ok("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.ok("文件导入失败！");
    }

}
