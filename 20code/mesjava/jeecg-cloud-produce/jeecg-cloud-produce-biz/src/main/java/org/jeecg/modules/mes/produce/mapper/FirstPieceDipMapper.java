package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.FirstPieceDip;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: DIP首件确认表
 * @Author: jeecg-boot
 * @Date:   2021-03-22
 * @Version: V1.0
 */
public interface FirstPieceDipMapper extends BaseMapper<FirstPieceDip> {

}
