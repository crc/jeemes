package org.jeecg.modules.mes.produce.mapper;


import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 制造中心-制令单信息
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
public interface MesCommandbillInfoMapper extends BaseMapper<MesCommandbillInfo> {

}
