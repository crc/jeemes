package org.jeecg.modules.mes.produce.mapper;


import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.mes.produce.entity.MesCommandbillPitem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @Description: 制造中心—制令单BOM项目
 * @Author: jeecg-boot
 * @Date:   2020-12-17
 * @Version: V1.0
 */
public interface MesCommandbillPitemMapper extends BaseMapper<MesCommandbillPitem> {
	
	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesCommandbillPitem> selectByMainId(@Param("mainId") String mainId);
}
