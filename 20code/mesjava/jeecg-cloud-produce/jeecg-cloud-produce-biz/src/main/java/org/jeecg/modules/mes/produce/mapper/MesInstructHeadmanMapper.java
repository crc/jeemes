package org.jeecg.modules.mes.produce.mapper;

import java.util.List;
import org.jeecg.modules.mes.produce.entity.MesInstructHeadman;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 制造中心-检测项目(指示单项目责任人)
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface MesInstructHeadmanMapper extends BaseMapper<MesInstructHeadman> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesInstructHeadman> selectByMainId(@Param("mainId") String mainId);
}
