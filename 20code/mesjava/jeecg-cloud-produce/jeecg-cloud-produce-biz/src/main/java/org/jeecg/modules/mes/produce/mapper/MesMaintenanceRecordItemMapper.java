package org.jeecg.modules.mes.produce.mapper;

import org.jeecg.modules.mes.produce.entity.MesMaintenanceRecordItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 维护保养点检记录子表
 * @Author: jeecg-boot
 * @Date:   2021-06-11
 * @Version: V1.0
 */
public interface MesMaintenanceRecordItemMapper extends BaseMapper<MesMaintenanceRecordItem> {

}
