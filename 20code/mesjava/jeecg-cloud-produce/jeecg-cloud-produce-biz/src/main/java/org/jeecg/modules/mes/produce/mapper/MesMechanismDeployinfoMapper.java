package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.MesMechanismDeployinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 机种项目配置-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface MesMechanismDeployinfoMapper extends BaseMapper<MesMechanismDeployinfo> {

}
