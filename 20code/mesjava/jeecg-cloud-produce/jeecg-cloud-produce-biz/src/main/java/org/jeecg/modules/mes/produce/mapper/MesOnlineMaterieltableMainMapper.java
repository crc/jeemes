package org.jeecg.modules.mes.produce.mapper;

import org.jeecg.modules.mes.produce.entity.MesOnlineMaterieltableMain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 制造中心-在线料表-主表
 * @Author: jeecg-boot
 * @Date:   2021-05-17
 * @Version: V1.0
 */
public interface MesOnlineMaterieltableMainMapper extends BaseMapper<MesOnlineMaterieltableMain> {

}
