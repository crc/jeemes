package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.MesProductionCapacityReport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: NEW生产产能报表
 * @Author: jeecg-boot
 * @Date:   2021-05-25
 * @Version: V1.0
 */
public interface MesProductionCapacityReportMapper extends BaseMapper<MesProductionCapacityReport> {

    List<MesProductionCapacityReport> mesProductionCapacityReport(Page<MesProductionCapacityReport> page, @Param("mesProductionCapacityReport") MesProductionCapacityReport mesProductionCapacityReport);
}
