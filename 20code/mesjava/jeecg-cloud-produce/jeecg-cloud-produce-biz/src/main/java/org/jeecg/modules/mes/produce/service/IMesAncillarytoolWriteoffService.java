package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesAncillarytoolWriteoff;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-辅料报废
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesAncillarytoolWriteoffService extends IService<MesAncillarytoolWriteoff> {

}
