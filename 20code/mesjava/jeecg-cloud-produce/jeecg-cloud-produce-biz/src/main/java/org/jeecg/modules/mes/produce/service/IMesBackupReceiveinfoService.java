package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesBackupReceiveitem;
import org.jeecg.modules.mes.produce.entity.MesBackupReceiveinfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 备品领用-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesBackupReceiveinfoService extends IService<MesBackupReceiveinfo> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesBackupReceiveinfo mesBackupReceiveinfo,List<MesBackupReceiveitem> mesBackupReceiveitemList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesBackupReceiveinfo mesBackupReceiveinfo,List<MesBackupReceiveitem> mesBackupReceiveitemList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
