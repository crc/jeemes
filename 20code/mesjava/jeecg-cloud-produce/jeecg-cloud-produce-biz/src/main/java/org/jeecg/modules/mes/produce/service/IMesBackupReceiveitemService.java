package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesBackupReceiveitem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 备品领用-明细信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesBackupReceiveitemService extends IService<MesBackupReceiveitem> {

	public List<MesBackupReceiveitem> selectByMainId(String mainId);
}
