package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesConditionPlan;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-保养计划
 * @Author: jeecg-boot
 * @Date:   2020-10-18
 * @Version: V1.0
 */
public interface IMesConditionPlanService extends IService<MesConditionPlan> {

}
