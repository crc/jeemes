package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesCourseScanLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 物料追踪记录
 * @Author: jeecg-boot
 * @Date:   2021-03-16
 * @Version: V1.0
 */
public interface IMesCourseScanLogService extends IService<MesCourseScanLog> {

}
