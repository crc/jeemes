package org.jeecg.modules.mes.produce.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.produce.entity.MesMaintenanceRecordMain;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.produce.vo.OperationRecordVo;

import java.text.ParseException;

/**
 * @Description: 维护保养点检记录主表
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
public interface IMesMaintenanceRecordMainService extends IService<MesMaintenanceRecordMain> {

    Result<?> addMesMaintenanceRecordMain(MesMaintenanceRecordMain mesMaintenanceRecordMain);

    Result<?> queryProjectModeByCode(String tableNameCode);

    Result<?> queryProjectModeByCodeAndProject(String tableNameCode, String project);

    Result<?> addMesMaintenanceRecordItemForApp(OperationRecordVo operationRecordVo) throws ParseException;
}
