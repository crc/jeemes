package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesSpitestInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-SPI测试信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesSpitestInfoService extends IService<MesSpitestInfo> {

}
