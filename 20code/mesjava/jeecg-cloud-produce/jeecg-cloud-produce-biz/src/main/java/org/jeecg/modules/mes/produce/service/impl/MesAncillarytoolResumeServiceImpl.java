package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesAncillarytoolResume;
import org.jeecg.modules.mes.produce.mapper.MesAncillarytoolResumeMapper;
import org.jeecg.modules.mes.produce.service.IMesAncillarytoolResumeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-辅料履历
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesAncillarytoolResumeServiceImpl extends ServiceImpl<MesAncillarytoolResumeMapper, MesAncillarytoolResume> implements IMesAncillarytoolResumeService {

}
