package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesAncillarytoolWriteoff;
import org.jeecg.modules.mes.produce.mapper.MesAncillarytoolWriteoffMapper;
import org.jeecg.modules.mes.produce.service.IMesAncillarytoolWriteoffService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-辅料报废
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesAncillarytoolWriteoffServiceImpl extends ServiceImpl<MesAncillarytoolWriteoffMapper, MesAncillarytoolWriteoff> implements IMesAncillarytoolWriteoffService {

}
