package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mes.client.WareHouseClient;
import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import org.jeecg.modules.mes.produce.mapper.MesCommandbillInfoMapper;
import org.jeecg.modules.mes.produce.service.IMesCommandbillInfoService;
import org.jeecg.modules.mes.storage.entity.MesErrorOperationRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 制造中心-制令单信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesCommandbillInfoServiceImpl extends ServiceImpl<MesCommandbillInfoMapper, MesCommandbillInfo> implements IMesCommandbillInfoService {

    @Autowired
    private MesCommandbillInfoMapper mesCommandbillInfoMapper;
	
    @Autowired
    private WareHouseClient wareHouseClient;

    @Override
    public void recordErrorOperation(String callAddress, String operationSteps, String wrongSteps, String errMeg, String commandBillId) {
        // 获取登录用户信息
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        MesErrorOperationRecord errorOperationRecord = new MesErrorOperationRecord();
        errorOperationRecord.setCreateBy(sysUser.getRealname());//创建人
        //errorOperationRecord.setCreateTime(new Date);//创建日期
        errorOperationRecord.setSysOrgCode(sysUser.getOrgCode());//所属部门
        errorOperationRecord.setOperatorId(sysUser.getId());//操作人ID
        errorOperationRecord.setOperatorAccount(sysUser.getUsername());//操作人账号
        errorOperationRecord.setPhone(sysUser.getPhone());//操作人电话
        //errorOperationRecord.setUserStatus(sysUser.getStatus());//用户状态

        errorOperationRecord.setCallAddress(callAddress);//调用地址
        errorOperationRecord.setOperationSteps(operationSteps);//操作步骤
        errorOperationRecord.setWrongSteps(wrongSteps);//错误步骤
        errorOperationRecord.setErrorCause(errMeg);//错误原因
        if (StringUtils.isNotBlank(commandBillId)) {
            MesCommandbillInfo mesCommandBillInfo = null;
            if (commandBillId.contains("add")) {
                QueryWrapper<MesCommandbillInfo> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("commandbill_code",commandBillId).orderByDesc("create_time");
                List<MesCommandbillInfo> list = mesCommandbillInfoMapper.selectList(queryWrapper);
                if (com.epms.util.ObjectHelper.isNotEmpty(list)) {
                    mesCommandBillInfo = list.get(0);
                }
            }else{
                mesCommandBillInfo = mesCommandbillInfoMapper.selectById(commandBillId);
            }
            if (com.epms.util.ObjectHelper.isNotEmpty(mesCommandBillInfo)) {
                errorOperationRecord.setLineType(mesCommandBillInfo.getLineType());//线别
                errorOperationRecord.setProduceGrade(mesCommandBillInfo.getProduceGrade());//阶别
            }
        }
        //errorOperationRecord.setRemark();//备注
        wareHouseClient.add(errorOperationRecord);
    }

    @Override
    public List<MesCommandbillInfo> queryMesCommandBillInfoByProduceId(List<String> produceIds) {
        QueryWrapper<MesCommandbillInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("produce_id",produceIds);
        return mesCommandbillInfoMapper.selectList(queryWrapper);
    }

}
