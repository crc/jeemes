package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesConditionPlan;
import org.jeecg.modules.mes.produce.mapper.MesConditionPlanMapper;
import org.jeecg.modules.mes.produce.service.IMesConditionPlanService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-保养计划
 * @Author: jeecg-boot
 * @Date:   2020-10-18
 * @Version: V1.0
 */
@Service
public class MesConditionPlanServiceImpl extends ServiceImpl<MesConditionPlanMapper, MesConditionPlan> implements IMesConditionPlanService {

}
