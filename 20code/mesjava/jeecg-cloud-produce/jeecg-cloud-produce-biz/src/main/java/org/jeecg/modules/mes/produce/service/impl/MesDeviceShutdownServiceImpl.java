package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesDeviceShutdown;
import org.jeecg.modules.mes.produce.mapper.MesDeviceShutdownMapper;
import org.jeecg.modules.mes.produce.service.IMesDeviceShutdownService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-设备停机
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesDeviceShutdownServiceImpl extends ServiceImpl<MesDeviceShutdownMapper, MesDeviceShutdown> implements IMesDeviceShutdownService {

}
