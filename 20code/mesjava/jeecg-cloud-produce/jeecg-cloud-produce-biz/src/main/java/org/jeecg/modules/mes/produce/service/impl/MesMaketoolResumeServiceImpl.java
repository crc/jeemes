package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesMaketoolResume;
import org.jeecg.modules.mes.produce.mapper.MesMaketoolResumeMapper;
import org.jeecg.modules.mes.produce.service.IMesMaketoolResumeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-制具履历
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesMaketoolResumeServiceImpl extends ServiceImpl<MesMaketoolResumeMapper, MesMaketoolResume> implements IMesMaketoolResumeService {

}
