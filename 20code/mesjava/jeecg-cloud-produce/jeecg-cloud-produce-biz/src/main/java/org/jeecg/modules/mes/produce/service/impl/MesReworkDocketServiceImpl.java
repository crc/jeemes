package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesReworkDocket;
import org.jeecg.modules.mes.produce.mapper.MesReworkDocketMapper;
import org.jeecg.modules.mes.produce.service.IMesReworkDocketService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-返工单信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesReworkDocketServiceImpl extends ServiceImpl<MesReworkDocketMapper, MesReworkDocket> implements IMesReworkDocketService {

}
