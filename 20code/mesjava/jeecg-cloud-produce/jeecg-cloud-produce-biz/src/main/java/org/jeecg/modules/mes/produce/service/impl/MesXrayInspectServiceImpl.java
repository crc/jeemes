package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesXrayInspect;
import org.jeecg.modules.mes.produce.entity.MesXrayBadinfo;
import org.jeecg.modules.mes.produce.mapper.MesXrayBadinfoMapper;
import org.jeecg.modules.mes.produce.mapper.MesXrayInspectMapper;
import org.jeecg.modules.mes.produce.service.IMesXrayInspectService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 制造中心-X-Ray检测
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesXrayInspectServiceImpl extends ServiceImpl<MesXrayInspectMapper, MesXrayInspect> implements IMesXrayInspectService {

	@Autowired
	private MesXrayInspectMapper mesXrayInspectMapper;
	@Autowired
	private MesXrayBadinfoMapper mesXrayBadinfoMapper;
	
	@Override
	@Transactional
	public void saveMain(MesXrayInspect mesXrayInspect, List<MesXrayBadinfo> mesXrayBadinfoList) {
		mesXrayInspectMapper.insert(mesXrayInspect);
		if(mesXrayBadinfoList!=null && mesXrayBadinfoList.size()>0) {
			for(MesXrayBadinfo entity:mesXrayBadinfoList) {
				//外键设置
				entity.setXrayId(mesXrayInspect.getId());
				mesXrayBadinfoMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesXrayInspect mesXrayInspect,List<MesXrayBadinfo> mesXrayBadinfoList) {
		mesXrayInspectMapper.updateById(mesXrayInspect);
		
		//1.先删除子表数据
		mesXrayBadinfoMapper.deleteByMainId(mesXrayInspect.getId());
		
		//2.子表数据重新插入
		if(mesXrayBadinfoList!=null && mesXrayBadinfoList.size()>0) {
			for(MesXrayBadinfo entity:mesXrayBadinfoList) {
				//外键设置
				entity.setXrayId(mesXrayInspect.getId());
				mesXrayBadinfoMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesXrayBadinfoMapper.deleteByMainId(id);
		mesXrayInspectMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesXrayBadinfoMapper.deleteByMainId(id.toString());
			mesXrayInspectMapper.deleteById(id);
		}
	}
	
}
