package org.jeecg.modules.mes.produce.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class MaintenanceRecordVo implements Serializable {
    @ApiModelProperty(value = "表名称编码")
    private String tableNameCode;

    @ApiModelProperty(value = "设备SN")
    private String equipmentSn;

    @ApiModelProperty(value = "记录类型")
    private String recordType;

    @ApiModelProperty(value = "保养人")
    private String maintainer;

    @ApiModelProperty(value = "审核人")
    private String reviewer;

    @ApiModelProperty(value = "检查项目")
    private List<MaintenanceRecordProjectVo> projectVoList;
}
