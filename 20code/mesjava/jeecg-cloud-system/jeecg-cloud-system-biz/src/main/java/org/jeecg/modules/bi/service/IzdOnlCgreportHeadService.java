package org.jeecg.modules.bi.service;

import org.jeecg.modules.bi.entity.zdOnlCgreportParam;
import org.jeecg.modules.bi.entity.zdOnlCgreportItem;
import org.jeecg.modules.bi.entity.zdOnlCgreportHead;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: onl_cgreport_head
 * @Author: wms-cloud
 * @Date:   2020-12-09
 * @Version: V1.0
 */
public interface IzdOnlCgreportHeadService extends IService<zdOnlCgreportHead> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(zdOnlCgreportHead zdOnlCgreportHead, List<zdOnlCgreportParam> zdOnlCgreportParamList, List<zdOnlCgreportItem> zdOnlCgreportItemList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(zdOnlCgreportHead zdOnlCgreportHead, List<zdOnlCgreportParam> zdOnlCgreportParamList, List<zdOnlCgreportItem> zdOnlCgreportItemList);
	
	/**
	 * 删除一对多
	 */
	public void delMain(String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain(Collection<? extends Serializable> idList);
	
}
