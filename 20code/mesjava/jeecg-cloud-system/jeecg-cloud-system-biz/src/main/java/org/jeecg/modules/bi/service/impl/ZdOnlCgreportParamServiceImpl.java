package org.jeecg.modules.bi.service.impl;

import org.jeecg.modules.bi.entity.zdOnlCgreportParam;
import org.jeecg.modules.bi.mapper.zdOnlCgreportParamMapper;
import org.jeecg.modules.bi.service.IzdOnlCgreportParamService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: onl_cgreport_param
 * @Author: wms-cloud
 * @Date:   2020-12-09
 * @Version: V1.0
 */
@Service
public class ZdOnlCgreportParamServiceImpl extends ServiceImpl<zdOnlCgreportParamMapper, zdOnlCgreportParam> implements IzdOnlCgreportParamService {
	
	@Autowired
	private zdOnlCgreportParamMapper zdOnlCgreportParamMapper;
	
	@Override
	public List<zdOnlCgreportParam> selectByMainId(String mainId) {
		return zdOnlCgreportParamMapper.selectByMainId(mainId);
	}
}
