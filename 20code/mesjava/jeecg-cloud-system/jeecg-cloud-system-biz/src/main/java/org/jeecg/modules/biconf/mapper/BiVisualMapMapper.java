package org.jeecg.modules.biconf.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.biconf.entity.BiVisualMap;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 地图主数据
 * @Author: jeecg-boot
 * @Date:   2020-09-09
 * @Version: V1.0
 */
public interface BiVisualMapMapper extends BaseMapper<BiVisualMap> {

}
