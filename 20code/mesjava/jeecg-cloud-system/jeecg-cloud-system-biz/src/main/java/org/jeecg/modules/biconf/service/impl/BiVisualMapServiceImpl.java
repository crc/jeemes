package org.jeecg.modules.biconf.service.impl;

import org.jeecg.modules.biconf.entity.BiVisualMap;
import org.jeecg.modules.biconf.mapper.BiVisualMapMapper;
import org.jeecg.modules.biconf.service.IBiVisualMapService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 地图主数据
 * @Author: jeecg-boot
 * @Date:   2020-09-09
 * @Version: V1.0
 */
@Service
public class BiVisualMapServiceImpl extends ServiceImpl<BiVisualMapMapper, BiVisualMap> implements IBiVisualMapService {

}
