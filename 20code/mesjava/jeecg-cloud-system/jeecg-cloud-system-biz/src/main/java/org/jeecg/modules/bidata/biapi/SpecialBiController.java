package org.jeecg.modules.bidata.biapi;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataProductline;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataProductlineService;
import org.jeecg.modules.mes.client.InspectClient;
import org.jeecg.modules.mes.client.ProduceClient;
import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "SpecialBiController", description = "SpecialBiController", tags = "SpecialBiController")
@RestController
@RequestMapping(value = "/basebi/special")
public class SpecialBiController {

    @Autowired
    private IMesChiefdataProductlineService mesChiefdataProductlineService;
    @Autowired
    private ProduceClient produceClient;
    @Autowired
    private InspectClient inspectClient;
//前台大屏--获取产线列表
    @ApiOperation(value = "getProductLineInfo", produces = "application/json", httpMethod = "GET")
    @RequestMapping(value = "getProductLineInfo", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getProductLineInfo(HttpServletRequest request) {

        QueryWrapper<MesChiefdataProductline> w1 =new QueryWrapper<>();
        w1.orderByDesc("productline_type");
        w1.orderByAsc("productline_code");
        List<MesChiefdataProductline> list= mesChiefdataProductlineService.list(w1);
        List<MesCommandbillInfo> billlist = produceClient.getInfoByLine();
        List<Map<String,String>> ret = new ArrayList<>();
        int count = 1;
        for(MesChiefdataProductline line : list){
            String plannum = "0";
            for(MesCommandbillInfo bill : billlist){
                if(bill.getLineType().equals(line.getProductlineCode())){
                    plannum = bill.getPlantNum();
                    break;
                }
            }
            Map m = new HashMap();
            m.put("type1",count+"");
            m.put("type2",line.getProductlineCode());
            m.put("type3",line.getQuery3());
            if("0".equals(plannum)) {
                m.put("type4", "停机");
            }else {
                m.put("type4", plannum);
            }
            BigDecimal b = new BigDecimal("0");
            if(StringUtils.isNotEmpty(line.getQuery3())){
                b = new BigDecimal(plannum).divide(new BigDecimal(line.getQuery3()),2);
                if(b.compareTo(new BigDecimal("24")) > 0){
                    b = b.divide(new BigDecimal("24"),2).setScale(1);
                }else{
                    b = b.setScale(2);
                }
            }
            m.put("type5",b+"");
            ret.add(m);
            count++;
        }
        return new ResponseEntity(ret, HttpStatus.OK);
    }

//前台大屏--获取产线总产能
    @ApiOperation(value = "getLineOut", produces = "application/json", httpMethod = "GET")
    @RequestMapping(value = "getLineOut", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getLineOut(HttpServletRequest request) {
        Map ret = new HashMap();
        QueryWrapper<MesChiefdataProductline> w1 =new QueryWrapper<>();
        w1.select("sum(query3) as query3");
        MesChiefdataProductline line = mesChiefdataProductlineService.getOne(w1);
        ret.put("value",line.getQuery3());
        return new ResponseEntity(ret, HttpStatus.OK);
    }
//前台大屏-获取产能比例
    @ApiOperation(value = "getLineOutBl", produces = "application/json", httpMethod = "GET")
    @RequestMapping(value = "getLineOutBl", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getLineOutBl(HttpServletRequest request) {
        QueryWrapper<MesChiefdataProductline> w1 =new QueryWrapper<>();
        w1.select("sum(query3) as query3");
        MesChiefdataProductline line = mesChiefdataProductlineService.getOne(w1);
        MesCommandbillInfo bill = produceClient.getInfoTotalByLine();
        BigDecimal b = new BigDecimal("0");
        if(StringUtils.isNotEmpty(line.getQuery3()) && !"0".equals(line.getQuery3())){
            b = new BigDecimal(bill.getPlantNum()).divide(new BigDecimal(line.getQuery3()),2,BigDecimal.ROUND_DOWN).multiply(new BigDecimal("100"));
            b = b.setScale(0);
        }
        Map ret = new HashMap();
        ret.put("min",1);
        ret.put("max",100);
        ret.put("label","产能负荷");
        ret.put("value",b);
        ret.put("unit","%");
        return new ResponseEntity(ret, HttpStatus.OK);
    }
    //前台大屏-获取质检数据
    @ApiOperation(value = "getInspectTotle", produces = "application/json", httpMethod = "GET")
    @RequestMapping(value = "getInspectTotle", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getInspectTotle(HttpServletRequest request) {
        List<Object> list =new ArrayList<>();
        Map map1 = new HashMap();
        map1.put("backgroundColor","#67C23A");
        map1.put("prefixText","来料检验总单数");
        map1.put("value",inspectClient.inspectTotle());
        map1.put("suffixText","单");
        list.add(map1);
        Map map2 = new HashMap();
        map2.put("backgroundColor","#409EFF");
        map2.put("prefixText","出库检验总单数");
        map2.put("value",inspectClient.inspectOutTotle());
        map2.put("suffixText","单");
        list.add(map2);
        Map map3 = new HashMap();
        map3.put("backgroundColor","#E6A23C");
        map3.put("prefixText","质检总数量");
        map3.put("value",inspectClient.inspectIqcOutSum());
        map3.put("suffixText","件");
        list.add(map3);
        Map map4 = new HashMap();
        map4.put("backgroundColor","#F56C6C");
        map4.put("prefixText","检验人员");
        map4.put("value","10");
        map4.put("suffixText","人");
        list.add(map4);
        Map map5 = new HashMap();
        map5.put("backgroundColor","#7232dd");
        map5.put("prefixText","自动化检验机台");
        map5.put("value","20");
        map5.put("suffixText","台");
        list.add(map5);
        Map map6 = new HashMap();
        map6.put("backgroundColor","blue");
        map6.put("prefixText","采集质检数据");
        map6.put("value","4536701");
        map6.put("suffixText","条");
        list.add(map6);
        return new ResponseEntity(list, HttpStatus.OK);
    }
}
