package org.jeecg.modules.bidata.service;

import org.jeecg.modules.bidata.entity.BiSql;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 数据配置接口
 * @Author: jeecg-boot
 * @Date:   2020-09-09
 * @Version: V1.0
 */
public interface IBiSqlService extends IService<BiSql> {

}
