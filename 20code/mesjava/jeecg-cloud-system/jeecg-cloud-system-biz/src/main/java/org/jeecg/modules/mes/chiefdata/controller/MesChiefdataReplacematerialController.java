package org.jeecg.modules.mes.chiefdata.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataReplacematerial;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataReplacematerialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

 /**
 * @Description: 主数据—替代料
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Api(tags="主数据—替代料")
@RestController
@RequestMapping("/chiefdata/mesChiefdataReplacematerial")
@Slf4j
public class MesChiefdataReplacematerialController extends JeecgController<MesChiefdataReplacematerial, IMesChiefdataReplacematerialService> {
	@Autowired
	private IMesChiefdataReplacematerialService mesChiefdataReplacematerialService;

	/**
	 * 分页列表查询
	 *
	 * @param mesChiefdataReplacematerial
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "主数据—替代料-分页列表查询")
	@ApiOperation(value="主数据—替代料-分页列表查询", notes="主数据—替代料-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChiefdataReplacematerial mesChiefdataReplacematerial,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChiefdataReplacematerial> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataReplacematerial, req.getParameterMap());
		Page<MesChiefdataReplacematerial> page = new Page<MesChiefdataReplacematerial>(pageNo, pageSize);
		IPage<MesChiefdataReplacematerial> pageList = mesChiefdataReplacematerialService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param mesChiefdataReplacematerial
	 * @return
	 */
	@AutoLog(value = "主数据—替代料-添加")
	@ApiOperation(value="主数据—替代料-添加", notes="主数据—替代料-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChiefdataReplacematerial mesChiefdataReplacematerial) {
		mesChiefdataReplacematerialService.save(mesChiefdataReplacematerial);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param mesChiefdataReplacematerial
	 * @return
	 */
	@AutoLog(value = "主数据—替代料-编辑")
	@ApiOperation(value="主数据—替代料-编辑", notes="主数据—替代料-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChiefdataReplacematerial mesChiefdataReplacematerial) {
		mesChiefdataReplacematerialService.updateById(mesChiefdataReplacematerial);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—替代料-通过id删除")
	@ApiOperation(value="主数据—替代料-通过id删除", notes="主数据—替代料-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChiefdataReplacematerialService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "主数据—替代料-批量删除")
	@ApiOperation(value="主数据—替代料-批量删除", notes="主数据—替代料-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChiefdataReplacematerialService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—替代料-通过id查询")
	@ApiOperation(value="主数据—替代料-通过id查询", notes="主数据—替代料-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesChiefdataReplacematerial mesChiefdataReplacematerial = mesChiefdataReplacematerialService.getById(id);
		if(mesChiefdataReplacematerial==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataReplacematerial);
	}

	 /**
	  * 通过id查询
	  *
	  * @param machineSort
	  * @param mainCode
	  * @return
	  */
	 @AutoLog(value = "主数据—替代料-通过成品料号和主料料号查询替代料信息")
	 @ApiOperation(value="主数据—替代料-通过成品料号和主料料号查询替代料信息", notes="主数据—替代料-通过成品料号和主料料号查询替代料信息")
	 @GetMapping(value = "/queryMainCode")
	 public Result<?> queryMainCode(@RequestParam(name="machineSort",required=true) String machineSort,
								@RequestParam(name="mainCode",required=true) String mainCode) {
		 QueryWrapper<MesChiefdataReplacematerial> queryWrapper = new QueryWrapper<>();
		 queryWrapper.eq("machine_sort",machineSort);
		 queryWrapper.eq("main_code",mainCode);
		 List<MesChiefdataReplacematerial> list = mesChiefdataReplacematerialService.list(queryWrapper);
		 return Result.ok(list);
	 }

	 /**
	  * 根据成品料号和主料料号、替代料料号查询 远程调用
	  * @param machineSort
	  * @param replaceCode
	  * @return
	  */
	 @GetMapping(value = "/queryReplaceCode")
	 public List<MesChiefdataReplacematerial> queryReplaceCode(@RequestParam(name="machineSort",required=true) String machineSort,
								@RequestParam(name="mainCode",required=false) String mainCode,
								@RequestParam(name="replaceCode",required=true) String replaceCode) {
		 QueryWrapper<MesChiefdataReplacematerial> queryWrapper = new QueryWrapper<>();
		 queryWrapper.eq("machine_sort",machineSort);
		 if (StringUtils.isNotBlank(mainCode)) {
			 queryWrapper.eq("main_code", mainCode);
		 }
		 queryWrapper.eq("replace_code",replaceCode);
		 return mesChiefdataReplacematerialService.list(queryWrapper);
	 }

    /**
    * 导出excel
    *
    * @param request
    * @param mesChiefdataReplacematerial
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataReplacematerial mesChiefdataReplacematerial) {
        return super.exportXls(request, mesChiefdataReplacematerial, MesChiefdataReplacematerial.class, "主数据—替代料");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesChiefdataReplacematerial.class);
    }

}
