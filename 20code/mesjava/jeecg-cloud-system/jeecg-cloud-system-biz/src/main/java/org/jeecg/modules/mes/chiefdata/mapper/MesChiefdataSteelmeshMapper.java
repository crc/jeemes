package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSteelmesh;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 钢网建档
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface MesChiefdataSteelmeshMapper extends BaseMapper<MesChiefdataSteelmesh> {

    @Update("UPDATE mes_chiefdata_steelmesh set point_usage_num=point_usage_num+1 WHERE steelmesh_sn=#{steelmeshSn}")
    boolean increasePointUsageNum(@Param("steelmeshSn") String steelmeshSn);
}
