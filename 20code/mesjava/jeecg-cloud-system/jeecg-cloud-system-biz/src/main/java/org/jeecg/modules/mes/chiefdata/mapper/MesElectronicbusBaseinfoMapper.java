package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;
import org.jeecg.modules.mes.chiefdata.entity.MesElectronicbusBaseinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 主数据—电子料车基本信息
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface MesElectronicbusBaseinfoMapper extends BaseMapper<MesElectronicbusBaseinfo> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesElectronicbusBaseinfo> selectByMainId(@Param("mainId") String mainId);
}
