package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;
import org.jeecg.modules.mes.chiefdata.entity.MesMsdcontrolruleItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 主数据—MSD管控规则明细
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface MesMsdcontrolruleItemMapper extends BaseMapper<MesMsdcontrolruleItem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesMsdcontrolruleItem> selectByMainId(@Param("mainId") String mainId);
}
