package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.chiefdata.entity.MesTransactionLock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 业务锁定
 * @Author: jeecg-boot
 * @Date:   2020-10-24
 * @Version: V1.0
 */
public interface MesTransactionLockMapper extends BaseMapper<MesTransactionLock> {

}
