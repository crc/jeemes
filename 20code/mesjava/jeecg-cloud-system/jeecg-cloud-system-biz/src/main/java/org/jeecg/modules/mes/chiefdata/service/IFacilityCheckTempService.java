package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.chiefdata.entity.FacilityCheckTemp;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 设备检验项目模板
 * @Author: jeecg-boot
 * @Date:   2021-06-02
 * @Version: V1.0
 */
public interface IFacilityCheckTempService extends IService<FacilityCheckTemp> {

    Result<?> queryByFacilityNameAndTempSort(String facilityName, String tempSort);
}
