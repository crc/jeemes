package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesPatrolcheckTimespan;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataPatrolcheck;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 主数据—巡检方案
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataPatrolcheckService extends IService<MesChiefdataPatrolcheck> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesChiefdataPatrolcheck mesChiefdataPatrolcheck,List<MesPatrolcheckTimespan> mesPatrolcheckTimespanList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesChiefdataPatrolcheck mesChiefdataPatrolcheck,List<MesPatrolcheckTimespan> mesPatrolcheckTimespanList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
