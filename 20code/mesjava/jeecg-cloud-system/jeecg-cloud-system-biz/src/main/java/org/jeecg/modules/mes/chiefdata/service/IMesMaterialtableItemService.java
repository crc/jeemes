package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterialtableItem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 主数据—料站表明细
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesMaterialtableItemService extends IService<MesMaterialtableItem> {

	public List<MesMaterialtableItem> selectByMainId(String mainId);
}
