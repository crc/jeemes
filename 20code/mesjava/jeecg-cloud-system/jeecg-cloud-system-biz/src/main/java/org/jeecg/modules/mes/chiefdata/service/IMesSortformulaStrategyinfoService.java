package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesSortformulaStrategyinfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 主数据—拣货方案策略
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesSortformulaStrategyinfoService extends IService<MesSortformulaStrategyinfo> {

	public List<MesSortformulaStrategyinfo> selectByMainId(String mainId);
}
