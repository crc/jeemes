package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesAncillarytoolStir;
import org.jeecg.modules.mes.chiefdata.mapper.MesAncillarytoolStirMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesAncillarytoolStirService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-辅料搅拌
 * @Author: jeecg-boot
 * @Date:   2020-11-04
 * @Version: V1.0
 */
@Service
public class MesAncillarytoolStirServiceImpl extends ServiceImpl<MesAncillarytoolStirMapper, MesAncillarytoolStir> implements IMesAncillarytoolStirService {

}
