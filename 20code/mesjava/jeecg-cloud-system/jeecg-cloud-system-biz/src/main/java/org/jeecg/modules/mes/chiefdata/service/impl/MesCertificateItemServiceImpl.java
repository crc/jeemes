package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesCertificateItem;
import org.jeecg.modules.mes.chiefdata.mapper.MesCertificateItemMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesCertificateItemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 物料凭证项目
 * @Author: jeecg-boot
 * @Date:   2020-10-12
 * @Version: V1.0
 */
@Service
public class MesCertificateItemServiceImpl extends ServiceImpl<MesCertificateItemMapper, MesCertificateItem> implements IMesCertificateItemService {
	
	@Autowired
	private MesCertificateItemMapper mesCertificateItemMapper;
	
	@Override
	public List<MesCertificateItem> selectByMainId(String mainId) {
		return mesCertificateItemMapper.selectByMainId(mainId);
	}
}
