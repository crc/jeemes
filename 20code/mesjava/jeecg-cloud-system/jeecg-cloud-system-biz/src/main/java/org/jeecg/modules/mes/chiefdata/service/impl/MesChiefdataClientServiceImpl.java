package org.jeecg.modules.mes.chiefdata.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataClient;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataClientMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epms.util.ObjectHelper;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;


/**
 * @Description: 主数据—客户
 * @Author: jeecg-boot
 * @Date: 2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataClientServiceImpl extends ServiceImpl<MesChiefdataClientMapper, MesChiefdataClient> implements IMesChiefdataClientService {

    @Autowired
    private MesChiefdataClientMapper mesChiefdataClientMapper;

    @Override
    public Result<?> add(MesChiefdataClient mesChiefdataClient) {
        QueryWrapper<MesChiefdataClient> queryWrapper = new QueryWrapper();
        queryWrapper.eq("client_name", mesChiefdataClient.getClientName());
        if (ObjectHelper.isNotEmpty(mesChiefdataClientMapper.selectList(queryWrapper))) {
            return Result.error("客户名称已存在，添加失败！");
        }
        mesChiefdataClientMapper.insert(mesChiefdataClient);
        return Result.ok("添加成功！");
    }

    @Override
    public Result<?> edit(MesChiefdataClient mesChiefdataClient) {
        QueryWrapper<MesChiefdataClient> queryWrapper = new QueryWrapper();
        queryWrapper.eq("client_name", mesChiefdataClient.getClientName());
        List<MesChiefdataClient> list = mesChiefdataClientMapper.selectList(queryWrapper);
        if (ObjectHelper.isNotEmpty(list) && !mesChiefdataClient.getId().equals(list.get(0).getId())) {
            return Result.error("客户名称已存在，编辑失败！");
        }
        mesChiefdataClientMapper.updateById(mesChiefdataClient);
        return Result.ok("编辑成功!");
    }
}
