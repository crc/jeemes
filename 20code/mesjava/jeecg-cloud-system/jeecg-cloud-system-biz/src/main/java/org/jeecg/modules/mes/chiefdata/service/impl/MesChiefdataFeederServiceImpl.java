package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataFeeder;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataFeederMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataFeederService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—FEEDER建档
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataFeederServiceImpl extends ServiceImpl<MesChiefdataFeederMapper, MesChiefdataFeeder> implements IMesChiefdataFeederService {

}
