package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataForbidmaterial;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataForbidmaterialMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataForbidmaterialService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—禁用料
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataForbidmaterialServiceImpl extends ServiceImpl<MesChiefdataForbidmaterialMapper, MesChiefdataForbidmaterial> implements IMesChiefdataForbidmaterialService {

}
