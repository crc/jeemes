package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataGoodsgroup;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataGoodsgroupMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataGoodsgroupService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—产品组
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataGoodsgroupServiceImpl extends ServiceImpl<MesChiefdataGoodsgroupMapper, MesChiefdataGoodsgroup> implements IMesChiefdataGoodsgroupService {

}
