package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataProductgroup;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataProductgroupMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataProductgroupService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—商品组
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataProductgroupServiceImpl extends ServiceImpl<MesChiefdataProductgroupMapper, MesChiefdataProductgroup> implements IMesChiefdataProductgroupService {

}
