package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataProductprice;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataProductpriceMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataProductpriceService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—商品价格
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataProductpriceServiceImpl extends ServiceImpl<MesChiefdataProductpriceMapper, MesChiefdataProductprice> implements IMesChiefdataProductpriceService {

}
