package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataReceiveitem;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataReceiveitemMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataReceiveitemService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—收款条款
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataReceiveitemServiceImpl extends ServiceImpl<MesChiefdataReceiveitemMapper, MesChiefdataReceiveitem> implements IMesChiefdataReceiveitemService {

}
