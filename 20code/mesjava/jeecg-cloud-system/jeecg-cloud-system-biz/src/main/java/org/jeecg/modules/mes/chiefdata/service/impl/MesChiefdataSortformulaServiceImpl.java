package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSortformula;
import org.jeecg.modules.mes.chiefdata.entity.MesSortformulaStrategyinfo;
import org.jeecg.modules.mes.chiefdata.mapper.MesSortformulaStrategyinfoMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataSortformulaMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataSortformulaService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 主数据—拣货方案
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataSortformulaServiceImpl extends ServiceImpl<MesChiefdataSortformulaMapper, MesChiefdataSortformula> implements IMesChiefdataSortformulaService {

	@Autowired
	private MesChiefdataSortformulaMapper mesChiefdataSortformulaMapper;
	@Autowired
	private MesSortformulaStrategyinfoMapper mesSortformulaStrategyinfoMapper;
	
	@Override
	@Transactional
	public void saveMain(MesChiefdataSortformula mesChiefdataSortformula, List<MesSortformulaStrategyinfo> mesSortformulaStrategyinfoList) {
		mesChiefdataSortformulaMapper.insert(mesChiefdataSortformula);
		if(mesSortformulaStrategyinfoList!=null && mesSortformulaStrategyinfoList.size()>0) {
			for(MesSortformulaStrategyinfo entity:mesSortformulaStrategyinfoList) {
				//外键设置
				entity.setSortId(mesChiefdataSortformula.getId());
				mesSortformulaStrategyinfoMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesChiefdataSortformula mesChiefdataSortformula,List<MesSortformulaStrategyinfo> mesSortformulaStrategyinfoList) {
		mesChiefdataSortformulaMapper.updateById(mesChiefdataSortformula);
		
		//1.先删除子表数据
		mesSortformulaStrategyinfoMapper.deleteByMainId(mesChiefdataSortformula.getId());
		
		//2.子表数据重新插入
		if(mesSortformulaStrategyinfoList!=null && mesSortformulaStrategyinfoList.size()>0) {
			for(MesSortformulaStrategyinfo entity:mesSortformulaStrategyinfoList) {
				//外键设置
				entity.setSortId(mesChiefdataSortformula.getId());
				mesSortformulaStrategyinfoMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesSortformulaStrategyinfoMapper.deleteByMainId(id);
		mesChiefdataSortformulaMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesSortformulaStrategyinfoMapper.deleteByMainId(id.toString());
			mesChiefdataSortformulaMapper.deleteById(id);
		}
	}
	
}
