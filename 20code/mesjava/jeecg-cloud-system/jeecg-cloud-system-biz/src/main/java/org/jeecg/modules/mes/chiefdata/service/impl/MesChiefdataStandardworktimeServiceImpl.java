package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataStandardworktime;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataStandardworktimeMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataStandardworktimeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—标准工时
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataStandardworktimeServiceImpl extends ServiceImpl<MesChiefdataStandardworktimeMapper, MesChiefdataStandardworktime> implements IMesChiefdataStandardworktimeService {

}
