package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataUnitgroup;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataUnitgroupMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataUnitgroupService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—单位组
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataUnitgroupServiceImpl extends ServiceImpl<MesChiefdataUnitgroupMapper, MesChiefdataUnitgroup> implements IMesChiefdataUnitgroupService {

}
