package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielOther;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielOtherMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterielOtherService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 物料—其他
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Service
public class MesMaterielOtherServiceImpl extends ServiceImpl<MesMaterielOtherMapper, MesMaterielOther> implements IMesMaterielOtherService {
	
	@Autowired
	private MesMaterielOtherMapper mesMaterielOtherMapper;
	
	@Override
	public List<MesMaterielOther> selectByMainId(String mainId) {
		return mesMaterielOtherMapper.selectByMainId(mainId);
	}
}
