package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesProductgroupCount;
import org.jeecg.modules.mes.chiefdata.mapper.MesProductgroupCountMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesProductgroupCountService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—商品组计费
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesProductgroupCountServiceImpl extends ServiceImpl<MesProductgroupCountMapper, MesProductgroupCount> implements IMesProductgroupCountService {

}
