package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesScanDocket;
import org.jeecg.modules.mes.chiefdata.mapper.MesScanDocketMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesScanDocketService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—扫描单据
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesScanDocketServiceImpl extends ServiceImpl<MesScanDocketMapper, MesScanDocket> implements IMesScanDocketService {

}
