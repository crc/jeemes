package org.jeecg.modules.mes.client;

import org.jeecg.common.constant.ServiceNameConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Component
@FeignClient(contextId = "InspectServiceClient",value = ServiceNameConstants.INSPECT_SERVICE,qualifier = "inspectService")
public interface InspectClient {

    @GetMapping("inspect/mesIpqcInspect/inspectTotle")
    public String inspectTotle();
    @GetMapping("inspect/mesIpqcInspect/inspectOutTotle")
    public String inspectOutTotle();
    @GetMapping("inspect/mesIpqcInspect/inspectIqcOutSum")
    public String inspectIqcOutSum();
}
