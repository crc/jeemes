package org.jeecg.modules.mes.mesapp.service.impl;

import org.jeecg.modules.mes.mesapp.entity.MesAppFunction;
import org.jeecg.modules.mes.mesapp.mapper.MesAppFunctionMapper;
import org.jeecg.modules.mes.mesapp.service.IMesAppFunctionService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—APP功能模块
 * @Author: jeecg-boot
 * @Date:   2020-10-14
 * @Version: V1.0
 */
@Service
public class MesAppFunctionServiceImpl extends ServiceImpl<MesAppFunctionMapper, MesAppFunction> implements IMesAppFunctionService {

}
