package org.jeecg.modules.mes.mesapp.service.impl;

import org.jeecg.modules.mes.mesapp.entity.MesAppRole;
import org.jeecg.modules.mes.mesapp.mapper.MesAppRoleMapper;
import org.jeecg.modules.mes.mesapp.service.IMesAppRoleService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—APP角色
 * @Author: jeecg-boot
 * @Date:   2020-10-14
 * @Version: V1.0
 */
@Service
public class MesAppRoleServiceImpl extends ServiceImpl<MesAppRoleMapper, MesAppRole> implements IMesAppRoleService {

}
