package org.jeecg.modules.mes.mesapp.service.impl;

import org.jeecg.modules.mes.mesapp.entity.MesAppUser;
import org.jeecg.modules.mes.mesapp.mapper.MesAppUserMapper;
import org.jeecg.modules.mes.mesapp.service.IMesAppUserService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—用户功能
 * @Author: jeecg-boot
 * @Date:   2020-10-14
 * @Version: V1.0
 */
@Service
public class MesAppUserServiceImpl extends ServiceImpl<MesAppUserMapper, MesAppUser> implements IMesAppUserService {

}
