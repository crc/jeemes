package org.jeecg.modules.mes.organize.service;

import org.jeecg.modules.mes.organize.entity.MesOrganizeCompany;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 组织—公司
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesOrganizeCompanyService extends IService<MesOrganizeCompany> {

}
