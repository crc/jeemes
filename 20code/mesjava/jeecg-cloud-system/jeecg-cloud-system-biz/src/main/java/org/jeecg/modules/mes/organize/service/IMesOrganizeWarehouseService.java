package org.jeecg.modules.mes.organize.service;

import org.jeecg.modules.mes.organize.entity.MesOrganizeWarehouse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 组织—仓库
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesOrganizeWarehouseService extends IService<MesOrganizeWarehouse> {

}
