package org.jeecg.modules.mes.organize.service.impl;

import org.jeecg.modules.mes.organize.entity.MesOrganizeCalendar;
import org.jeecg.modules.mes.organize.entity.MesOrganizeHoliday;
import org.jeecg.modules.mes.organize.entity.MesOrganizeClasstype;
import org.jeecg.modules.mes.organize.mapper.MesOrganizeHolidayMapper;
import org.jeecg.modules.mes.organize.mapper.MesOrganizeClasstypeMapper;
import org.jeecg.modules.mes.organize.mapper.MesOrganizeCalendarMapper;
import org.jeecg.modules.mes.organize.service.IMesOrganizeCalendarService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 组织—日历
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesOrganizeCalendarServiceImpl extends ServiceImpl<MesOrganizeCalendarMapper, MesOrganizeCalendar> implements IMesOrganizeCalendarService {

	@Autowired
	private MesOrganizeCalendarMapper mesOrganizeCalendarMapper;
	@Autowired
	private MesOrganizeHolidayMapper mesOrganizeHolidayMapper;
	@Autowired
	private MesOrganizeClasstypeMapper mesOrganizeClasstypeMapper;
	
	@Override
	@Transactional
	public void delMain(String id) {
		mesOrganizeHolidayMapper.deleteByMainId(id);
		mesOrganizeClasstypeMapper.deleteByMainId(id);
		mesOrganizeCalendarMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesOrganizeHolidayMapper.deleteByMainId(id.toString());
			mesOrganizeClasstypeMapper.deleteByMainId(id.toString());
			mesOrganizeCalendarMapper.deleteById(id);
		}
	}
	
}
