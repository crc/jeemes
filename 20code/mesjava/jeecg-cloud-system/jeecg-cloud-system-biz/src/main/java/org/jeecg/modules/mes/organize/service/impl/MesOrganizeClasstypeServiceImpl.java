package org.jeecg.modules.mes.organize.service.impl;

import org.jeecg.modules.mes.organize.entity.MesOrganizeClasstype;
import org.jeecg.modules.mes.organize.mapper.MesOrganizeClasstypeMapper;
import org.jeecg.modules.mes.organize.service.IMesOrganizeClasstypeService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 组织—日历（班别）
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesOrganizeClasstypeServiceImpl extends ServiceImpl<MesOrganizeClasstypeMapper, MesOrganizeClasstype> implements IMesOrganizeClasstypeService {
	
	@Autowired
	private MesOrganizeClasstypeMapper mesOrganizeClasstypeMapper;
	
	@Override
	public List<MesOrganizeClasstype> selectByMainId(String mainId) {
		return mesOrganizeClasstypeMapper.selectByMainId(mainId);
	}
}
