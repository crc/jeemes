package org.jeecg.modules.mes.client;

import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.storage.entity.MesLackMaterial;
import org.jeecg.modules.mes.storage.entity.MesMaterielOccupy;
import org.jeecg.modules.mes.storage.entity.MesStockManage;
import org.jeecg.modules.mes.storage.entity.MesStorageWholesale;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(contextId = "StockServiceClient", value = ServiceNameConstants.WAREHOUSE_SERVICE)
public interface StockClient {

    @GetMapping("storage/mesStockManage/queryByMcode")
    public MesStockManage queryByMcode(@RequestParam(name="mcode",required=true) String mcode);

    /**
     * 根据物料料号和客户名称查询库存
     */
    @GetMapping("storage/mesStockManage/queryByMcodeAndClient")
    public MesStockManage queryByMcodeAndClient(@RequestParam(name="mCode") String mCode,
                                                @RequestParam(name="clientName") String clientName);

    @PutMapping("storage/mesStockManage/stockEdit")
    public String stockEdit(@RequestBody MesStockManage mesStockManage);


    @PostMapping("storage/mesLackMaterial/addLack")
    public String addLack(@RequestBody MesLackMaterial mesLackMaterial);

    @DeleteMapping("storage/mesLackMaterial/deleteByorderId")
    public String deleteByorderId(@RequestParam(name="orderId",required=true) String orderId,
                                  @RequestParam(name="mCode",required=false) String mCode);

    @GetMapping("storage/mesLackMaterial/getByOrderItemId")
    public MesLackMaterial getByOrderItemId(@RequestParam(name="orderItemId",required=true) String orderItemId);

    @GetMapping("storage/mesLackMaterial/queryByOrderId")
    public List<MesLackMaterial> queryByOrderId(@RequestParam(name="orderId",required=true) String orderId);

    @PutMapping("storage/mesLackMaterial/myEdit")
    public String myEdit(@RequestBody MesLackMaterial mesLackMaterial);

    @PostMapping("storage/mesMaterielOccupy/addOccupy")
    public String addOccupy(@RequestBody MesMaterielOccupy mesMaterielOccupy);

    @DeleteMapping("storage/mesMaterielOccupy/deleteOccupyByorderId")
    public String deleteOccupyByorderId(@RequestParam(name="orderId",required=true) String orderId);

    @GetMapping("storage/mesMaterielOccupy/MoccupyByOrderId")
    public List<MesMaterielOccupy> MoccupyByOrderId(@RequestParam(name="orderId",required=true) String orderId);

    @GetMapping("storage/mesMaterielOccupy/queryByIdAndmCode")
    public MesMaterielOccupy queryByIdAndmCode(@RequestParam(name="orderId",required=true) String orderId,
                                          @RequestParam(name="mCode",required=true) String mCode);

    @GetMapping(value = "storage/mesStorageWholesale/findById")
    public MesStorageWholesale findById(@RequestParam(name="id") String id);


}
