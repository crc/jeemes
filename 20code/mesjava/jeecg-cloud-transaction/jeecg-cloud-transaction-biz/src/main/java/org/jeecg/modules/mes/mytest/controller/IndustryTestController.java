package org.jeecg.modules.ems.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.ems.entity.IndustryTest;
import org.jeecg.modules.ems.service.IIndustryTestService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.ems.entity.IndustryTest;
import org.jeecg.modules.ems.service.IIndustryTestService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 测试表
 * @Author: jeecg-boot
 * @Date:   2020-08-31
 * @Version: V1.0
 */
@Api(tags="测试表")
@RestController
@RequestMapping("/ems/industryTest")
@Slf4j
public class IndustryTestController extends JeecgController<IndustryTest, IIndustryTestService> {
	@Autowired
	private IIndustryTestService industryTestService;

	@RequestMapping("/showname/{name}")
	public String showname(@PathVariable("name") String name){
		return "My name is "+name;
	}
	
	/**
	 * 分页列表查询
	 *
	 * @param industryTest
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "测试表-分页列表查询")
	@ApiOperation(value="测试表-分页列表查询", notes="测试表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(IndustryTest industryTest,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<IndustryTest> queryWrapper = QueryGenerator.initQueryWrapper(industryTest, req.getParameterMap());
		Page<IndustryTest> page = new Page<IndustryTest>(pageNo, pageSize);
		IPage<IndustryTest> pageList = industryTestService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param industryTest
	 * @return
	 */
	@AutoLog(value = "测试表-添加")
	@ApiOperation(value="测试表-添加", notes="测试表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody IndustryTest industryTest) {
//		LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//		System.err.println(loginUser.getId());
		System.err.println("调用到了这里！");
		industryTestService.save(industryTest);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param industryTest
	 * @return
	 */
	@AutoLog(value = "测试表-编辑")
	@ApiOperation(value="测试表-编辑", notes="测试表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody IndustryTest industryTest) {
		industryTestService.updateById(industryTest);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "测试表-通过id删除")
	@ApiOperation(value="测试表-通过id删除", notes="测试表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		industryTestService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "测试表-批量删除")
	@ApiOperation(value="测试表-批量删除", notes="测试表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.industryTestService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "测试表-通过id查询")
	@ApiOperation(value="测试表-通过id查询", notes="测试表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		IndustryTest industryTest = industryTestService.getById(id);
		if(industryTest==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(industryTest);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param industryTest
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, IndustryTest industryTest) {
        return super.exportXls(request, industryTest, IndustryTest.class, "测试表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, IndustryTest.class);
    }

}
