package org.jeecg.modules.mes.order.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.order.entity.ExpApplyBill;
import org.jeecg.modules.mes.order.service.IExpApplyBillService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 出口申请单
 * @Author: jeecg-boot
 * @Date:   2021-04-06
 * @Version: V1.0
 */
@Api(tags="出口申请单")
@RestController
@RequestMapping("/order/expApplyBill")
@Slf4j
public class ExpApplyBillController extends JeecgController<ExpApplyBill, IExpApplyBillService> {
	@Autowired
	private IExpApplyBillService expApplyBillService;
	
	/**
	 * 分页列表查询
	 *
	 * @param expApplyBill
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "出口申请单-分页列表查询")
	@ApiOperation(value="出口申请单-分页列表查询", notes="出口申请单-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ExpApplyBill expApplyBill,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ExpApplyBill> queryWrapper = QueryGenerator.initQueryWrapper(expApplyBill, req.getParameterMap());
		Page<ExpApplyBill> page = new Page<ExpApplyBill>(pageNo, pageSize);
		IPage<ExpApplyBill> pageList = expApplyBillService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param expApplyBill
	 * @return
	 */
	@AutoLog(value = "出口申请单-添加")
	@ApiOperation(value="出口申请单-添加", notes="出口申请单-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ExpApplyBill expApplyBill) {
		expApplyBillService.save(expApplyBill);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param expApplyBill
	 * @return
	 */
	@AutoLog(value = "出口申请单-编辑")
	@ApiOperation(value="出口申请单-编辑", notes="出口申请单-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ExpApplyBill expApplyBill) {
		expApplyBillService.updateById(expApplyBill);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "出口申请单-通过id删除")
	@ApiOperation(value="出口申请单-通过id删除", notes="出口申请单-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		expApplyBillService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "出口申请单-批量删除")
	@ApiOperation(value="出口申请单-批量删除", notes="出口申请单-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.expApplyBillService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "出口申请单-通过id查询")
	@ApiOperation(value="出口申请单-通过id查询", notes="出口申请单-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ExpApplyBill expApplyBill = expApplyBillService.getById(id);
		if(expApplyBill==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(expApplyBill);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param expApplyBill
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ExpApplyBill expApplyBill) {
        return super.exportXls(request, expApplyBill, ExpApplyBill.class, "出口申请单");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ExpApplyBill.class);
    }

}
