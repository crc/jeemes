package org.jeecg.modules.mes.order.controller;

import org.jeecg.common.system.query.QueryGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import java.util.Arrays;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.order.entity.MesAskpurchaseItem;
import org.jeecg.modules.mes.order.entity.MesOrderAskpurchase;
import org.jeecg.modules.mes.order.service.IMesOrderAskpurchaseService;
import org.jeecg.modules.mes.order.service.IMesAskpurchaseItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 订单管理—请购单
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Api(tags="订单管理—请购单")
@RestController
@RequestMapping("/order/mesOrderAskpurchase")
@Slf4j
public class MesOrderAskpurchaseController extends JeecgController<MesOrderAskpurchase, IMesOrderAskpurchaseService> {

	@Autowired
	private IMesOrderAskpurchaseService mesOrderAskpurchaseService;

	@Autowired
	private IMesAskpurchaseItemService mesAskpurchaseItemService;


	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * @param mesOrderAskpurchase
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "订单管理—请购单-分页列表查询")
	@ApiOperation(value="订单管理—请购单-分页列表查询", notes="订单管理—请购单-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesOrderAskpurchase mesOrderAskpurchase,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesOrderAskpurchase> queryWrapper = QueryGenerator.initQueryWrapper(mesOrderAskpurchase, req.getParameterMap());
		Page<MesOrderAskpurchase> page = new Page<MesOrderAskpurchase>(pageNo, pageSize);
		IPage<MesOrderAskpurchase> pageList = mesOrderAskpurchaseService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
     *   添加
     * @param mesOrderAskpurchase
     * @return
     */
    @AutoLog(value = "订单管理—请购单-添加")
    @ApiOperation(value="订单管理—请购单-添加", notes="订单管理—请购单-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesOrderAskpurchase mesOrderAskpurchase) {
        mesOrderAskpurchaseService.save(mesOrderAskpurchase);
        return Result.ok("添加成功！");
    }

    /**
     *  编辑
     * @param mesOrderAskpurchase
     * @return
     */
    @AutoLog(value = "订单管理—请购单-编辑")
    @ApiOperation(value="订单管理—请购单-编辑", notes="订单管理—请购单-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesOrderAskpurchase mesOrderAskpurchase) {
        mesOrderAskpurchaseService.updateById(mesOrderAskpurchase);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "订单管理—请购单-通过id删除")
    @ApiOperation(value="订单管理—请购单-通过id删除", notes="订单管理—请购单-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name="id",required=true) String id) {
        mesOrderAskpurchaseService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "订单管理—请购单-批量删除")
    @ApiOperation(value="订单管理—请购单-批量删除", notes="订单管理—请购单-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.mesOrderAskpurchaseService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesOrderAskpurchase mesOrderAskpurchase) {
        return super.exportXls(request, mesOrderAskpurchase, MesOrderAskpurchase.class, "订单管理—请购单");
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesOrderAskpurchase.class);
    }
	/*---------------------------------主表处理-end-------------------------------------*/
	

    /*--------------------------------子表处理-订单管理—请购单子表-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "订单管理—请购单子表-通过主表ID查询")
	@ApiOperation(value="订单管理—请购单子表-通过主表ID查询", notes="订单管理—请购单子表-通过主表ID查询")
	@GetMapping(value = "/listMesAskpurchaseItemByMainId")
    public Result<?> listMesAskpurchaseItemByMainId(MesAskpurchaseItem mesAskpurchaseItem,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<MesAskpurchaseItem> queryWrapper = QueryGenerator.initQueryWrapper(mesAskpurchaseItem, req.getParameterMap());
        Page<MesAskpurchaseItem> page = new Page<MesAskpurchaseItem>(pageNo, pageSize);
        IPage<MesAskpurchaseItem> pageList = mesAskpurchaseItemService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

	/**
	 * 添加
	 * @param mesAskpurchaseItem
	 * @return
	 */
	@AutoLog(value = "订单管理—请购单子表-添加")
	@ApiOperation(value="订单管理—请购单子表-添加", notes="订单管理—请购单子表-添加")
	@PostMapping(value = "/addMesAskpurchaseItem")
	public Result<?> addMesAskpurchaseItem(@RequestBody MesAskpurchaseItem mesAskpurchaseItem) {
		mesAskpurchaseItemService.save(mesAskpurchaseItem);
		return Result.ok("添加成功！");
	}

    /**
	 * 编辑
	 * @param mesAskpurchaseItem
	 * @return
	 */
	@AutoLog(value = "订单管理—请购单子表-编辑")
	@ApiOperation(value="订单管理—请购单子表-编辑", notes="订单管理—请购单子表-编辑")
	@PutMapping(value = "/editMesAskpurchaseItem")
	public Result<?> editMesAskpurchaseItem(@RequestBody MesAskpurchaseItem mesAskpurchaseItem) {
		mesAskpurchaseItemService.updateById(mesAskpurchaseItem);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "订单管理—请购单子表-通过id删除")
	@ApiOperation(value="订单管理—请购单子表-通过id删除", notes="订单管理—请购单子表-通过id删除")
	@DeleteMapping(value = "/deleteMesAskpurchaseItem")
	public Result<?> deleteMesAskpurchaseItem(@RequestParam(name="id",required=true) String id) {
		mesAskpurchaseItemService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "订单管理—请购单子表-批量删除")
	@ApiOperation(value="订单管理—请购单子表-批量删除", notes="订单管理—请购单子表-批量删除")
	@DeleteMapping(value = "/deleteBatchMesAskpurchaseItem")
	public Result<?> deleteBatchMesAskpurchaseItem(@RequestParam(name="ids",required=true) String ids) {
	    this.mesAskpurchaseItemService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportMesAskpurchaseItem")
    public ModelAndView exportMesAskpurchaseItem(HttpServletRequest request, MesAskpurchaseItem mesAskpurchaseItem) {
		 // Step.1 组装查询条件
		 QueryWrapper<MesAskpurchaseItem> queryWrapper = QueryGenerator.initQueryWrapper(mesAskpurchaseItem, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<MesAskpurchaseItem> pageList = mesAskpurchaseItemService.list(queryWrapper);
		 List<MesAskpurchaseItem> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "订单管理—请购单子表"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, MesAskpurchaseItem.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("订单管理—请购单子表报表", "导出人:" + sysUser.getRealname(), "订单管理—请购单子表"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importMesAskpurchaseItem/{mainId}")
    public Result<?> importMesAskpurchaseItem(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<MesAskpurchaseItem> list = ExcelImportUtil.importExcel(file.getInputStream(), MesAskpurchaseItem.class, params);
				 for (MesAskpurchaseItem temp : list) {
                    temp.setAskId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 mesAskpurchaseItemService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.ok("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-订单管理—请购单子表-end----------------------------------------------*/




}
