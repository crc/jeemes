package org.jeecg.modules.mes.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.jeecg.modules.mes.order.entity.MesPurchaseItem;

import java.util.List;

/**
 * @Description: 订单管理—采购订单子表
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface MesPurchaseItemMapper extends BaseMapper<MesPurchaseItem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);

	public List<MesPurchaseItem> selectByMainId(@Param("mainId") String mainId);

	/**
	 * 更新采购订单子表未收货数量
	 * @param id
	 * @return
	 */
	@Update("UPDATE `jeecg-transaction`.mes_purchase_item set unreceive_num=purchase_num-(select IFNULL(SUM(inware_num),0) from `jeecg-warehouse`.mes_storage_wholesale where base_dockettype='扫描收货' and base_code=#{id}) where id=#{id};")
	int uploadUnreceiveNum(@Param("id") String id);
}
