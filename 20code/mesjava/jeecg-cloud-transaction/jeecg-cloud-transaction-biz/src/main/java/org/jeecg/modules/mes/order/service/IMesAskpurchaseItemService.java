package org.jeecg.modules.mes.order.service;

import org.jeecg.modules.mes.order.entity.MesAskpurchaseItem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 订单管理—请购单子表
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesAskpurchaseItemService extends IService<MesAskpurchaseItem> {

	public List<MesAskpurchaseItem> selectByMainId(String mainId);
}
