package org.jeecg.modules.mes.order.service;

import org.jeecg.modules.mes.order.entity.MesAskpurchaseItem;
import org.jeecg.modules.mes.order.entity.MesOrderAskpurchase;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 订单管理—请购单
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesOrderAskpurchaseService extends IService<MesOrderAskpurchase> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);


}
