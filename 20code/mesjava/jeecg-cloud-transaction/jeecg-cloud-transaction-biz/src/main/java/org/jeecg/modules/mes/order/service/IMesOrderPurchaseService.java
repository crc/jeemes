package org.jeecg.modules.mes.order.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.order.entity.*;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 订单管理—采购订单
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesOrderPurchaseService extends IService<MesOrderPurchase> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

	/**
	 * 生成进口单得时候修改状态，并保存进口单
	 * @param order
	 * @param billlist
	 */
	public void saveImpUpdateState(MesOrderPurchase order, List<ImpApplyBill> billlist);

	/**
	 * 添加采购订单并且根据物料编号生成采购订单子表信息
	 * @param order
	 */
	public void addAll(MesOrderPurchase order);

    Result<?> queryMesPurchaseItemListByMainId(String id, String gauge, String materielCode, String ifFinish);

	Result<?> queryMesPurchaseItemsByMainId(String id, String gauge, String materielCode, String ifFinish, Page<MesPurchaseItem> page);
}
