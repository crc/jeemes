package org.jeecg.modules.mes.order.service.impl;

import org.jeecg.common.util.ApplyBillDBUtil;
import org.jeecg.modules.mes.order.entity.ImpApplyBill;
import org.jeecg.modules.mes.order.mapper.ImpApplyBillMapper;
import org.jeecg.modules.mes.order.service.IImpApplyBillService;
import org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 进口申请单
 * @Author: jeecg-boot
 * @Date:   2021-04-06
 * @Version: V1.0
 */
@Service
public class ImpApplyBillServiceImpl extends ServiceImpl<ImpApplyBillMapper, ImpApplyBill> implements IImpApplyBillService {

    @Autowired
    private ImpApplyBillMapper impApplyBillMapper;

    @Override
    public List<ImpApplyBill> selectByMainId(String mainId) {
        return impApplyBillMapper.selectByMainId(mainId);
    }

}
