package org.jeecg.modules.mes.client;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.inspect.entity.MaterielRetreatRecord;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(contextId = "InspectServiceClient", value = ServiceNameConstants.INSPECT_SERVICE)
public interface InspectClient {

    @PostMapping("/inspect/materielRetreatRecord/add")
    Result<?> add(@RequestBody MaterielRetreatRecord materielRetreatRecord);

}
