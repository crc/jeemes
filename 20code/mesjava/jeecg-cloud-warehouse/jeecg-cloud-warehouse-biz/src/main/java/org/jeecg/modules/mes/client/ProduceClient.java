package org.jeecg.modules.mes.client;


import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.produce.entity.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(contextId = "CommandBillServiceClient", value = ServiceNameConstants.PRODUCE_SERVICE)
public interface ProduceClient {

    @GetMapping("produce/mesCommandbillInfo/getById")
    public MesCommandbillInfo getById(@RequestParam(name = "id", required = true) String id);
    /**
     * 远程调用 根据制令单code查询制令单信息
     */
    @GetMapping("produce/mesCommandbillInfo/queryCommandBillInfoCode")
    public List<MesCommandbillInfo> queryCommandBillInfoCode(@RequestParam(name = "code")  String code);

    @PutMapping("produce/mesCommandbillInfo/editCommand")
    public boolean editCommand(@RequestBody MesCommandbillInfo mesCommandbillInfo);
    /**
     * 远程调用，通过制令单id和料号，获取制令单BOM的数据
     *
     * @param commandId
     * @param mCode
     * @return
     */
    @GetMapping(value = "produce/mesCommandbillPitem/getPitemById")
    public MesCommandbillPitem getPitemById(@RequestParam(name = "commandId", required = true) String commandId,
                                            @RequestParam(name = "mCode", required = true) String mCode);

    @GetMapping(value = "produce/mesCommandbillPitem/getItemByCommandIdAndType")
    public MesCommandbillPitem getItemByCommandIdAndType(@RequestParam(name = "commandId") String commandId,
                                                         @RequestParam(name = "materiel_type") String materiel_type);

    @GetMapping("produce/mesCommandbillPitem/queryPitemById")
    public MesCommandbillPitem queryPitemById(@RequestParam(name = "id", required = true) String id);

    /**
     * 远程调用，通过子表id获取制令单BOM主表的数据
     *
     * @param itemId
     * @return
     */
    @GetMapping(value = "produce/mesCommandbillPitem//queryCommandbillByItemId")
    public MesCommandbillInfo queryCommandbillByItemId(@RequestParam(name = "itemId", required = true) String itemId);

    @GetMapping(value = "/produce/mesCommandbillPitem/getCommandBomBycommandbillId")
    public String getCommandBomBycommandbillId(@RequestParam(name = "commandbillId", required = true) String commandbillId,@RequestParam(name = "materielCode", required = false) String materielCode);

    @GetMapping(value = "/produce/mesCommandbillInfo/checkProductStatusByCommandBillId")
    public Boolean checkProductStatusByCommandBillId(@RequestParam(name = "commandBillId", required = true) String commandBillId);

    @PutMapping("produce/mesCommandbillPitem/editComitem")
    public String editComitem(@RequestBody MesCommandbillPitem mesCommandbillPitem);

    @PostMapping("produce/mesCommandbillDlog/addLog")
    public Boolean addDLog(@RequestBody MesCommandbillDlog mesCommandbillDlog);

    @PostMapping("produce/mesCommandbillUlog/addLog")
    public Boolean addULog(@RequestBody MesCommandbillUlog mesCommandbillUlog);

    @GetMapping("produce/mesOnlineMaterieltable/queryBylineType")
    public MesOnlineMaterieltable queryBylineType(@RequestParam(name = "pcode", required = true) String pcode,
                                                  @RequestParam(name = "mcode", required = true) String mcode,
                                                  @RequestParam(name = "lineType", required = true) String lineType,
                                                  @RequestParam(name = "passage", required = false) String passage);

    @GetMapping("produce/mesCommandbillPitem/getPitemByCode")
    public MesCommandbillPitem getPitemByCode(@RequestParam(name = "commandCode", required = true) String commandCode,
                                              @RequestParam(name = "mCode", required = true) String mCode);

    @GetMapping(value = "produce/mesCourseScan/queryByCommandbillCode")
    public List<MesCourseScan> queryByCommandbillCode(@RequestParam(name="CommandbillCode",required=true) String CommandbillCode) ;

    @PutMapping(value = "produce/mesCourseScan/editItem")
    public boolean editItem(@RequestBody MesCourseScan mesCourseScan);

    @PostMapping(value = "produce/turnProduceRecord/turnProduceRecordadd")
    public boolean TurnProduceRecordadd(@RequestBody TurnProduceRecord turnProduceRecord);

    /**
     * 远程调用，查询是否有搅拌记录且状态为达标
     * @param sn
     * @return
     */
    @GetMapping(value = "produce/mesAncillarytoolStir/queryAncillaryBySn")
    public MesAncillarytoolStir queryAncillaryBySn(@RequestParam(name = "sn", required = true) String sn);



}
