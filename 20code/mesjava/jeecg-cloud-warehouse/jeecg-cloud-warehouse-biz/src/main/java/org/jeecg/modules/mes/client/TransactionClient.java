package org.jeecg.modules.mes.client;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.order.entity.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
@FeignClient(contextId = "TransactionServiceClient", value = ServiceNameConstants.TRANSACTION_SERVICE)
public interface TransactionClient {

    @GetMapping(value = "order/mesOrderPurchase/getOrderPurchaseById")
    public MesOrderPurchase getOrderPurchaseById(@RequestParam(name="id",required=true) String id);
    /**
     * 远程调用，通过采购订单编号模糊搜索数据 返回所有id拼接的字符串
     * @param code
     * @return id,id 字符串
     */
    @GetMapping(value = "order/mesOrderPurchase/getOrderPurchaseLikeCode")
    public List<String> getOrderPurchaseLikeCode(@RequestParam(name="code",required=true) String code);

    @PutMapping(value = "order/mesOrderPurchase/editPurchase")
    public String editPurchase(@RequestBody MesOrderPurchase mesOrderPurchase);

    @PutMapping("order/mesOrderPurchase/editPurchaseItem")
    public String editPurchaseItem(@RequestBody MesPurchaseItem mesPurchaseItem);

    @GetMapping("order/mesOrderPurchase/queryMesPurchaseItemById")
    public MesPurchaseItem queryMesPurchaseItemById(@RequestParam(name="id",required=true) String id);

    @GetMapping("/order/mesOrderPurchase/uploadUnreceiveNum")
    public Result<?> uploadUnreceiveNum(@RequestParam(name = "id") String id);

    @GetMapping("order/mesOrderSale/queryMesSaleItemById")
    public MesSaleItem queryMesSaleItemById(@RequestParam(name="id",required=true) String id);

    @PutMapping("order/mesOrderSale/editSaleItem")
    public String editSaleItem(@RequestBody MesSaleItem mesSaleItem);

    @GetMapping("order/mesOrderSale/queryMesSaleOrderById")
    public MesOrderSale queryMesSaleOrderById(@RequestParam(name="id",required=true) String id);

    /**
     * 远程调用模块，通过Code获取销售订单数据
     *
     * @param code
     * @return
     */
    @GetMapping(value = "order/mesOrderSale/queryMesSaleOrderByCode")
    public List<MesOrderSale> queryMesSaleOrderByCode(@RequestParam(name = "code", required = true) String code);

    @PutMapping("order/mesOrderSale/editSaleOrder")
    public String editSaleOrder(@RequestBody MesOrderSale mesOrderSale);

    @GetMapping("order/mesOrderProduce/queryMesProduceById")
    public MesOrderProduce queryMesProduceById(@RequestParam(name="id",required=true) String id);

    @GetMapping("order/mesOrderProduce/queryMesProduceItemById")
    public MesProduceItem queryMesProduceItemById(@RequestParam(name="id",required=true) String id);
    /**
     * 远程调用，通过子表id获取生产订单子表的数据
     *
     * @param orderid
     * @return
     */
    @GetMapping(value = "/queryMesProduceItemByOrderId")
    public MesOrderProduce queryMesProduceItemByOrderId(@RequestParam(name = "orderid", required = true) String orderid);

    @PutMapping("order/mesOrderProduce/editProduceItem")
    public String editProduceItem(@RequestBody MesProduceItem mesProduceItem);

    @PutMapping("order/mesOrderProduce/editProduce")
    public String editProduce(@RequestBody MesOrderProduce mesOrderProduce);

    @PutMapping("/outpro/mesOutproWater/add")
    public Result<?> add(@RequestBody MesOutproWater mesOutproWater);

    @GetMapping("/order/mesOrderProduce/queryById")
    public Result<?> queryById(@RequestParam(name = "id") String id);
    /**
     * 远程调用 通过id查询
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/order/mesOrderProduce/queryBysId")
    public MesOrderProduce queryBysId(@RequestParam(name = "id", required = true) String id);

}
