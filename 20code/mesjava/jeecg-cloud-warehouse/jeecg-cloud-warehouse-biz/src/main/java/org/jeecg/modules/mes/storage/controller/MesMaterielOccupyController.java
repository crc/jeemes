package org.jeecg.modules.mes.storage.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.client.TransactionClient;
import org.jeecg.modules.mes.order.entity.MesOrderProduce;
import org.jeecg.modules.mes.storage.entity.MesMaterielOccupy;
import org.jeecg.modules.mes.storage.entity.MesStockManage;
import org.jeecg.modules.mes.storage.service.IMesMaterielOccupyService;
import org.jeecg.modules.mes.storage.service.IMesStockManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * @Description: 仓库管理-领料清单
 * @Author: jeecg-boot
 * @Date: 2020-11-16
 * @Version: V1.0
 */
@Api(tags = "仓库管理-领料清单")
@RestController
@RequestMapping("/storage/mesMaterielOccupy")
@Slf4j
public class MesMaterielOccupyController extends JeecgController<MesMaterielOccupy, IMesMaterielOccupyService> {
	@Autowired
	private IMesMaterielOccupyService mesMaterielOccupyService;
	@Autowired
	private IMesStockManageService mesStockManageService;
	@Autowired
	private TransactionClient transactionClient;

	/**
	 * 分页列表查询
	 *
	 * @param mesMaterielOccupy
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "仓库管理-领料清单-分页列表查询")
	@ApiOperation(value = "仓库管理-领料清单-分页列表查询", notes = "仓库管理-领料清单-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesMaterielOccupy mesMaterielOccupy,
								   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
								   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesMaterielOccupy> queryWrapper = QueryGenerator.initQueryWrapper(mesMaterielOccupy, req.getParameterMap());
		Page<MesMaterielOccupy> page = new Page<MesMaterielOccupy>(pageNo, pageSize);
		IPage<MesMaterielOccupy> pageList = mesMaterielOccupyService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	@PostMapping(value = "/addOccupy")
	public String addOccupy(@RequestBody MesMaterielOccupy mesMaterielOccupy) {
		if (StringUtils.isNotBlank(mesMaterielOccupy.getOrderId()) && StringUtils.isNotBlank(mesMaterielOccupy.getOrderCode()) && StringUtils.isNotBlank(mesMaterielOccupy.getMaterielCode())) {
			String orderId = mesMaterielOccupy.getOrderId();
			String orderCode = mesMaterielOccupy.getOrderCode();
			String mCode = mesMaterielOccupy.getMaterielCode();
			QueryWrapper<MesMaterielOccupy> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("order_id", orderId).eq("order_code", orderCode).eq("materiel_code", mCode);
			MesMaterielOccupy occupy = mesMaterielOccupyService.getOne(queryWrapper);
			if (occupy == null) {
				mesMaterielOccupyService.save(mesMaterielOccupy);
			}
		}
		return "添加成功！";
	}

	/**
	 * 添加
	 *
	 * @param mesMaterielOccupy
	 * @return
	 */
	@AutoLog(value = "仓库管理-领料清单-添加")
	@ApiOperation(value = "仓库管理-领料清单-添加", notes = "仓库管理-领料清单-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesMaterielOccupy mesMaterielOccupy) {
		mesMaterielOccupyService.save(mesMaterielOccupy);
		return Result.ok("添加成功！");
	}

	/**
	 * 编辑
	 *
	 * @param mesMaterielOccupy
	 * @return
	 */
	@AutoLog(value = "仓库管理-领料清单-编辑")
	@ApiOperation(value = "仓库管理-领料清单-编辑", notes = "仓库管理-领料清单-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesMaterielOccupy mesMaterielOccupy) {
		mesMaterielOccupyService.updateById(mesMaterielOccupy);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "仓库管理-领料清单-通过id删除")
	@ApiOperation(value = "仓库管理-领料清单-通过id删除", notes = "仓库管理-领料清单-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
		mesMaterielOccupyService.removeById(id);
		return Result.ok("删除成功!");
	}


	@DeleteMapping(value = "/deleteOccupyByorderId")
	public String deleteOccupyByorderId(@RequestParam(name = "orderId", required = true) String orderId) {
		QueryWrapper<MesMaterielOccupy> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("order_id", orderId);
		List<MesMaterielOccupy> materielOccupyList = mesMaterielOccupyService.list(queryWrapper);
		if (materielOccupyList.size() != 0) {
			for (MesMaterielOccupy materielOccupy : materielOccupyList) {
				String mCode = materielOccupy.getMaterielCode();//物料编号
				BigDecimal occupyNum = new BigDecimal(materielOccupy.getOccupyNum());//领用数量
				//通过物料编号查询库存信息
				QueryWrapper<MesStockManage> wrapper = new QueryWrapper<>();
				wrapper.eq("materiel_code", mCode);
				MesStockManage stockManage = mesStockManageService.getOne(wrapper);
				if (stockManage != null) {
					//删除领料信息时，把领用的物料，返还给库存
					BigDecimal stockNum = new BigDecimal(stockManage.getStockNum());//库存数量
					BigDecimal newStockNum = stockNum.add(occupyNum);
					stockManage.setStockNum(newStockNum.toString());
					mesStockManageService.updateById(stockManage);
				}
			}
		}
		mesMaterielOccupyService.remove(queryWrapper);
		return "删除成功!";
	}

	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "仓库管理-领料清单-批量删除")
	@ApiOperation(value = "仓库管理-领料清单-批量删除", notes = "仓库管理-领料清单-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
		this.mesMaterielOccupyService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "仓库管理-领料清单-通过id查询")
	@ApiOperation(value = "仓库管理-领料清单-通过id查询", notes = "仓库管理-领料清单-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
		MesMaterielOccupy mesMaterielOccupy = mesMaterielOccupyService.getById(id);
		if (mesMaterielOccupy == null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesMaterielOccupy);
	}

	/**
	 * 根据生产订单id和物料料号查询领料清单  远程调用
	 *
	 * @param orderId
	 * @param mCode
	 * @return
	 */

	@GetMapping(value = "/queryByIdAndmCode")
	public MesMaterielOccupy queryByIdAndmCode(@RequestParam(name = "orderId", required = true) String orderId,
											   @RequestParam(name = "mCode", required = true) String mCode) {
		QueryWrapper<MesMaterielOccupy> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("order_id", orderId).eq("materiel_code", mCode);
		MesMaterielOccupy mesMaterielOccupy = mesMaterielOccupyService.getOne(queryWrapper);
		return mesMaterielOccupy;
	}

	/**
	 * 根据生产订单id和物料料号查询领料清单 并且更新领料表的转产数量  远程调用
	 *
	 * @param orderId
	 * @param mCode
	 * @return
	 */
	@GetMapping(value = "/queryByIdAndmCodeUpdateTransformNum")
	public MesMaterielOccupy queryByIdAndmCodeUpdateTransformNum(@RequestParam(name = "orderId", required = true) String orderId,
																 @RequestParam(name = "mCode", required = true) String mCode,
																 @RequestParam(name = "realNum", required = true) String realNum) {
		return mesMaterielOccupyService.queryByIdAndmCodeUpdateTransformNum(orderId,mCode,realNum);
	}

	/**
	 * 根据生产订单id和物料料号查询领料清单 并且更新领料表的发料数量  远程调用
	 *
	 * @param orderId
	 * @param mCode
	 * @return
	 */
	@GetMapping(value = "/queryByIdAndmCodeUpdateSendNum")
	public MesMaterielOccupy queryByIdAndmCodeUpdateSendNum(@RequestParam(name = "orderId", required = true) String orderId,
															@RequestParam(name = "mCode", required = true) String mCode,
															@RequestParam(name = "sendNum", required = true) String sendNum) {
		return mesMaterielOccupyService.queryByIdAndmCodeUpdateSendNum(orderId,mCode,sendNum);
	}

	/**
	 * 根据生产订单id和物料料号查询领料清单 并且更新领料表的退料数量 远程调用
	 *
	 * @param orderId
	 * @param mCode
	 * @return
	 */
	@GetMapping(value = "/queryByIdAndmCodeUpdatewithdrawNum")
	public MesMaterielOccupy queryByIdAndmCodeUpdatewithdrawNum(@RequestParam(name = "orderId", required = true) String orderId,
																@RequestParam(name = "mCode", required = true) String mCode,
																@RequestParam(name = "withdrawNum", required = true) String withdrawNum) {
		return mesMaterielOccupyService.queryByIdAndmCodeUpdatewithdrawNum(orderId,mCode,withdrawNum);
	}

	/**
	 * 根据生产订单id查询领料清单  远程调用
	 *
	 * @param orderId
	 * @return
	 */
	@GetMapping(value = "/MoccupyByOrderId")
	public List<MesMaterielOccupy> MoccupyByOrderId(@RequestParam(name = "orderId", required = true) String orderId) {
		QueryWrapper<MesMaterielOccupy> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("order_id", orderId);
		List<MesMaterielOccupy> mesMaterielOccupyList = mesMaterielOccupyService.list(queryWrapper);
		return mesMaterielOccupyList;
	}

	/**
	 * 根据生产订单生成领料清单 远程调用
	 */
	@PostMapping(value = "/addMaterielOccupyByProduce")
	public String addMaterielOccupyByProduce(@RequestBody MesOrderProduce mesOrderProduce) {
		return mesMaterielOccupyService.addMaterielOccupyByProduce(mesOrderProduce);
	}

	/**
	 * 导出excel
	 *
	 * @param request
	 * @param mesMaterielOccupy
	 */
	@RequestMapping(value = "/exportXls")
	public ModelAndView exportXls(HttpServletRequest request, MesMaterielOccupy mesMaterielOccupy) {
		return super.exportXls(request, mesMaterielOccupy, MesMaterielOccupy.class, "仓库管理-领料清单");
	}

	/**
	 * 通过excel导入数据
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/importExcel", method = RequestMethod.POST)
	public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		return super.importExcel(request, response, MesMaterielOccupy.class);
	}

}
