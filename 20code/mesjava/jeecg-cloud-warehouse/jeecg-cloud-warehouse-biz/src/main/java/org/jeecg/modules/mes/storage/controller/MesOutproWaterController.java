package org.jeecg.modules.mes.storage.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.order.entity.MesOutproWater;
import org.jeecg.modules.mes.storage.service.IMesOutproWaterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
* @Description: 出货流水报表
* @Author: jeecg-boot
* @Date:   2021-03-08
* @Version: V1.0
*/
@Api(tags="出货流水报表")
@RestController
@RequestMapping("/outpro/mesOutproWater")
@Slf4j
public class MesOutproWaterController extends JeecgController<MesOutproWater, IMesOutproWaterService> {
   @Autowired
   private IMesOutproWaterService mesOutproWaterService;

   /**
    * 分页列表查询
    *
    * @param mesOutproWater
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "出货流水报表-分页列表查询")
   @ApiOperation(value="出货流水报表-分页列表查询", notes="出货流水报表-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(MesOutproWater mesOutproWater,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<MesOutproWater> queryWrapper = QueryGenerator.initQueryWrapper(mesOutproWater, req.getParameterMap());
       Page<MesOutproWater> page = new Page<MesOutproWater>(pageNo, pageSize);
       IPage<MesOutproWater> pageList = mesOutproWaterService.page(page, queryWrapper);
       return Result.ok(pageList);
   }

   /**
    *   添加
    *
    * @param mesOutproWater
    * @return
    */
   @AutoLog(value = "出货流水报表-添加")
   @ApiOperation(value="出货流水报表-添加", notes="出货流水报表-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody MesOutproWater mesOutproWater) {
       mesOutproWaterService.save(mesOutproWater);
       return Result.ok("添加成功！");
   }

   /**
    *  编辑
    *
    * @param mesOutproWater
    * @return
    */
   @AutoLog(value = "出货流水报表-编辑")
   @ApiOperation(value="出货流水报表-编辑", notes="出货流水报表-编辑")
   @PutMapping(value = "/edit")
   public Result<?> edit(@RequestBody MesOutproWater mesOutproWater) {
       mesOutproWaterService.updateById(mesOutproWater);
       return Result.ok("编辑成功!");
   }

   /**
    *   通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "出货流水报表-通过id删除")
   @ApiOperation(value="出货流水报表-通过id删除", notes="出货流水报表-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       mesOutproWaterService.removeById(id);
       return Result.ok("删除成功!");
   }

   /**
    *  批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "出货流水报表-批量删除")
   @ApiOperation(value="出货流水报表-批量删除", notes="出货流水报表-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.mesOutproWaterService.removeByIds(Arrays.asList(ids.split(",")));
       return Result.ok("批量删除成功!");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "出货流水报表-通过id查询")
   @ApiOperation(value="出货流水报表-通过id查询", notes="出货流水报表-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       MesOutproWater mesOutproWater = mesOutproWaterService.getById(id);
       if(mesOutproWater==null) {
           return Result.error("未找到对应数据");
       }
       return Result.ok(mesOutproWater);
   }

   /**
   * 导出excel
   *
   * @param request
   * @param mesOutproWater
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, MesOutproWater mesOutproWater) {
       return super.exportXls(request, mesOutproWater, MesOutproWater.class, "出货流水报表");
   }

   /**
     * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
       return super.importExcel(request, response, MesOutproWater.class);
   }

}
