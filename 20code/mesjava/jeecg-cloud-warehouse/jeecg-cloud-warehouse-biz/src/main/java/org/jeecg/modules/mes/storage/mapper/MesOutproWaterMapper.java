package org.jeecg.modules.mes.storage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mes.order.entity.MesOutproWater;

/**
 * @Description: 出货流水报表
 * @Author: jeecg-boot
 * @Date:   2021-03-08
 * @Version: V1.0
 */
public interface MesOutproWaterMapper extends BaseMapper<MesOutproWater> {

}
