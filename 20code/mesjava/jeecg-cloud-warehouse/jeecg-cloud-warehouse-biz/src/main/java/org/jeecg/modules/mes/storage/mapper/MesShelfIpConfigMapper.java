package org.jeecg.modules.mes.storage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.storage.entity.MesShelfIpConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: mes_shelf_ip_config
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
public interface MesShelfIpConfigMapper extends BaseMapper<MesShelfIpConfig> {

}
