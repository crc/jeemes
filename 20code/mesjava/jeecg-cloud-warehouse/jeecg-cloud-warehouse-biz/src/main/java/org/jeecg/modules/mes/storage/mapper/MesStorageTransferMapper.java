package org.jeecg.modules.mes.storage.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.storage.entity.MesStorageTransfer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 仓库管理—调拨移动
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface MesStorageTransferMapper extends BaseMapper<MesStorageTransfer> {

}
