package org.jeecg.modules.mes.storage.mapper;

import java.util.List;
import org.jeecg.modules.mes.storage.entity.MesTransferItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 仓库管理—调拨移动子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface MesTransferItemMapper extends BaseMapper<MesTransferItem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesTransferItem> selectByMainId(@Param("mainId") String mainId);
}
