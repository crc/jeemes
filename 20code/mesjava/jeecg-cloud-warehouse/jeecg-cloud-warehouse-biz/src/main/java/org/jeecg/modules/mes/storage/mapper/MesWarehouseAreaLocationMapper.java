package org.jeecg.modules.mes.storage.mapper;

import java.util.List;
import org.jeecg.modules.mes.storage.entity.MesWarehouseAreaLocation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 仓库区域位置表
 * @Author: jeecg-boot
 * @Date:   2021-05-13
 * @Version: V1.0
 */
public interface MesWarehouseAreaLocationMapper extends BaseMapper<MesWarehouseAreaLocation> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesWarehouseAreaLocation> selectByMainId(@Param("mainId") String mainId);

	List<MesWarehouseAreaLocation> selectByLocationCode(@Param("locationCode") String locationCode);

}
