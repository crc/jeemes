package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesAcceptanceItem;
import org.jeecg.modules.mes.storage.entity.MesAcceptanceSite;
import org.jeecg.modules.mes.storage.entity.MesStorageAcceptance;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 仓库管理—验收入库
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesStorageAcceptanceService extends IService<MesStorageAcceptance> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesStorageAcceptance mesStorageAcceptance,List<MesAcceptanceItem> mesAcceptanceItemList,List<MesAcceptanceSite> mesAcceptanceSiteList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesStorageAcceptance mesStorageAcceptance,List<MesAcceptanceItem> mesAcceptanceItemList,List<MesAcceptanceSite> mesAcceptanceSiteList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
