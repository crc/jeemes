package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesStorageAssign;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 仓库管理—拣配单
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesStorageAssignService extends IService<MesStorageAssign> {

}
