package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesStorageBatchwaresite;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 仓库管理—批次库位库存
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesStorageBatchwaresiteService extends IService<MesStorageBatchwaresite> {

}
