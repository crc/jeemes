package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesCountItem;
import org.jeecg.modules.mes.storage.entity.MesStorageCount;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 仓库管理—盘点
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesStorageCountService extends IService<MesStorageCount> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesStorageCount mesStorageCount,List<MesCountItem> mesCountItemList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesStorageCount mesStorageCount,List<MesCountItem> mesCountItemList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
