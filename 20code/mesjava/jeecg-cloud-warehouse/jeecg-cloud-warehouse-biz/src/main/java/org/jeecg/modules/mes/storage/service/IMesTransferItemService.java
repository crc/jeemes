package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesTransferItem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 仓库管理—调拨移动子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesTransferItemService extends IService<MesTransferItem> {

	public List<MesTransferItem> selectByMainId(String mainId);
}
