package org.jeecg.modules.mes.storage.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.storage.entity.MesWarehouseAreaLocation;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 仓库区域位置表
 * @Author: jeecg-boot
 * @Date:   2021-05-13
 * @Version: V1.0
 */
public interface IMesWarehouseAreaLocationService extends IService<MesWarehouseAreaLocation> {

	public List<MesWarehouseAreaLocation> selectByMainId(String mainId);

    Result<?> addMesWarehouseAreaLocation(MesWarehouseAreaLocation mesWarehouseAreaLocation);

    Result<?> editMesWarehouseAreaLocation(MesWarehouseAreaLocation mesWarehouseAreaLocation);

    Result<?> deleteMesWarehouseAreaLocation(String id);

    Result<?> deleteBatchMesWarehouseAreaLocation(List<String> asList);

    MesWarehouseAreaLocation getAreaLocationByClient(String clientName);
}
