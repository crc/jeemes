package org.jeecg.modules.mes.storage.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.storage.entity.MesWarehouseArea;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.Serializable;
import java.util.Collection;

/**
 * @Description: 仓库区域表
 * @Author: jeecg-boot
 * @Date: 2021-05-13
 * @Version: V1.0
 */
public interface IMesWarehouseAreaService extends IService<MesWarehouseArea> {

    /**
     * 删除一对多
     */
    public void delMain(String id);

    /**
     * 批量删除一对多
     */
    public void delBatchMain(Collection<? extends Serializable> idList);


    Result<?> add(MesWarehouseArea mesWarehouseArea);

    Result<?> edit(MesWarehouseArea mesWarehouseArea);

    MesWarehouseArea getMesWarehouseAreaByClientName(String clientName);

    Result<?> queryListByAreaCode(String areaCode);

    Result<?> queryListByLocationCode(String areaId, String locationCode);
}
