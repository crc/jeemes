package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesAcceptanceItem;
import org.jeecg.modules.mes.storage.mapper.MesAcceptanceItemMapper;
import org.jeecg.modules.mes.storage.service.IMesAcceptanceItemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 仓库管理—验收入库子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesAcceptanceItemServiceImpl extends ServiceImpl<MesAcceptanceItemMapper, MesAcceptanceItem> implements IMesAcceptanceItemService {
	
	@Autowired
	private MesAcceptanceItemMapper mesAcceptanceItemMapper;
	
	@Override
	public List<MesAcceptanceItem> selectByMainId(String mainId) {
		return mesAcceptanceItemMapper.selectByMainId(mainId);
	}
}
