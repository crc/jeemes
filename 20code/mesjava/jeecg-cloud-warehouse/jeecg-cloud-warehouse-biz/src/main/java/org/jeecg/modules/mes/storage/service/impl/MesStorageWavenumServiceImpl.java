package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesStorageWavenum;
import org.jeecg.modules.mes.storage.entity.MesWavenumItem;
import org.jeecg.modules.mes.storage.mapper.MesWavenumItemMapper;
import org.jeecg.modules.mes.storage.mapper.MesStorageWavenumMapper;
import org.jeecg.modules.mes.storage.service.IMesStorageWavenumService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 仓库管理—波次单
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesStorageWavenumServiceImpl extends ServiceImpl<MesStorageWavenumMapper, MesStorageWavenum> implements IMesStorageWavenumService {

	@Autowired
	private MesStorageWavenumMapper mesStorageWavenumMapper;
	@Autowired
	private MesWavenumItemMapper mesWavenumItemMapper;
	
	@Override
	@Transactional
	public void saveMain(MesStorageWavenum mesStorageWavenum, List<MesWavenumItem> mesWavenumItemList) {
		mesStorageWavenumMapper.insert(mesStorageWavenum);
		if(mesWavenumItemList!=null && mesWavenumItemList.size()>0) {
			for(MesWavenumItem entity:mesWavenumItemList) {
				//外键设置
				entity.setWaveId(mesStorageWavenum.getId());
				mesWavenumItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesStorageWavenum mesStorageWavenum,List<MesWavenumItem> mesWavenumItemList) {
		mesStorageWavenumMapper.updateById(mesStorageWavenum);
		
		//1.先删除子表数据
		mesWavenumItemMapper.deleteByMainId(mesStorageWavenum.getId());
		
		//2.子表数据重新插入
		if(mesWavenumItemList!=null && mesWavenumItemList.size()>0) {
			for(MesWavenumItem entity:mesWavenumItemList) {
				//外键设置
				entity.setWaveId(mesStorageWavenum.getId());
				mesWavenumItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesWavenumItemMapper.deleteByMainId(id);
		mesStorageWavenumMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesWavenumItemMapper.deleteByMainId(id.toString());
			mesStorageWavenumMapper.deleteById(id);
		}
	}
	
}
