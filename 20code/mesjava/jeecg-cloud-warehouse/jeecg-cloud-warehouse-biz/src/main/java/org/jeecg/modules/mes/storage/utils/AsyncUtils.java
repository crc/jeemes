package org.jeecg.modules.mes.storage.utils;

import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.mes.client.SystemClient;
import org.jeecg.modules.mes.order.entity.MesPurchaseItem;
import org.jeecg.modules.mes.organize.entity.MesShelfUtil;
import org.jeecg.modules.mes.storage.entity.MesShelfData;
import org.jeecg.modules.mes.storage.entity.MesShelfIpConfig;
import org.jeecg.modules.mes.storage.entity.MesShelfLight;
import org.jeecg.modules.mes.storage.service.IMesShelfIpConfigService;
import org.jeecg.modules.system.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AsyncUtils{

    @Autowired
    private SystemClient systemClient;
    @Autowired
    private IMesShelfIpConfigService mesShelfIpConfigService;

    /**
     * 异步给质检员发送质检信息
     * @param purchaseItem
     */
    @Async
    public void asyncSend(MesPurchaseItem purchaseItem){
        //收货完成，发送质检提醒
        List<SysUser> sysUserList = systemClient.queryRoleByCode("pinzhi");
        if (sysUserList !=null && sysUserList.size() > 0) {
            for (SysUser user : sysUserList) {
                //收货完成，发送质检提醒
                systemClient.sendCheckMessage(user.getUsername(),purchaseItem.getMaterielName(),purchaseItem.getPurchaseNum(),purchaseItem.getOrderUnit());
                try {Thread.sleep(200);} catch (InterruptedException e) {e.printStackTrace();}
            }

        }
    }

    /**
     * 异步执行，入库的时候亮红灯2秒后，再关闭货架灯光
     * @param shelfCode 货架编码
     */
    @Async
    public void asyncSleep(String shelfCode){
        System.out.println("====asyncSleep:扫描入库亮灯============1");
        try{
            MesShelfIpConfig mesShelfIpConfig =  mesShelfIpConfigService.lambdaQuery()
                    .eq(MesShelfIpConfig::getShelfCodePre,shelfCode.split("-")[0]).one();
            if (mesShelfIpConfig == null) {
                throw new JeecgBootException("货架没有配置Host");
            }
            String url = mesShelfIpConfig.getShelfHost();//智能货架的URl

            List<MesShelfData> shelfDataList = new ArrayList<>();
            MesShelfData mesShelfData = new MesShelfData();
            mesShelfData.setCell_num(shelfCode);//货架编码
            mesShelfData.setIndex(1);
            mesShelfData.setColor("red");//颜色 红色
            mesShelfData.setBlink(0);
            shelfDataList.add(mesShelfData);
            MesShelfLight mesShelfLight = ShelfLight(shelfDataList);
            MesShelfUtil.litShelfLight(url, mesShelfLight);

            System.out.println("开始异步休眠2秒");
            Thread.sleep(2000);
            System.out.println("异步休眠休眠结束");

            //2秒后，将货架的灯关掉
            shelfDataList.get(0).setColor("black");//black：关闭货架灯
            MesShelfLight mesShelfLight2 = ShelfLight(shelfDataList);
            MesShelfUtil.litShelfLight(url, mesShelfLight2);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     *  调用智能货架，自己封装的实体类
     * @param shelfDataList
     * @return
     */
    public static  MesShelfLight ShelfLight(List<MesShelfData> shelfDataList) {
        MesShelfLight mesShelfLight = new MesShelfLight();
        mesShelfLight.setControl_caty("A3");//第三排货架
        mesShelfLight.setApp_id(12345678);//固定值，无特殊意义
        List<MesShelfData> data = new ArrayList<>();
        data.addAll(shelfDataList);
        mesShelfLight.setData(data);
        return mesShelfLight;
    }
}
