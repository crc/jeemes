package org.jeecg.modules.mes.storage.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

@Data
public class MaterUseReport implements Serializable {

    @Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private java.lang.String query4;
    @Excel(name = "基本单据类型", width = 15)
    @ApiModelProperty(value = "基本单据类型")
    private java.lang.String baseDockettype;
    @Excel(name = "产品编号", width = 15)
    @ApiModelProperty(value = "产品编号")
    private java.lang.String productCode;
    @Excel(name = "产品名称", width = 15)
    @ApiModelProperty(value = "产品名称")
    private java.lang.String productName;
    @Excel(name = "物料二维码", width = 15)
    @ApiModelProperty(value = "物料二维码")
    private java.lang.String glazeId;
    @Excel(name = "仓库/通道", width = 15)
    @ApiModelProperty(value = "仓库/通道")
    private java.lang.String wareSite;
    @Excel(name = "数量", width = 15)
    @ApiModelProperty(value = "数量")
    private java.lang.String inwareNum;
    @Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
    @Excel(name = "工厂/feeder", width = 15)
    @ApiModelProperty(value = "工厂/feeder")
    private java.lang.String query2;
    @Excel(name = "操作人", width = 15)
    @ApiModelProperty(value = "操作人")
    private java.lang.String createBy;
    @Excel(name = "操作时间", width = 15 , format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "操作时间")
    private java.util.Date createTime;
}
