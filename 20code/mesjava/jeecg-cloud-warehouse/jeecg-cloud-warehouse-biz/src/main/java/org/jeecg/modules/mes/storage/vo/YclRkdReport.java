package org.jeecg.modules.mes.storage.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
public class YclRkdReport implements Serializable {
    @Excel(name = "订单编号", width = 15)
    @ApiModelProperty(value = "订单编号")
    private String attr1;
    @Excel(name = "料号", width = 15)
    @ApiModelProperty(value = "料号")
    private String attr2;
    @Excel(name = "客户料号", width = 15)
    @ApiModelProperty(value = "客户料号")
    private String attr3;
    @Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private String attr4;
    @Excel(name = "规格", width = 15)
    @ApiModelProperty(value = "规格")
    private String attr5;
    @Excel(name = "伏数", width = 15)
    @ApiModelProperty(value = "伏数")
    private String attr6;
    @Excel(name = "误差", width = 15)
    @ApiModelProperty(value = "误差")
    private String attr7;
    @Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private String attr8;
    @Excel(name = "申购数量", width = 15)
    @ApiModelProperty(value = "申购数量")
    private String attr9;
    @Excel(name = "实收数量", width = 15)
    @ApiModelProperty(value = "实收数量")
    private String attr10;
    @Excel(name = "日期", width = 15)
    @ApiModelProperty(value = "日期")
    private String attr11;
    @Excel(name = "供应商", width = 15)
    @ApiModelProperty(value = "供应商")
    private String attr12;
    @Excel(name = "入库数量", width = 15)
    @ApiModelProperty(value = "入库数量")
    private String attr13;
    @Excel(name = "区域编码", width = 15)
    @ApiModelProperty(value = "区域编码")
    private String attr14;
    @Excel(name = "位置编码", width = 15)
    @ApiModelProperty(value = "位置编码")
    private String attr15;
}
