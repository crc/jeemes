package com.epms.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 工具类
 */
public class ObjectHelper {

    static Logger logger = LoggerFactory.getLogger(ObjectHelper.class);

    public static boolean isEquals(Object object1, Object object2) {
        boolean ret = false;
        try {
            if (object1 == null && object2 == null) {
                ret = true;
                return ret;
            }
            ret = object1.equals(object2);
        } catch (NullPointerException e) {
            ret = false;
        }
        return ret;

    }

    /**
     * 比较两字符串是否相等，忽略大小写
     *
     * @param s1
     * @param s2
     * @return
     */
    public static boolean equalsIgnorecase(String s1, String s2) {
        if (s1 == null && s2 == null)
            return true;
        if (s1 != null && s2 != null) {
            if (s1.toLowerCase().equals(s2.toLowerCase()))
                return true;
        }
        return false;
    }

    /**
     * 判断对象是否为空
     *
     * @param obj
     *            -参数对象
     * @return boolean -true:表示对象为空;false:表示对象为非空 集合： Collection.isEmpty()
     *         数组：判断数组每个元素，所有元素都为空，即判定数组为空
     *         字符串：判断字符串等于"null"，或去除两端""字窜后返回String.isEmpty()的结果 其它类型返回 false
     */
    public static boolean isEmpty(Object obj) {
        if (obj == null)
            return true;

        if (obj instanceof Map) {
            return ((Map<?, ?>) obj).entrySet().isEmpty();
        }

        if (obj instanceof Collection) {
            return ((Collection<?>) obj).isEmpty();
        }

        if (obj instanceof String) {
            return ((String) obj).equalsIgnoreCase("null") | ((String) obj).trim().isEmpty();
        }

        if (obj instanceof StringBuffer) {
            return ((StringBuffer) obj).length() == 0;
        }

        if (obj.getClass().isArray()) {
            try {
                Object[] a = (Object[]) obj;

                boolean b = true;
                for (Object o : a) {
                    b = b & ObjectHelper.isEmpty(o);

                    if (!b)
                        break;
                }

                return b;
            } catch (ClassCastException e) {
            }
        }

        return false;
    }

    /**
     *
     * @param obj
     * @return
     */
    public static boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }

    /**
     * 校验邮箱是否合规
     *
     * @param email
     *            邮箱
     * @return true 合规 false 不合规
     */
    public static boolean isEmailFormat(String email) {

        Pattern pattern = Pattern
                .compile("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$");
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isEmailAdressFormat(String email) {
        boolean isExist = false;
        if (isEmpty(email)) {
            return isExist;
        }
        Pattern p = Pattern.compile("\\w+@(\\w+\\.)+[a-z]{2,3}");
        Matcher m = p.matcher(email);
        boolean b = m.matches();
        if (b) {
            isExist = true;
        }
        return isExist;
    }


    /**
     * 将string 转换成 回调函数+名称
     *
     * @param str
     * @param jsonCallBack
     * @return
     */
    public static String objectToJson(String str, String jsonCallBack) {
        return jsonCallBack + "('" + str + "')";
    }

    /**
     * 将string 转换成 回调函数+名称
     *
     * @param jsonCallBack
     * @return
     */
    public static String objectToJson(Boolean flag, String jsonCallBack) {
        return jsonCallBack + "(" + flag + ")";
    }

    /**
     * 判断obj是否实现了type接口,如果type不是接口类型，直接返回false
     *
     * @param obj
     *            要检测的对象
     * @param type
     *            接口的类型
     * @return
     */
    public static boolean isImplement(Object obj, Class<?> type) {
        if (!type.isInterface()) {
            return false;
        }
        Class<?>[] clzz = obj.getClass().getInterfaces();

        for (Class<?> clz : clzz) {
            if (clz.getName().equals(type.getName())) {
                return true;
            }
        }

        return false;
    }

    /**
     * 字符集转换(utf-8--->iso-8859-1)
     *
     * @param source
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String decodeFromUtfToCode(String source)
            throws UnsupportedEncodingException {
        return decodeFromUtfToCode(source, "iso-8859-1");
    }

    /**
     * 字符集转换(utf-8--->code)
     *
     * @param source
     * @param code
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String decodeFromUtfToCode(String source, String code)
            throws UnsupportedEncodingException {
        if (ObjectHelper.isEmpty(source))
            return null;
        return new String(source.getBytes("utf-8"), code);
    }

    public static int fractionsByString(String judgeStr){
        int i = 0;
        if(ObjectHelper.isEmpty(judgeStr)||!judgeStr.matches("([0-9]+.[0-9]+|[0-9]+)")){
            return -1;
        }
        String str[] = judgeStr.split("\\.");
        if(str.length==2){
            String  fractions = str[1];
            return fractions.length();
        }
        return i;
    }
}
