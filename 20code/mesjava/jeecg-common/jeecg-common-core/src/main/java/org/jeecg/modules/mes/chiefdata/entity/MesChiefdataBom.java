package org.jeecg.modules.mes.chiefdata.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 主数据—BOM
 * @Author: jeecg-boot
 * @Date:   2020-11-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_bom")
@ApiModel(value="mes_chiefdata_bom对象", description="主数据—BOM")
public class MesChiefdataBom implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**物料id*/
    @Excel(name = "物料id", width = 15)
    @ApiModelProperty(value = "物料id")
    private java.lang.String materialId;
	/** 成品料号*/
    @Excel(name ="成品料号", width = 15)
    @ApiModelProperty(value = "成品料号")
    private java.lang.String machinesortCode;
	/**商品编码*/
    @Excel(name = "商品编码", width = 15)
    @ApiModelProperty(value = "商品编码")
    private java.lang.String productCode;
	/**成品名称*/
    @Excel(name = "成品名称", width = 15)
    @ApiModelProperty(value = "成品名称")
    private java.lang.String materialName;
	/**成品规格*/
    @Excel(name = "成品规格", width = 15)
    @ApiModelProperty(value = "成品规格")
    private java.lang.String materialGauge;
    /**单位*/
    @Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
    /**类别*/
    @Excel(name = "类别", width = 15)
    @ApiModelProperty(value = "类别")
    private java.lang.String materialType;
    /**BOM有效期*/
    @Excel(name = "BOM有效期", width = 15)
    @ApiModelProperty(value = "BOM有效期")
    private java.lang.String expiryDate;
	/**版本*/
    @Excel(name = "版本", width = 15)
    @ApiModelProperty(value = "版本")
    private java.lang.String edition;
    /**批次号*/
//    @Excel(name = "批次号", width = 15)
    @ApiModelProperty(value = "批次号")
    private java.lang.String sectionNo;
    /**公司编码*/
//    @Excel(name = "公司编码", width = 15)
    @ApiModelProperty(value = "公司编码")
    private java.lang.String companyCode;
	/**Top文件*/
    @Excel(name = "Top文件", width = 15)
    @ApiModelProperty(value = "Top文件")
    private java.lang.String topFiles;
	/**BOTTOM文件*/
    @Excel(name = "BOTTOM文件", width = 15)
    @ApiModelProperty(value = "BOTTOM文件")
    private java.lang.String bottomFiles;
	/**坐标文件*/
    @Excel(name = "坐标文件", width = 15)
    @ApiModelProperty(value = "坐标文件")
    private java.lang.String coordinateFiles;
	/**备注*/
    @Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
	/**生产阶别*/
    @Excel(name = "生产阶别", width = 15, dicCode = "produce_grade")
    @Dict(dicCode = "produce_grade")
    @ApiModelProperty(value = "生产阶别")
    private java.lang.String query2;
	/**备用3*/
//    @Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
//    @Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
    /**是否报关（默认为0）*/
    @Excel(name = "是否报关（默认为0）", width = 15)
    @ApiModelProperty(value = "是否报关（默认为0）")
    private java.lang.String importState;
}
