package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—物料组
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_materielgroup")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_materielgroup对象", description="主数据—物料组")
public class MesChiefdataMaterielgroup implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**物料组编码*/
	@Excel(name = "物料组编码", width = 15)
    @ApiModelProperty(value = "物料组编码")
    private java.lang.String groupCode;
	/**物料组名称*/
	@Excel(name = "物料组名称", width = 15)
    @ApiModelProperty(value = "物料组名称")
    private java.lang.String groupName;
	/**物料组类型*/
	@Excel(name = "物料组类型", width = 15, dicCode = "group_type")
	@Dict(dicCode = "group_type")
    @ApiModelProperty(value = "物料组类型")
    private java.lang.String groupType;
	/**物料打印模版*/
	@Excel(name = "物料打印模版", width = 15)
    @ApiModelProperty(value = "物料打印模版")
    private java.lang.String materielPrintmodel;
	/**物料条码规则*/
	@Excel(name = "物料条码规则", width = 15)
    @ApiModelProperty(value = "物料条码规则")
    private java.lang.String materielBarcode;
	/**最小安全库存*/
	@Excel(name = "最小安全库存", width = 15)
    @ApiModelProperty(value = "最小安全库存")
    private java.lang.String minStock;
	/**最大安全库存*/
	@Excel(name = "最大安全库存", width = 15)
    @ApiModelProperty(value = "最大安全库存")
    private java.lang.String maxStock;
	/**保质期*/
	@Excel(name = "保质期", width = 15)
    @ApiModelProperty(value = "保质期")
    private java.lang.String shelfLife;
	/**到期提醒时间*/
	@Excel(name = "到期提醒时间", width = 15)
    @ApiModelProperty(value = "到期提醒时间")
    private java.lang.String duedateRemind;
	/**MSD管控标志*/
	@Excel(name = "MSD管控标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "MSD管控标志")
    private java.lang.String msdControl;
	/**MSD规则*/
	@Excel(name = "MSD规则", width = 15)
    @ApiModelProperty(value = "MSD规则")
    private java.lang.String msdRule;
	/**库存复检周期*/
	@Excel(name = "库存复检周期", width = 15)
    @ApiModelProperty(value = "库存复检周期")
    private java.lang.String stockCheckcycle;
	/**复检提醒天数*/
	@Excel(name = "复检提醒天数", width = 15)
    @ApiModelProperty(value = "复检提醒天数")
    private java.lang.String checkRemindays;
	/**呆滞周期*/
	@Excel(name = "呆滞周期", width = 15)
    @ApiModelProperty(value = "呆滞周期")
    private java.lang.String tranceCycle;
	/**体积*/
	@Excel(name = "体积", width = 15)
    @ApiModelProperty(value = "体积")
    private java.lang.String bulk;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
