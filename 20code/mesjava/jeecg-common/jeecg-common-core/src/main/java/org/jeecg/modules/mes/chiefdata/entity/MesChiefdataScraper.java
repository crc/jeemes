package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 刮刀建档
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_scraper")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_scraper对象", description="刮刀建档")
public class MesChiefdataScraper implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**刮刀SN*/
	@Excel(name = "刮刀SN", width = 15)
    @ApiModelProperty(value = "刮刀SN")
    private java.lang.String scraperSn;
	/**刮刀名称*/
	@Excel(name = "刮刀名称", width = 15)
    @ApiModelProperty(value = "刮刀名称")
    private java.lang.String scraperName;
	/**刮刀规格*/
	@Excel(name = "刮刀规格", width = 15)
    @ApiModelProperty(value = "刮刀规格")
    private java.lang.String scraperGague;
	/**刮刀类型*/
	@Excel(name = "刮刀类型", width = 15)
    @ApiModelProperty(value = "刮刀类型")
    private java.lang.String scraperType;
	/**使用点数上限*/
	@Excel(name = "使用点数上限", width = 15)
    @ApiModelProperty(value = "使用点数上限")
    private java.lang.String pointNumLimit;
	/**当前使用点数*/
	@Excel(name = "当前使用点数", width = 15)
    @ApiModelProperty(value = "当前使用点数")
    private java.lang.String pointUsageNum;
}
