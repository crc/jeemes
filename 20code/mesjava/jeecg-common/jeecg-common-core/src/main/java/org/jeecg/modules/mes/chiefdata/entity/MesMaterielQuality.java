package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 物料—品质
 * @Author: jeecg-boot
 * @Date:   2020-11-17
 * @Version: V1.0
 */
@ApiModel(value="mes_chiefdata_materiel对象", description="主数据—物料")
@Data
@TableName("mes_materiel_quality")
public class MesMaterielQuality implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**保质期(天)*/
	@Excel(name = "保质期(天)", width = 15)
	@ApiModelProperty(value = "保质期(天)")
	private java.lang.String shelfLife;
	/**过期提醒时间*/
	@Excel(name = "过期提醒时间", width = 15)
	@ApiModelProperty(value = "过期提醒时间")
	private java.lang.String duedateRemind;
	/**FQC送检*/
	@Excel(name = "FQC送检", width = 15, dicCode = "yn")
	@ApiModelProperty(value = "FQC送检")
	private java.lang.String fqcCheck;
	/**送检数量*/
	@Excel(name = "送检数量", width = 15)
	@ApiModelProperty(value = "送检数量")
	private java.lang.String checkNum;
	/**不良报废*/
	@Excel(name = "不良报废", width = 15, dicCode = "yn")
	@ApiModelProperty(value = "不良报废")
	private java.lang.String writeOff;
	/**封样测试*/
	@Excel(name = "封样测试", width = 15, dicCode = "yn")
	@ApiModelProperty(value = "封样测试")
	private java.lang.String demoTest;
	/**维修上限次数*/
	@Excel(name = "维修上限次数", width = 15)
	@ApiModelProperty(value = "维修上限次数")
	private java.lang.String fixNum;
	/**阻容上限*/
	@Excel(name = "阻容上限", width = 15)
	@ApiModelProperty(value = "阻容上限")
	private java.lang.String upperLimit;
	/**阻容下限*/
	@Excel(name = "阻容下限", width = 15)
	@ApiModelProperty(value = "阻容下限")
	private java.lang.String downLimit;
	/**阻容单位*/
	@Excel(name = "阻容单位", width = 15, dicCode = "resistance_unit")
	@ApiModelProperty(value = "阻容单位")
	private java.lang.String resistanceUnit;
	/**库存复检周期*/
	@Excel(name = "库存复检周期", width = 15)
	@ApiModelProperty(value = "库存复检周期")
	private java.lang.String checkCycle;
	/**复检提醒天数*/
	@Excel(name = "复检提醒天数", width = 15)
	@ApiModelProperty(value = "复检提醒天数")
	private java.lang.String remindDays;
	/**物料id*/
	@ApiModelProperty(value = "物料id")
	private java.lang.String materielId;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
