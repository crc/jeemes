package org.jeecg.modules.mes.inspect.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 物料退料记录表
 * @Author: jeecg-boot
 * @Date:   2021-04-25
 * @Version: V1.0
 */
@Data
@TableName("materiel_retreat_record")
@ApiModel(value="materiel_retreat_record对象", description="物料退料记录表")
public class MaterielRetreatRecord implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**订单号*/
    @Excel(name = "订单号", width = 15)
    @ApiModelProperty(value = "订单号")
    private java.lang.String orderCode;
	/**日期*/
    @Excel(name = "日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "日期")
    private java.util.Date birDay;
	/**客户料号*/
    @Excel(name = "客户料号", width = 15)
    @ApiModelProperty(value = "客户料号")
    private java.lang.String clientCode;
	/**物料料号*/
    @Excel(name = "物料料号", width = 15)
    @ApiModelProperty(value = "物料料号")
    private java.lang.String materielCode;
	/**品名*/
    @Excel(name = "品名", width = 15)
    @ApiModelProperty(value = "品名")
    private java.lang.String materielName;
	/**规格*/
    @Excel(name = "规格", width = 15)
    @ApiModelProperty(value = "规格")
    private java.lang.String materielSpecs;
	/**单位*/
    @Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
	/**退料数量*/
    @Excel(name = "退料数量", width = 15)
    @ApiModelProperty(value = "退料数量")
    private java.lang.String retreatNum;
	/**实收数量*/
    @Excel(name = "实收数量", width = 15)
    @ApiModelProperty(value = "实收数量")
    private java.lang.String uretreatNum;
	/**箱数*/
    @Excel(name = "箱数", width = 15)
    @ApiModelProperty(value = "箱数")
    private java.lang.String xsNum;
	/**毛重（KG）*/
    @Excel(name = "毛重（KG）", width = 15)
    @ApiModelProperty(value = "毛重（KG）")
    private java.lang.String mzNum;
	/**状态（0 质检前、1 质检后）*/
    @Excel(name = "状态（0 质检前、1 质检后）", width = 15)
    @ApiModelProperty(value = "状态（0 质检前、1 质检后）")
    private java.lang.String state;
	/**备注*/
    @Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remarks;
    /**现象数组*/
    @TableField(exist = false)
    private List<MaterielRetreatReason> reasonList;

    /**破损*/
    @Excel(name = "破损", width = 15)
    @ApiModelProperty(value = "破损")
    @TableField(exist = false)
    private java.lang.String reason1;
    /**断裂*/
    @Excel(name = "断裂", width = 15)
    @ApiModelProperty(value = "断裂")
    @TableField(exist = false)
    private java.lang.String reason2;
    /**划伤*/
    @Excel(name = "划伤", width = 15)
    @ApiModelProperty(value = "划伤")
    @TableField(exist = false)
    private java.lang.String reason3;
    /**来料损件*/
    @Excel(name = "来料损件", width = 15)
    @ApiModelProperty(value = "来料损件")
    @TableField(exist = false)
    private java.lang.String reason4;
    /**尺寸不符*/
    @Excel(name = "尺寸不符", width = 15)
    @ApiModelProperty(value = "尺寸不符")
    @TableField(exist = false)
    private java.lang.String reason5;
    /**功能不良*/
    @Excel(name = "功能不良", width = 15)
    @ApiModelProperty(value = "功能不良")
    @TableField(exist = false)
    private java.lang.String reason6;
    /**实物与焊盘不符*/
    @Excel(name = "实物与焊盘不符", width = 15)
    @ApiModelProperty(value = "实物与焊盘不符")
    @TableField(exist = false)
    private java.lang.String reason7;
    /**料号不符*/
    @Excel(name = "料号不符", width = 15)
    @ApiModelProperty(value = "料号不符")
    @TableField(exist = false)
    private java.lang.String reason8;
    /**线束不良*/
    @Excel(name = "线束不良", width = 15)
    @ApiModelProperty(value = "线束不良")
    @TableField(exist = false)
    private java.lang.String reason9;
    /**型号不符*/
    @Excel(name = "型号不符", width = 15)
    @ApiModelProperty(value = "型号不符")
    @TableField(exist = false)
    private java.lang.String reason10;
    /**其他*/
    @Excel(name = "其他", width = 15)
    @ApiModelProperty(value = "其他")
    @TableField(exist = false)
    private java.lang.String reason11;

    /**凭证抬头id*/
    @Excel(name = "凭证项目-预留编号", width = 15)
    @ApiModelProperty(value = "凭证项目-预留编号")
    @TableField(exist = false)
    private java.lang.String reserveCode;

    /**凭证抬头id*/
    @Excel(name = "凭证项目id", width = 15)
    @ApiModelProperty(value = "凭证项目id")
    @TableField(exist = false)
    private java.lang.String perkItemId;


}
