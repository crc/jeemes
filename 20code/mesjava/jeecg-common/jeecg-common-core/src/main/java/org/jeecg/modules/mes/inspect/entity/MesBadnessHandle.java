package org.jeecg.modules.mes.inspect.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 质检中心-不良处理
 * @Author: jeecg-boot
 * @Date:   2020-11-09
 * @Version: V1.0
 */
@Data
@TableName("mes_badness_handle")
@ApiModel(value="mes_badness_handle对象", description="质检中心-不良处理")
public class MesBadnessHandle implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
    /**检验单号*/
    @Excel(name = "检验单号", width = 15)
    @ApiModelProperty(value = "检验单号")
    private java.lang.String inspectCode;
	/**单号名称*/
    @Excel(name = "单号名称", width = 15)
    @ApiModelProperty(value = "单号名称")
    private java.lang.String matproSn;
	/**检测阶别*/
    @Excel(name = "检测阶别", width = 15)
    @ApiModelProperty(value = "检测阶别")
    private java.lang.String inspectGrade;
	/**物料料号*/
    @Excel(name = "物料料号", width = 15)
    @ApiModelProperty(value = "物料料号")
    private java.lang.String materielCode;
	/**物料名称*/
    @Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private java.lang.String materielName;
	/**物料规格*/
    @Excel(name = "物料规格", width = 15)
    @ApiModelProperty(value = "物料规格")
    private java.lang.String materielGague;
	/**处理方式*/
    @Excel(name = "处理方式", width = 15)
    @ApiModelProperty(value = "处理方式")
    private java.lang.String handleWay;
	/**关联单号*/
    @Excel(name = "关联单号", width = 15)
    @ApiModelProperty(value = "关联单号")
    private java.lang.String relatedCode;
	/**替换SN*/
    @Excel(name = "替换SN", width = 15)
    @ApiModelProperty(value = "替换SN")
    private java.lang.String replaceSn;
	/**维修内容*/
    @Excel(name = "维修内容", width = 15)
    @ApiModelProperty(value = "维修内容")
    private java.lang.String repairContent;
    /**检测数量*/
    @Excel(name = "检测数量", width = 15)
    @ApiModelProperty(value = "检测数量")
    private java.lang.String inspectNum;
	/**不良数量*/
    @Excel(name = "不良数量", width = 15)
    @ApiModelProperty(value = "不良数量")
    private java.lang.String badNum;
	/**不良率*/
    @Excel(name = "不良率", width = 15)
    @ApiModelProperty(value = "不良率")
    private java.lang.String badRate;
	/**检验合格*/
    @Excel(name = "检验合格", width = 15)
    @ApiModelProperty(value = "检验合格")
    private java.lang.String inspectQualify;
	/**检验员*/
    @Excel(name = "检验员", width = 15)
    @ApiModelProperty(value = "检验员")
    private java.lang.String inspectPerson;
	/**审核*/
    @Excel(name = "审核", width = 15)
    @ApiModelProperty(value = "审核")
    private java.lang.String verify;
	/**受理人*/
    @Excel(name = "受理人", width = 15)
    @ApiModelProperty(value = "受理人")
    private java.lang.String dealPerosn;
	/**受理时间*/
    @Excel(name = "受理时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "受理时间")
    private java.util.Date dealTime;
	/**原因分析及处理措施*/
    @Excel(name = "原因分析及处理措施", width = 15)
    @ApiModelProperty(value = "原因分析及处理措施")
    private java.lang.String reasonAnalyze;
	/**效果确认及描述*/
    @Excel(name = "效果确认及描述", width = 15)
    @ApiModelProperty(value = "效果确认及描述")
    private java.lang.String effectConfirm;
	/**确认人*/
    @Excel(name = "确认人", width = 15)
    @ApiModelProperty(value = "确认人")
    private java.lang.String confirmPerson;
	/**确认时间*/
    @Excel(name = "确认时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "确认时间")
    private java.util.Date confirmTime;
}
