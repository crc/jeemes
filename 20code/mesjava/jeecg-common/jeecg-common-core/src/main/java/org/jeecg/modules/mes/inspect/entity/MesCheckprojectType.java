package org.jeecg.modules.mes.inspect.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecg.common.aspect.annotation.Dict;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 检测类型信息
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Data
@TableName("mes_checkproject_type")
@ApiModel(value="mes_materiel_checktype对象", description="质检中心-物料检测类型")
public class MesCheckprojectType implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**检测阶别*/
	@Excel(name = "检测阶别", width = 15)
	@ApiModelProperty(value = "检测阶别")
	private java.lang.String checkGrade;
	/**项目类型*/
	@Excel(name = "项目类型", width = 15)
	@ApiModelProperty(value = "项目类型")
	private java.lang.String projectType;
	/**抽样方案*/
	@Excel(name = "抽样方案", width = 15)
	@ApiModelProperty(value = "抽样方案")
	private java.lang.String sampleManner;
	/**检测类型*/
	@Excel(name = "检测类型", width = 15)
	@ApiModelProperty(value = "检测类型")
	private java.lang.String checkType;
	/**AQL值*/
	@Excel(name = "AQL值", width = 15)
	@ApiModelProperty(value = "AQL值")
	private java.lang.String aqlValue;
	/**缺陷等级*/
	@Excel(name = "缺陷等级", width = 15)
	@ApiModelProperty(value = "缺陷等级")
	private java.lang.String faultGrade;
	/**检查水平*/
	@Excel(name = "检查水平", width = 15)
	@ApiModelProperty(value = "检查水平")
	private java.lang.String checkLevel;
	/**水平等级*/
	@Excel(name = "水平等级", width = 15)
	@ApiModelProperty(value = "水平等级")
	private java.lang.String levelGrade;
	/**默认标志*/
	@Excel(name = "默认标志", width = 15)
	@ApiModelProperty(value = "默认标志")
	private java.lang.String defaultToken;
	/**全检标志*/
	@Excel(name = "全检标志", width = 15)
	@ApiModelProperty(value = "全检标志")
	private java.lang.String wholeChecktoken;
	/**全检阈值*/
	@Excel(name = "全检阈值", width = 15)
	@ApiModelProperty(value = "全检阈值")
	private java.lang.String wholeCheckvalue;
	/**物料检测id*/
	@ApiModelProperty(value = "物料检测id")
	private java.lang.String mchecktypeId;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
