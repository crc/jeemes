package org.jeecg.modules.mes.inspect.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 宗申首、巡、末件检验记录表
 * @Author: jeecg-boot
 * @Date:   2021-05-15
 * @Version: V1.0
 */
@Data
@TableName("zs_first_patrol_record")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="zs_first_patrol_record对象", description="宗申首、巡、末件检验记录表")
public class ZsFirstPatrolRecord implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**制令单号*/
	@Excel(name = "制令单号", width = 15)
	@ApiModelProperty(value = "制令单号")
	private java.lang.String commandbillCode;
	/**生产订单id*/
	@Excel(name = "生产订单id", width = 15)
	@ApiModelProperty(value = "生产订单id")
	private java.lang.String produceId;
	/**产品名称/顾客*/
	@Excel(name = "产品名称/顾客", width = 15)
    @ApiModelProperty(value = "产品名称/顾客")
    private java.lang.String customer;
	@Excel(name = "状态号", width = 15)
    @ApiModelProperty(value = "状态号")
    private java.lang.String statusNum;
	/**数量*/
	@Excel(name = "数量", width = 15)
    @ApiModelProperty(value = "数量")
    private java.lang.Integer num;
	/**分类（首件、巡检、末件）*/
	@Excel(name = "分类（首件、巡检、末件）", width = 15, dicCode = "zs_inspect_type")
	@Dict(dicCode = "zs_inspect_type")
    @ApiModelProperty(value = "分类（首件、巡检、末件）")
    private java.lang.String type;
	/**物料确认-PCB板*/
	@Excel(name = "物料确认-PCB板", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "物料确认-PCB板")
    private java.lang.String project01;
	/**物料确认-物料*/
	@Excel(name = "物料确认-物料", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "物料确认-物料")
    private java.lang.String project02;
	/**物料确认-BOM表*/
	@Excel(name = "物料确认-BOM表", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "物料确认-BOM表")
    private java.lang.String project03;
	/**功能确认-焊接效果*/
	@Excel(name = "功能确认-焊接效果", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "功能确认-焊接效果")
    private java.lang.String project04;
	/**功能确认-程序烧写*/
	@Excel(name = "功能确认-程序烧写", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "功能确认-程序烧写")
    private java.lang.String project05;
	/**功能确认-功能检测*/
	@Excel(name = "功能确认-功能检测", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "功能确认-功能检测")
    private java.lang.String project06;
	/**功能确认-耐压绝缘*/
	@Excel(name = "功能确认-耐压绝缘", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "功能确认-耐压绝缘")
    private java.lang.String project07;
	/**功能确认-限流值*/
	@Excel(name = "功能确认-限流值", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "功能确认-限流值")
    private java.lang.String project08;
	/**功能确认-欠压值*/
	@Excel(name = "功能确认-欠压值", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "功能确认-欠压值")
    private java.lang.String project09;
	/**外观确认-外观尺寸*/
	@Excel(name = "外观确认-外观尺寸", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "外观确认-外观尺寸")
    private java.lang.String project10;
	/**外观确认-线束*/
	@Excel(name = "外观确认-线束", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "外观确认-线束")
    private java.lang.String project11;
	/**外观确认-密封效果*/
	@Excel(name = "外观确认-密封效果", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "外观确认-密封效果")
    private java.lang.String project12;
	/**外观确认-铭牌*/
	@Excel(name = "外观确认-铭牌", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "外观确认-铭牌")
    private java.lang.String project13;
	/**外观确认-刻字*/
	@Excel(name = "外观确认-刻字", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "外观确认-刻字")
    private java.lang.String project14;
	/**外观确认-包装*/
	@Excel(name = "外观确认-包装", width = 15, dicCode = "zs_inspect_confirm")
	@Dict(dicCode = "zs_inspect_confirm")
    @ApiModelProperty(value = "外观确认-包装")
    private java.lang.String project15;
	/**判定（OK/NG）*/
	@Excel(name = "判定（OK/NG）", width = 15, dicCode = "zs_inspect_decide")
	@Dict(dicCode = "zs_inspect_decide")
    @ApiModelProperty(value = "判定（OK/NG）")
    private java.lang.String decide;
	/**其它（请注明）*/
	@Excel(name = "其它（请注明）", width = 15)
    @ApiModelProperty(value = "其它（请注明）")
    private java.lang.String remarks;
	/**生产签字确认(预留)*/
	@Excel(name = "生产签字确认(预留)", width = 15)
    @ApiModelProperty(value = "生产签字确认(预留)")
    private java.lang.String productConfirm;
	/**质量签字确认(预留)*/
	@Excel(name = "质量签字确认(预留)", width = 15)
    @ApiModelProperty(value = "质量签字确认(预留)")
    private java.lang.String qualityConfirm;
}
