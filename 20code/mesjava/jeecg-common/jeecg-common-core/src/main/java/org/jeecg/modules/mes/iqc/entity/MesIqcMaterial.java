package org.jeecg.modules.mes.iqc.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 来料检验物料详情
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
@Data
@TableName("mes_iqc_material")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_iqc_material对象", description="来料检验物料详情")
public class MesIqcMaterial implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**主表关联编号*/
	@Excel(name = "主表关联编号", width = 15)
    @ApiModelProperty(value = "主表关联编号")
    private java.lang.String iqcId;
	/**物料编号*/
	@Excel(name = "物料编号", width = 15)
    @ApiModelProperty(value = "物料编号")
    private java.lang.String materialId;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private java.lang.String materialName;
	/**来料数量*/
	@Excel(name = "来料数量", width = 15)
    @ApiModelProperty(value = "来料数量")
    private java.lang.String inNum;
	/**抽样数量*/
	@Excel(name = "抽样数量", width = 15)
    @ApiModelProperty(value = "抽样数量")
    private java.lang.String samplesNum;
	/**原值*/
	@Excel(name = "原值", width = 15)
    @ApiModelProperty(value = "原值")
    private java.lang.String origValue;
	/**测试值*/
	@Excel(name = "测试值", width = 15)
    @ApiModelProperty(value = "测试值")
    private java.lang.String testValue;
	/**来料批次*/
	@Excel(name = "来料批次", width = 15)
    @ApiModelProperty(value = "来料批次")
    private java.lang.String inBatch;
	/**供应商名称*/
	@Excel(name = "供应商名称", width = 15)
    @ApiModelProperty(value = "供应商名称")
    private java.lang.String supName;
	/**允收数-致命*/
	@Excel(name = "允收数-致命", width = 15)
    @ApiModelProperty(value = "允收数-致命")
    private java.lang.String accepNum1;
	/**允收数-严重*/
	@Excel(name = "允收数-严重", width = 15)
    @ApiModelProperty(value = "允收数-严重")
    private java.lang.String accepNum2;
	/**允收数-轻微*/
	@Excel(name = "允收数-轻微", width = 15)
    @ApiModelProperty(value = "允收数-轻微")
    private java.lang.String accepNum3;
	/**拒收数-致命*/
	@Excel(name = "拒收数-致命", width = 15)
    @ApiModelProperty(value = "拒收数-致命")
    private java.lang.String rejectionNum1;
	/**拒收数-严重*/
	@Excel(name = "拒收数-严重", width = 15)
    @ApiModelProperty(value = "拒收数-严重")
    private java.lang.String rejectionNum2;
	/**拒收数-轻微*/
	@Excel(name = "拒收数-轻微", width = 15)
    @ApiModelProperty(value = "拒收数-轻微")
    private java.lang.String rejectionNum3;
	/**是否有HSF标示*/
	@Excel(name = "是否有HSF标示", width = 15)
    @ApiModelProperty(value = "是否有HSF标示")
    private java.lang.String hsrfLabeling;
	/**合格状态*/
	@Excel(name = "合格状态", width = 15)
    @ApiModelProperty(value = "合格状态")
    private java.lang.String isQualified;
	/**最终处理结果*/
	@Excel(name = "最终处理结果", width = 15)
    @ApiModelProperty(value = "最终处理结果")
    private java.lang.String finalResults;
}
