package org.jeecg.modules.mes.print;

import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.CharsetUtils;
import org.apache.http.util.EntityUtils;
import org.jeecg.common.util.RestUtil;

import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@Slf4j
public class mesUtil {


    public static String gencontract(String htserver,
                                     conHead conHead) {

        String hturl = "";
        String res = RestUtil.postForEntity(htserver, conHead);
        hturl = res.toString();
        return hturl;

    }
}
