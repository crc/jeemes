package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.modules.mes.inspect.entity.MesBodLog;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * @Description: SMT首件确认表
 * @Author: jeecg-boot
 * @Date:   2021-03-22
 * @Version: V1.0
 */
@Data
@TableName("first_piece_smt")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="first_piece_smt对象", description="SMT首件确认表")
public class FirstPieceSmt implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**线别*/
	@Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private java.lang.String lineType;
	/**日期/班别*/
	@Excel(name = "日期/班别", width = 15)
    @ApiModelProperty(value = "日期/班别")
    private java.lang.String classType;
	/**客户名称*/
	@Excel(name = "客户名称", width = 15)
    @ApiModelProperty(value = "客户名称")
    private java.lang.String customerName;
	/**产品名称*/
	@Excel(name = "产品名称", width = 15)
    @ApiModelProperty(value = "产品名称")
    private java.lang.String productName;
	/**软件版本*/
	@Excel(name = "软件版本", width = 15)
    @ApiModelProperty(value = "软件版本")
    private java.lang.String softwareVersion;
	/**生产任务单*/
	@Excel(name = "生产任务单", width = 15)
    @ApiModelProperty(value = "生产任务单")
    private java.lang.String productNum;
	/**BOM*/
	@Excel(name = "BOM", width = 15)
    @ApiModelProperty(value = "BOM")
    private java.lang.String productBom;
	/**芯片型号*/
	@Excel(name = "芯片型号", width = 15)
    @ApiModelProperty(value = "芯片型号")
    private java.lang.String chipType;
	/**LED灯颜色*/
	@Excel(name = "LED灯颜色", width = 15)
    @ApiModelProperty(value = "LED灯颜色")
    private java.lang.String ledColor;
	/**批量/样本数*/
	@Excel(name = "批量/样本数", width = 15)
    @ApiModelProperty(value = "批量/样本数")
    private java.lang.String samplesNum;
	/**PCB周期/版本*/
	@Excel(name = "PCB周期/版本", width = 15)
    @ApiModelProperty(value = "PCB周期/版本")
    private java.lang.String pcbCycle;
	/**工艺要求*/
	@Excel(name = "工艺要求", width = 15)
    @ApiModelProperty(value = "工艺要求")
    private java.lang.String techReq;
	/**首件类型*/
	@Excel(name = "首件类型", width = 15)
    @ApiModelProperty(value = "首件类型")
    private java.lang.String firstType;
	/**判断结果*/
	@Excel(name = "判断结果", width = 15)
    @ApiModelProperty(value = "判断结果")
    private java.lang.String judgeResult;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
    /**内容*/
    @ApiModelProperty(value = "检查内容")
    @TableField(exist = false)
    private List<FirstPieceSmtLog> firstPieceSmtlogs;
}
