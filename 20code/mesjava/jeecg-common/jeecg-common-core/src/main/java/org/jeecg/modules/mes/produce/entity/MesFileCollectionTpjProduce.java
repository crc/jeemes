package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: mes_file_collection_tpj_produce
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Data
@TableName("mes_file_collection_tpj_produce")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_file_collection_tpj_produce对象", description="mes_file_collection_tpj_produce")
public class MesFileCollectionTpjProduce implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**主表id*/
	@Excel(name = "主表id", width = 15)
    @ApiModelProperty(value = "主表id")
    private String mainId;
	/**站点id*/
	@Excel(name = "站点id", width = 15)
    @ApiModelProperty(value = "站点id")
    private String stationId;
	/**传送通道id*/
	@Excel(name = "传送通道id", width = 15)
    @ApiModelProperty(value = "传送通道id")
    private String cnvrLaneId;
	/**生产枚数*/
	@Excel(name = "生产枚数", width = 15)
    @ApiModelProperty(value = "生产枚数")
    private String productNum;
	/**生产完成电路数*/
	@Excel(name = "生产完成电路数", width = 15)
    @ApiModelProperty(value = "生产完成电路数")
    private String productCir;
	/**坏板标记检测数*/
	@Excel(name = "坏板标记检测数", width = 15)
    @ApiModelProperty(value = "坏板标记检测数")
    private String badMarkNum;
	/**BOC标记检测数*/
	@Excel(name = "BOC标记检测数", width = 15)
    @ApiModelProperty(value = "BOC标记检测数")
    private String bocMarkNum;
	/**区域基准标记识别错误次数*/
	@Excel(name = "区域基准标记识别错误次数", width = 15)
    @ApiModelProperty(value = "区域基准标记识别错误次数")
    private String areaMarkNum;
	/**生产执行时间累计*/
	@Excel(name = "生产执行时间累计", width = 15)
    @ApiModelProperty(value = "生产执行时间累计")
    private String timeRun;
	/**基板传送等待时间*/
	@Excel(name = "基板传送等待时间", width = 15)
    @ApiModelProperty(value = "基板传送等待时间")
    private String timePwb;
	/**基板搬入等待时间*/
	@Excel(name = "基板搬入等待时间", width = 15)
    @ApiModelProperty(value = "基板搬入等待时间")
    private String timePwbIn;
	/**基板搬出等待时间*/
	@Excel(name = "基板搬出等待时间", width = 15)
    @ApiModelProperty(value = "基板搬出等待时间")
    private String timePwbOut;
	/**维护时间*/
	@Excel(name = "维护时间", width = 15)
    @ApiModelProperty(value = "维护时间")
    private String timeRepair;
	/**故障停止时间*/
	@Excel(name = "故障停止时间", width = 15)
    @ApiModelProperty(value = "故障停止时间")
    private String timeTrouble;
	/**元件用尽停止时间*/
	@Excel(name = "元件用尽停止时间", width = 15)
    @ApiModelProperty(value = "元件用尽停止时间")
    private String timeNoCompo;
	/**生产中断时间累计*/
	@Excel(name = "生产中断时间累计", width = 15)
    @ApiModelProperty(value = "生产中断时间累计")
    private String timeDown;
}
