package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecgframework.poi.excel.annotation.Excel;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 制造中心-在线料表-主表
 * @Author: jeecg-boot
 * @Date:   2021-05-17
 * @Version: V1.0
 */
@Data
@TableName("mes_online_materieltable_main")
@ApiModel(value="mes_online_materieltable_main对象", description="制造中心-在线料表-主表")
public class MesOnlineMaterieltableMain implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**线别*/
    @Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private java.lang.String lineType;
	/**成品料号*/
    @Excel(name = "成品料号", width = 15)
    @ApiModelProperty(value = "成品料号")
    private java.lang.String productCode;
	/**成品规格*/
    @Excel(name = "成品规格", width = 15)
    @ApiModelProperty(value = "成品规格")
    private java.lang.String productSpecifications;
	/**成品名称*/
    @Excel(name = "成品名称", width = 15)
    @ApiModelProperty(value = "成品名称")
    private java.lang.String productName;
}
