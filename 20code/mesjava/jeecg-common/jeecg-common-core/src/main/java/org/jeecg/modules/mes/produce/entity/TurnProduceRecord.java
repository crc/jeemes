package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 转产记录表
 * @Author: jeecg-boot
 * @Date:   2021-04-21
 * @Version: V1.0
 */
@Data
@TableName("turn_produce_record")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="turn_produce_record对象", description="转产记录表")
public class TurnProduceRecord implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**原制令单id*/
	@Excel(name = "原制令单id", width = 15)
    @ApiModelProperty(value = "原制令单id")
    private java.lang.String commandbillId;
	/**原制令单code*/
	@Excel(name = "原制令单code", width = 15)
    @ApiModelProperty(value = "原制令单code")
    private java.lang.String commandbillCode;
	/**原机种料号*/
	@Excel(name = "原机种料号", width = 15)
    @ApiModelProperty(value = "原机种料号")
    private java.lang.String mechanismCode;
	/**原机种名称*/
	@Excel(name = "原机种名称", width = 15)
    @ApiModelProperty(value = "原机种名称")
    private java.lang.String mechanismName;
	/**转产制令单id*/
	@Excel(name = "转产制令单id", width = 15)
    @ApiModelProperty(value = "转产制令单id")
    private java.lang.String zcommandbillId;
	/**转产制令单code*/
	@Excel(name = "转产制令单code", width = 15)
    @ApiModelProperty(value = "转产制令单code")
    private java.lang.String zcommandbillCode;
	/**转产机种料号*/
	@Excel(name = "转产机种料号", width = 15)
    @ApiModelProperty(value = "转产机种料号")
    private java.lang.String zmechanismCode;
	/**转产机种名称*/
	@Excel(name = "转产机种名称", width = 15)
    @ApiModelProperty(value = "转产机种名称")
    private java.lang.String zmechanismName;
	/**二维码编号*/
	@Excel(name = "二维码编号", width = 15)
    @ApiModelProperty(value = "二维码编号")
    private java.lang.String warehouseId;
	/**物料料号*/
	@Excel(name = "物料料号", width = 15)
    @ApiModelProperty(value = "物料料号")
    private java.lang.String productCode;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private java.lang.String productName;
	/**剩余数量*/
	@Excel(name = "剩余数量", width = 15)
    @ApiModelProperty(value = "剩余数量")
    private java.lang.String surplusNum;
	/**去向（转产、退料）*/
	@Excel(name = "去向（转产、退料）", width = 15)
    @ApiModelProperty(value = "去向（转产、退料）")
    private java.lang.String state;
}
