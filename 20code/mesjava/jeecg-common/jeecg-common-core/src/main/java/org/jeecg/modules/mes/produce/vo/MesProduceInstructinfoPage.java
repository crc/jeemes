package org.jeecg.modules.mes.produce.vo;

import java.util.List;
import org.jeecg.modules.mes.produce.entity.MesProduceInstructinfo;
import org.jeecg.modules.mes.produce.entity.MesProduceInstructitem;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 生产指示单-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_produce_instructinfoPage对象", description="生产指示单-基本信息")
public class MesProduceInstructinfoPage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**指示单号*/
	@Excel(name = "指示单号", width = 15)
	@ApiModelProperty(value = "指示单号")
	private java.lang.String instructCode;
	/**机种料号*/
	@Excel(name = "机种料号", width = 15)
	@ApiModelProperty(value = "机种料号")
	private java.lang.String mechanismCode;
	/**机种名称*/
	@Excel(name = "机种名称", width = 15)
	@ApiModelProperty(value = "机种名称")
	private java.lang.String mechanismName;
	/**机种规格*/
	@Excel(name = "机种规格", width = 15)
	@ApiModelProperty(value = "机种规格")
	private java.lang.String mechanismGague;
	/**项目模版*/
	@Excel(name = "项目模版", width = 15)
	@ApiModelProperty(value = "项目模版")
	private java.lang.String projectModel;
	/**状态*/
	@Excel(name = "状态", width = 15)
	@ApiModelProperty(value = "状态")
	private java.lang.String state;
	/**归档文件号*/
	@Excel(name = "归档文件号", width = 15)
	@ApiModelProperty(value = "归档文件号")
	private java.lang.String archiveFilenum;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
	
	@ExcelCollection(name="生产指示单-明细信息")
	@ApiModelProperty(value = "生产指示单-明细信息")
	private List<MesProduceInstructitem> mesProduceInstructitemList;
	
}
