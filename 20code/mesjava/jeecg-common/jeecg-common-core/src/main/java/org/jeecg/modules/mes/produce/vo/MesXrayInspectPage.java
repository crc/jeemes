package org.jeecg.modules.mes.produce.vo;

import java.util.List;
import org.jeecg.modules.mes.produce.entity.MesXrayInspect;
import org.jeecg.modules.mes.produce.entity.MesXrayBadinfo;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 制造中心-X-Ray检测
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_xray_inspectPage对象", description="制造中心-X-Ray检测")
public class MesXrayInspectPage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**工作中心*/
	@Excel(name = "工作中心", width = 15)
	@ApiModelProperty(value = "工作中心")
	private java.lang.String workCenter;
	/**产品SN*/
	@Excel(name = "产品SN", width = 15)
	@ApiModelProperty(value = "产品SN")
	private java.lang.String productSn;
	/**检测结果*/
	@Excel(name = "检测结果", width = 15)
	@ApiModelProperty(value = "检测结果")
	private java.lang.String inspectResult;
	/**工单号*/
	@Excel(name = "工单号", width = 15)
	@ApiModelProperty(value = "工单号")
	private java.lang.String workBill;
	/**制令单号*/
	@Excel(name = "制令单号", width = 15)
	@ApiModelProperty(value = "制令单号")
	private java.lang.String commandBill;
	/**机种料号*/
	@Excel(name = "机种料号", width = 15)
	@ApiModelProperty(value = "机种料号")
	private java.lang.String mechanismCode;
	/**备注详情*/
	@Excel(name = "备注详情", width = 15)
	@ApiModelProperty(value = "备注详情")
	private java.lang.String noteDetails;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
	
	@ExcelCollection(name="X-Ray检测-不良信息")
	@ApiModelProperty(value = "X-Ray检测-不良信息")
	private List<MesXrayBadinfo> mesXrayBadinfoList;
	
}
