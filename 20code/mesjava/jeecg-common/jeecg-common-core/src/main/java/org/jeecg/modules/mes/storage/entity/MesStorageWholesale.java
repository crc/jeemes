package org.jeecg.modules.mes.storage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 仓库管理—批号交易
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Data
@TableName("mes_storage_wholesale")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_storage_wholesale对象", description="仓库管理—批号交易")
public class MesStorageWholesale implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**行号*/
	@Excel(name = "行号", width = 15)
    @ApiModelProperty(value = "行号")
    private java.lang.String rowNum;
	/**基本行号*/
	@Excel(name = "基本行号", width = 15)
    @ApiModelProperty(value = "基本行号")
    private java.lang.String baseRownum;
	/**基本单号*/
	@Excel(name = "基本单号", width = 15)
    @ApiModelProperty(value = "基本单号")
    private java.lang.String baseCode;
	/**基本单据类型*/
	@Excel(name = "基本单据类型", width = 15)
    @ApiModelProperty(value = "基本单据类型")
    private java.lang.String baseDockettype;
	/**产品编号*/
	@Excel(name = "产品编号", width = 15)
    @ApiModelProperty(value = "产品编号")
    private java.lang.String productCode;
	/**产品名称*/
	@Excel(name = "产品名称", width = 15)
    @ApiModelProperty(value = "产品名称")
    private java.lang.String productName;
	/**工厂编号*/
	@Excel(name = "工厂编号", width = 15)
    @ApiModelProperty(value = "工厂编号")
    private java.lang.String wareCode;
	/**存储位置*/
	@Excel(name = "存储位置", width = 15)
    @ApiModelProperty(value = "存储位置")
    private java.lang.String wareSite;
	/**入库数量*/
	@Excel(name = "入库数量", width = 15)
    @ApiModelProperty(value = "入库数量")
    private java.lang.String inwareNum;
	/**出库数量*/
	@Excel(name = "出库数量", width = 15)
    @ApiModelProperty(value = "出库数量")
    private java.lang.String outwareNum;
	/**单位*/
	@Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
	/**时间*/
	@Excel(name = "时间", width = 15)
    @ApiModelProperty(value = "时间")
    private java.lang.String query1;
	/**工厂名称*/
	@Excel(name = "工厂名称", width = 15)
    @ApiModelProperty(value = "工厂名称")
    private java.lang.String query2;
	/**凭证抬头id*/
	@Excel(name = "凭证抬头id", width = 15)
    @ApiModelProperty(value = "凭证抬头id")
    private java.lang.String query3;
	/**
     * 备用4  拆板状态
     */
    @Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
    /**
     * 备用5
     */
    @Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private java.lang.String query5;
    /**
     * 批次号
     */
    @Excel(name = "批次号", width = 15)
    @ApiModelProperty(value = "批次号")
    private java.lang.String batchNum;
    /**客户料号*/
    @Excel(name = "客户料号", width = 15)
    @ApiModelProperty(value = "客户料号")
    private java.lang.String clientCode;
    /**上料id*/
    @Excel(name = "上料id", width = 15)
    @ApiModelProperty(value = "上料id")
    private java.lang.String glazeId;
    /**货架编码*/
    @Excel(name = "货架编码", width = 15)
    @ApiModelProperty(value = "货架编码")
    private java.lang.String shelfCode;
    /**货架状态*/
    @Excel(name = "货架状态", width = 15)
    @ApiModelProperty(value = "货架状态")
    private java.lang.String shelfState;
    /**区域编码*/
    @Excel(name = "区域编码", width = 15)
    @ApiModelProperty(value = "区域编码")
    private java.lang.String areaCode;
    /**位置编码*/
    @Excel(name = "位置编码", width = 15)
    @ApiModelProperty(value = "位置编码")
    private java.lang.String locationCode;

    @TableField(exist = false)
    @ApiModelProperty(value = "通道")
    private java.lang.String passage;

    @TableField(exist = false)
    @ApiModelProperty(value = "FEEDER")
    private java.lang.String feeder;

    @TableField(exist = false)
    @ApiModelProperty(value = "规格")
    private java.lang.String gauge;
}
