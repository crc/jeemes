package org.jeecg.modules.mes.storage.vo;

import java.util.List;
import org.jeecg.modules.mes.storage.entity.MesStorageAcceptance;
import org.jeecg.modules.mes.storage.entity.MesAcceptanceItem;
import org.jeecg.modules.mes.storage.entity.MesAcceptanceSite;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 仓库管理—验收入库
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_storage_acceptancePage对象", description="仓库管理—验收入库")
public class MesStorageAcceptancePage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**单号*/
	@Excel(name = "单号", width = 15)
	@ApiModelProperty(value = "单号")
	private java.lang.String orderCode;
	/**业务伙伴编号*/
	@Excel(name = "业务伙伴编号", width = 15)
	@ApiModelProperty(value = "业务伙伴编号")
	private java.lang.String parterCode;
	/**业务伙伴名称*/
	@Excel(name = "业务伙伴名称", width = 15)
	@ApiModelProperty(value = "业务伙伴名称")
	private java.lang.String parterName;
	/**联系人*/
	@Excel(name = "联系人", width = 15)
	@ApiModelProperty(value = "联系人")
	private java.lang.String contactPerson;
	/**联系电话*/
	@Excel(name = "联系电话", width = 15)
	@ApiModelProperty(value = "联系电话")
	private java.lang.String contactPhone;
	/**地址*/
	@Excel(name = "地址", width = 15)
	@ApiModelProperty(value = "地址")
	private java.lang.String address;
	/**邮箱*/
	@Excel(name = "邮箱", width = 15)
	@ApiModelProperty(value = "邮箱")
	private java.lang.String emialAddress;
	/**手机*/
	@Excel(name = "手机", width = 15)
	@ApiModelProperty(value = "手机")
	private java.lang.String mobilePhone;
	/**货币*/
	@Excel(name = "货币", width = 15)
	@ApiModelProperty(value = "货币")
	private java.lang.String currency;
	/**汇率*/
	@Excel(name = "汇率", width = 15)
	@ApiModelProperty(value = "汇率")
	private java.lang.String exchangeRate;
	/**下单日期*/
	@Excel(name = "下单日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "下单日期")
	private java.util.Date downDate;
	/**到期日期*/
	@Excel(name = "到期日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "到期日期")
	private java.util.Date dueDate;
	/**付款条款*/
	@Excel(name = "付款条款", width = 15)
	@ApiModelProperty(value = "付款条款")
	private java.lang.String payBarcode;
	/**税率*/
	@Excel(name = "税率", width = 15)
	@ApiModelProperty(value = "税率")
	private java.lang.String taxRate;
	/**税前金额*/
	@Excel(name = "税前金额", width = 15)
	@ApiModelProperty(value = "税前金额")
	private java.lang.String beforeTaxaccount;
	/**折扣*/
	@Excel(name = "折扣", width = 15)
	@ApiModelProperty(value = "折扣")
	private java.lang.String discount;
	/**总金额*/
	@Excel(name = "总金额", width = 15)
	@ApiModelProperty(value = "总金额")
	private java.lang.String grossAccount;
	/**外币金额*/
	@Excel(name = "外币金额", width = 15)
	@ApiModelProperty(value = "外币金额")
	private java.lang.String foreignAccount;
	/**状态*/
	@Excel(name = "状态", width = 15)
	@ApiModelProperty(value = "状态")
	private java.lang.String state;
	/**货主编号*/
	@Excel(name = "货主编号", width = 15)
	@ApiModelProperty(value = "货主编号")
	private java.lang.String shipperCode;
	/**物流单号*/
	@Excel(name = "物流单号", width = 15)
	@ApiModelProperty(value = "物流单号")
	private java.lang.String logisticalNum;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
	
	@ExcelCollection(name="仓库管理—验收入库子表")
	@ApiModelProperty(value = "仓库管理—验收入库子表")
	private List<MesAcceptanceItem> mesAcceptanceItemList;
	@ExcelCollection(name="仓库管理—库位子表")
	@ApiModelProperty(value = "仓库管理—库位子表")
	private List<MesAcceptanceSite> mesAcceptanceSiteList;
	
}
