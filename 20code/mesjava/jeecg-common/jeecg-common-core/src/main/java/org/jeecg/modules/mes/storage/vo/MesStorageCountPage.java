package org.jeecg.modules.mes.storage.vo;

import java.util.List;
import org.jeecg.modules.mes.storage.entity.MesStorageCount;
import org.jeecg.modules.mes.storage.entity.MesCountItem;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 仓库管理—盘点
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_storage_countPage对象", description="仓库管理—盘点")
public class MesStorageCountPage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**盘点单号*/
	@Excel(name = "盘点单号", width = 15)
	@ApiModelProperty(value = "盘点单号")
	private java.lang.String countCode;
	/**盘点日期*/
	@Excel(name = "盘点日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "盘点日期")
	private java.util.Date countDate;
	/**盘点人*/
	@Excel(name = "盘点人", width = 15)
	@ApiModelProperty(value = "盘点人")
	private java.lang.String countPerson;
	/**复核人*/
	@Excel(name = "复核人", width = 15)
	@ApiModelProperty(value = "复核人")
	private java.lang.String checkPerson;
	/**状态*/
	@Excel(name = "状态", width = 15)
	@ApiModelProperty(value = "状态")
	private java.lang.String state;
	/**复核时间*/
	@Excel(name = "复核时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "复核时间")
	private java.util.Date checkTime;
	/**监盘人*/
	@Excel(name = "监盘人", width = 15)
	@ApiModelProperty(value = "监盘人")
	private java.lang.String overseePerson;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
	
	@ExcelCollection(name="仓库管理—盘点子表")
	@ApiModelProperty(value = "仓库管理—盘点子表")
	private List<MesCountItem> mesCountItemList;
	
}
