一、项目地址

（1）https://gitee.com/erzhongxmu/jeemes.git

 

（2）演示地址：

http://zhaodui.natapp1.cc

账号：admin

密码：123456

 

 

二、sql导入

\1. 数据库MySQL8.0

\2. 数据库转换5.7

(1) Mysql8.0数据导入Mysql5.7问题解决

(2) 本地数据库是mysql5.7，但是需要导入由mysql8.0导出的sql文件

(3) 这是因为本地数据库mysql5.7不支持utf8mb4_0900_ai_ci字符集，所以就把数据库sql文件打开，搜索utf8mb4_0900_ai_ci，然后批量替换成：utf8_general_ci，然后再导入就行了。

(4) 如果原来数据库格式是utf8mb4，而要导入的数据库不支持utf8mb4，那就再把utf8mb4批量改成utf8就行。

 

二、启动nacos

1．配置nacos中conf文件夹下的application.properties文件

![img](file:///C:\Users\ADMINI~1.DES\AppData\Local\Temp\ksohtml\wps6DA1.tmp.jpg) 

2.启动nacos

![img](file:///C:\Users\ADMINI~1.DES\AppData\Local\Temp\ksohtml\wps6DA2.tmp.jpg) 

2．启动成功后进入http://localhost:8848/nacos配置yml文件

![img](file:///C:\Users\ADMINI~1.DES\AppData\Local\Temp\ksohtml\wps6DA3.tmp.jpg) 

![img](file:///C:\Users\ADMINI~1.DES\AppData\Local\Temp\ksohtml\wps6DA4.tmp.jpg) 

 

三、启动后端项目

![img](file:///C:\Users\ADMINI~1.DES\AppData\Local\Temp\ksohtml\wps6DA5.tmp.jpg) 

 

四、启动前端项目--

\1. 修改自己的连接地址

\2. 安装依赖yarn install

\3. 运行项目yarn run serve

![img](file:///C:\Users\ADMINI~1.DES\AppData\Local\Temp\ksohtml\wps6DA6.tmp.jpg) 

 

五、项目启动成功

1．登录页面

![img](file:///C:\Users\ADMINI~1.DES\AppData\Local\Temp\ksohtml\wps6DA7.tmp.jpg) 

2．首页

 

 

![img](file:///C:\Users\ADMINI~1.DES\AppData\Local\Temp\ksohtml\wps6DA8.tmp.jpg) 